package collections;

import java.util.NoSuchElementException;
/**
 * A simple representation of Netflix's database.
 *
 * @author David Weisiger dbw
 * @version September 15, 2017
 */

public class NetvidsDatabaseTest extends student.TestCase {
    /**
     * this makes the field variables
     */
    private Movie movie;
    private NetvidsDatabase container;

    /**
     * this sets up the objects
     */

    public void setUp() {
        container = new NetvidsDatabase();
        movie = new Movie("spiderman");
    }


    /**
     * tests the add method
     */
    public void testAdd() {
        assertTrue(container.addMovie(movie));
        assertFalse(container.addMovie(movie));
    }


    /**
     * Tests the remove method when the object to be removed is present.
     */

    public void testRemove() {
        container.addMovie(movie);

        // Make sure the size has been updated properly
        assertEquals(container.size(), 1);

        // WILL NEED TO IMPLEMENT REMOVE TO POP OUT THE DESIRED STRING
        // Make sure the remove method finds what we just added
        assertTrue(container.remove(movie).equals(movie));

        // Make sure the size has been updated properly
        assertEquals(container.size(), 0);

        // Double check that the string was actually removed
        assertFalse(container.contains(movie));
    }


    /**
     * Tests the remove method when the object to be removed is null.
     */
    public void testRemoveNull() {
        Exception exception = null;

        // Use try-catch block to see whether you get an exception of expected
        // type
        try {
            container.remove(null);
        }
        catch (IllegalArgumentException e) {
            exception = e;
        }

        assertNotNull(exception);

    }


    /**
     * Tests the remove method when the object to be removed is not present.
     */
    public void testRemoveNo() {
        Movie present = new Movie("test");
        Movie notPresent = new Movie("test2");

        container.addMovie(present);

        Exception exception = null;

        // Use try-catch block to see whether you get an exception of expected
        // type
        try {
            container.remove(notPresent);
        }
        catch (NoSuchElementException e) {
            exception = e;
        }

        assertNotNull(exception);
    }


    /**
     * tests the contains method
     * makes sure that it contains the movies that are
     * added
     */
    public void testContains() {
        assertFalse(container.contains(movie));
        container.addMovie(movie);
        assertTrue(container.contains(movie));
    }


    /**
     * tests the isEmpty method
     */
    public void testIsEmpty() {
        assertTrue(container.isEmpty());
        container.addMovie(movie);
        assertFalse(container.isEmpty());
    }


    /**
     * tests the size method
     */
    public void testSize() {
        assertEquals(container.size(), 0);
        container.addMovie(movie);
        assertEquals(container.size(), 1);

        // makes sure that it doesn't add test again
        container.addMovie(movie);
        assertEquals(container.size(), 1);

        // makes sure that it removes test
        container.remove(movie);
        assertEquals(container.size(), 0);
    }


    /**
     * test expand capacity
     */
    public void testExpandCapacity() {
        for (int i = 0; i < 10; i++) {
            container.addMovie(new Movie("" + i));
        }
        container.addMovie(new Movie("test"));
        // CAPACITY HAS NOT BEEN IMPLEMENTED, SHOULD NOT USE GETCURRENTSIZE,
        // WHICH SHOULD BE 11
        assertEquals(container.capacity(), 20);
    }
}
