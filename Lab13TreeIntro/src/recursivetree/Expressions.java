/**
 * 
 */
package recursivetree;

/**
 * @author henry argueta 
 * @version 11/30/2017
 * 
 *
 */
public class Expressions {

    /**
     * @param args
     */
    public static void main(String[] args) {
        
                BinaryTree<String> plus = new BinaryTree<String>("+");
                BinaryTree<String> b = new BinaryTree<String>("b");

                BinaryTree<String> a = new BinaryTree<String>("a");
                BinaryTree<String> treemin = new BinaryTree<String>("-",a,b);
                BinaryTree<String> c = new BinaryTree<String>("c");
                BinaryTree<String> d = new BinaryTree<String>("d");
                BinaryTree<String> treeplus = new BinaryTree<String>("+",c,d);
                BinaryTree<String> e = new BinaryTree<String>("e");
                BinaryTree<String> div = new BinaryTree<String>("/",treeplus,e);
                BinaryTree<String> star = new BinaryTree<String>("*",treemin,div);

                System.out.print(star.toInOrderString());


    }

}
