package spacecolonies;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import bsh.ParseException;

/**
 * This class reads in the planet and applicant information
 * 
 * @author dbw
 * @version 11.16.17
 *
 */
public class ColonyReader {
    /**
     * Instance variables
     */
    private Planet[] planets;
    private ArrayQueue<Person> queue;


    /**
     * Constructor takes in the applicant and planet filenames
     * 
     * @param applicantFileName
     * @param planetFileName
     * @throws FileNotFoundException
     * @throws ParseException
     * @throws SpaceColonyDataException
     */
    public ColonyReader(String applicantFileName, String planetFileName) {

        planets = readPlanetFile(planetFileName);
        queue = readQueueFile(applicantFileName);

        new SpaceWindow(new ColonyCalculator(queue, planets));
    }


    /**
     * return planets
     * 
     * @return planets array
     */
    public Planet[] getPlanets() {
        return planets;
    }


    /**
     * return queue
     * 
     * @return queue of people
     */
    public ArrayQueue<Person> getQueue() {
        return queue;
    }


    public Planet[] readPlanetFile(String filename) {
        try {
            Scanner file = new Scanner(new File(filename));

            planets = new Planet[4];
            int numPlanets = 0;

            // read file while it has lines and haven't reached 3 planets
            while (file.hasNextLine() && numPlanets < 3) {
                // split the line by comma and whitespace to make planets object
                String[] split = file.nextLine().split(", *");

                // make sure that there are 5 comma separated values
                if (split.length == 5) {
                    String name = split[0];
                    int agri = Integer.valueOf(split[1]);
                    int medi = Integer.valueOf(split[2]);
                    int tech = Integer.valueOf(split[3]);
                    int cap = Integer.valueOf(split[4]);

                    // check that skills are between 1 and 5
                    if (isInSkillRange(agri, medi, tech)) {
                        Planet planet = new Planet(name, agri, medi, tech, cap);
                        planets[numPlanets + 1] = planet;
                        numPlanets++;
                    }
                    // if not, throw a SCD exception
                    else {
                        file.close();
                        throw new SpaceColonyDataException();
                    }
                }
                // throws a parse exception if there are not 5 comma separated
                // values
                else {
                    file.close();
                    throw new ParseException();
                }
            }
            file.close();
            // if there are less than three planets throw a SCD exception
            if (numPlanets < 3) {
                throw new SpaceColonyDataException();
            }
        }
        // catches any thrown exceptions
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (SpaceColonyDataException e) {
            e.printStackTrace();
        }
        catch (ParseException e) {
            e.printStackTrace();
        }

        return planets;
    }


    /**
     * This method reads the applicant file and returns an ArrayQueue of
     * applicants
     * 
     * @param applicant
     *            filename
     * @return ArrayQueue<Person>
     */
    public ArrayQueue<Person> readQueueFile(String fileName) {
        ArrayQueue<Person> personQueue = new ArrayQueue<>();
        // tries to find file and add applicants who meet the skill range
        // criteria, otherwise throws an exception
        try {
            Scanner file = new Scanner(new File(fileName));

            while (file.hasNextLine()) {
                // delimits line by comma and whitespace
                String[] split = file.nextLine().split(", *");
                // checks to only add applicants that are formatted with all 5
                // attributes
                if (split.length == 5) {
                    String name = split[0];
                    int agri = Integer.valueOf(split[1]);
                    int medi = Integer.valueOf(split[2]);
                    int tech = Integer.valueOf(split[3]);
                    int planetPreference = getPlanetNumber(split[4]);

                    // check that skills are between 1 and 5
                    if (isInSkillRange(agri, medi, tech)) {
                        Person newPerson = new Person(name, agri, medi, tech,
                            planetPreference);
                        personQueue.enqueue(newPerson);
                    }
                    else {
                        file.close();
                        throw new SpaceColonyDataException();
                    }
                }
            }
            file.close();
        }
        // catches any thrown exceptions
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (SpaceColonyDataException e) {
            e.printStackTrace();
        }
        return personQueue;
    }


    /**
     * This private helper method returns the planet number
     * 
     * @param str
     *            planet name
     * @return int planet number
     */
    private int getPlanetNumber(String planetName) {
        String[] planetNames = new String[planets.length];

        // creates an array of planet names
        for (int i = 1; i < planets.length; i++) {
            planetNames[i] = planets[i].getName();
        }

        int planetNumber = java.util.Arrays.asList(planetNames).indexOf(
            planetName);
        if (planetNumber == -1) {
            return 0;
        }
        else {
            return planetNumber;
        }
    }


    /**
     * This method checks if the skills are within the allowed range
     * 
     * @param agri
     * @param medi
     * @param tech
     * @return true if within skill range, false otherwise
     */
    public boolean isInSkillRange(int agri, int medi, int tech) {
        return 1 <= agri && agri <= 5 || 1 <= medi && medi <= 5 || 1 <= tech
            && tech <= 5;
    }
}
