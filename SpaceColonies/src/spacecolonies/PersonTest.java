package spacecolonies;

/**
 * Tests the Person class
 * 
 * @author dbw
 * @version 11.16.17
 *
 */
public class PersonTest extends student.TestCase {
    /**
     * test objects, needs a colonyReader to check test Person class which
     * accesses ColonyCalculator statically
     */
    @SuppressWarnings("unused")
    private Person person1;
    private Person person2;
    private Person person3;


    /**
     * sets up the test objects to a default value
     */
    public void setUp() {
        person1 = new Person("David Weisiger", 1, 2, 3, 4);
        person2 = new Person("Mikey", 2, 2, 2, 3);
        person3 = new Person("David Weisiger", 1, 2, 3, 4);
    }


    /**
     * tests the getPlanetName method
     */
    public void testGetPlanetName() {
        @SuppressWarnings("unused")
        ColonyReader colonyReader = new ColonyReader("input.txt",
            "planets.txt");
        assertEquals("n/a", person1.getPlanetName());
        assertEquals("Planet3", person2.getPlanetName());
    }


    /**
     * tests the equals method
     */
    public void testEquals() {
        Person person5 = null;
        assertTrue(person1.equals(person1));
        assertFalse(person1.equals(person5));
        assertTrue(person1.equals(person3));
        assertFalse(person1.equals(0));
        assertFalse(person1.equals(person2));
    }


    /**
     * tests the toString method
     */
    public void testToString() {
        assertEquals("No-Planet David Weisiger A:1 M:2 T:3", person1
            .toString());
        assertEquals("Mikey A:2 M:2 T:2 Wants: Planet3", person2.toString());
    }
}
