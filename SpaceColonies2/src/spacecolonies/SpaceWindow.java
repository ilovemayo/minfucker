/**
 * 
 */
package spacecolonies;

import java.awt.Color;

import CS2114.Button;
import CS2114.CircleShape;
import CS2114.Shape;
import CS2114.TextShape;
import CS2114.Window;
import CS2114.WindowSide;
import list.AList;

/**
 * SpaceWindow is the front end of the class which obtains a ArrayQueue and a
 * Planet array from ColonyCalcaluator class. SpaceWindow is responsible for the
 * visualization of the applicants moving through the line and a circle is used
 * to represent people and the squares represents planets.
 * 
 * @author henry argueta
 * @version 11/12/17
 */
public class SpaceWindow {
    // ~ Fields -----------------------------------------------------

    /**
     * The Window class provides a simple Swing Window.
     */
    private Window window;

    /**
     * object handles all the major calculations and decision-making for the
     * program
     */
    private ColonyCalculator colonyCalculator;

    /**
     * Button class encapsulates a JButton and allows the student to create a
     * button
     */
    private Button accept;

    /**
     * Button class encapsulates a JButton and allows the student to create a
     * button
     */
    private Button reject;

    /**
     * containing the elements of the specified collection
     */
    private AList<CircleShape> personCircles;

    /**
     * Constructs a newly allocated Integer object that represents the specified
     * int value.
     * 
     */
    private static int PLANET_SIZE = 100;

    /**
     * Constructs a newly allocated Integer object that represents the specified
     * int value.
     */
    private static int CIRCLE_SIZE = 80;

    /**
     * Constructs a newly allocated Integer object that represents the specified
     * int value.
     */
    private static final int PLANET_GAP = 300;

    /**
     * Constructs a newly allocated Integer object that represents the specified
     * int value.
     */
    private static final int CIRCLE_GAP = 100;

    /**
     * TextShape allows the display of text inside a Shape.
     */
    private TextShape errorMessage;

    /**
     * The Window class provides a simple Swing Window.
     */
    private Planet planetAdd;

    /**
     * TextShape allows the display of text inside a Shape.
     */
    private TextShape personNext;
    

    /**
     * Shape Class provides a definition for objects which can be displayed on a
     * Window.
     */
    private Shape[] planetShapes = new Shape[4];

    /**
     * TextShape allows the display of text inside a Shape.
     */
    private TextShape[] planetsText = new TextShape[4];

    /**
     * The Color class is used to encapsulate colors.
     */
    private static Color[] PLANET_COLOR = { Color.blue.brighter(), Color.CYAN,
            Color.GREEN };

    // ~ Constructor -----------------------------------------------------

    /**
     * This default constructor will initialize the corresponding fields and add
     * the initial planet and applicant line.
     * 
     * @param calculator
     *            object to allow people in to planets
     */
    public SpaceWindow(ColonyCalculator calculator) {
        this.window = new Window();
        window.setSize(1400, 800);
        window.setTitle("Space Colony Placement");
        errorMessage = new TextShape(0, 0, " ");
        errorMessage.setBackgroundColor(Color.WHITE);
        planetAdd = null;
        this.colonyCalculator = calculator;
        int size = colonyCalculator.getQueue().getSize();
        Planet[] planet = ColonyCalculator.getPlanets();

        // center new error textShape
        int heightWindow = window.getHeight();
        int widthWindow = window.getWidth();
        int finalHight = (heightWindow / 2) - (errorMessage.getHeight() / 2);
        int finalWidth = (widthWindow / 2) - (errorMessage.getWidth());
        errorMessage = new TextShape(finalWidth, finalHight, " ");

        // values for the shape
        int x = window.getGraphPanelWidth() / 4;
        int y = window.getGraphPanelHeight() / 2;

        // adding planetShape in window
        planetShapes[1] = new Shape(x, y, PLANET_SIZE, PLANET_COLOR[0]);
        x += PLANET_GAP;
        planetShapes[2] = new Shape(x, y, PLANET_SIZE, PLANET_COLOR[1]);
        x += PLANET_GAP;
        planetShapes[3] = new Shape(x, y, PLANET_SIZE, PLANET_COLOR[2]);

        window.addShape(planetShapes[1]);
        window.addShape(planetShapes[2]);
        window.addShape(planetShapes[3]);

        // adding all applicants in window and setting color
        int widthPanel = window.getGraphPanelWidth() / size;
        int heightPanel = window.getGraphPanelHeight() / 6;

        personCircles = new AList<>();

        // Adding circles in window
        Object[] object = colonyCalculator.getQueue().toArray();
        for (int i = 0; i < size; i++)
        {

            int col = ((Person) object[i]).getPlanet();

            if (((Person) object[i]).getPlanet() != 0)
            {
                CircleShape newEntry = new CircleShape(widthPanel, heightPanel,
                        CIRCLE_SIZE, PLANET_COLOR[col - 1]);
                personCircles.add(newEntry);
            }

            else
            {
                CircleShape newEntry = new CircleShape(widthPanel, heightPanel,
                        CIRCLE_SIZE, Color.LIGHT_GRAY);
                personCircles.add(newEntry);
            }

            window.addShape(personCircles.getEntry(i));
            widthPanel += CIRCLE_GAP;
        }

        // Adding the first person toString to window
        if (colonyCalculator.getQueue().getFront() != null)
        {
            Person person1 = colonyCalculator.getQueue().getFront();
            String strPerson = person1.toString();
            personNext = new TextShape(50, 10, strPerson);
            personNext.setBackgroundColor(Color.WHITE);
            window.addShape(personNext);

        }

        int xplanet = window.getGraphPanelWidth() / 4;
        int yplanet = 3 * window.getGraphPanelHeight() / 4;

        // adding planet to window
        planetsText[1] = new TextShape(xplanet, yplanet, "");
        xplanet += PLANET_GAP;
        planetsText[2] = new TextShape(xplanet, yplanet, "");
        xplanet += PLANET_GAP;
        planetsText[3] = new TextShape(xplanet, yplanet, "");

        for (int i = 1; i < planetsText.length; i++)
        {

            String text = planet[i].getName() + " "
                    + planet[i].getPopulationSize() + "/"
                    + planet[i].getCapacity() + "";

            String skills = planet[i].getSkills().toString();

            planetsText[i].setText(text);
            planetsText[i].setBackgroundColor(Color.WHITE);

            TextShape textShape = new TextShape(planetsText[i].getX(),
                    planetsText[i].getY() + 40, skills);
            textShape.setBackgroundColor(Color.WHITE);

            window.addShape(textShape);
        }

        window.addShape(planetsText[1]);
        window.addShape(planetsText[2]);
        window.addShape(planetsText[3]);

        // add buttons
        accept = new Button("Accept");
        reject = new Button("Reject");
        window.addButton(accept, WindowSide.SOUTH);
        window.addButton(reject, WindowSide.SOUTH);
        reject.onClick(this, "clickedReject");
        accept.onClick(this, "clickedAccept");

        if (colonyCalculator.getQueue().getFront() != null)
        {
            Person person = colonyCalculator.getQueue().getFront();
            if (colonyCalculator.getPlanetForPerson(person) != null)
            {
                planetAdd = colonyCalculator.getPlanetForPerson(person);
                accept.enable();
            } else
            {
                accept.disable();
            }
        }

    }

    // ~ Methods -----------------------------------------------------

    /*
     * This method updates the applicant line of people waiting to be accepted
     * to a planet. Will update the line and will display the next person in
     * line information.
     */
    public void update() {
        
        // if size = 0 end the space colony
        if (!colonyCalculator.getQueue().isEmpty())
        {
            int size = personCircles.getLength();

            // remove circles and moves next circle in line
            if (personCircles.getEntry(0) != null)
            {

                for (int i = 0; i < size; i++)
                {
                    int x = personCircles.getEntry(i).getX();
                    int y = personCircles.getEntry(i).getY();
                    personCircles.getEntry(i).moveTo(x - PLANET_GAP / 3, y);
                }
            }

            // Display the next person in line information
            if (isApplicable())
            {
                Person person = nextInLine();
                personNext.setText(person.toString());
                personNext.setBackgroundColor(Color.WHITE);
            }
        }
        
        else 
        {
        //no more applicants
        window.removeAllShapes();
        window.addShape(new TextShape(errorMessage.getX()-80, errorMessage.getY(),
                "All Applicant Processed - Good Work!"));
        reject.disable();
        accept.disable();
        }

    }

    /**
     * This method is used to when the button is clicked the applicant accepted
     * will now be represented in the planet. will be solved
     * 
     * @param button
     *            button being clicked
     */
    public void clickedAccept(Button button) {

        if(!isApplicable()) {
            errorMessage.setText("error person next in line");

        }
        // accept person and adds to planet
        window.removeShape(personCircles.getEntry(0));
        personCircles.remove(0);
        Planet[] planet = ColonyCalculator.getPlanets();

        if (!colonyCalculator.getQueue().isEmpty())
        {

            // accept person and adds to planet
            colonyCalculator.accept();
            // find planet to set new text

            for (int i = 1; i < planetsText.length; i++)
            {
                if (planetAdd != null && planetAdd == planet[i])
                {
                    String text = planet[i].getName() + " "
                            + planet[i].getPopulationSize() + "/"
                            + planet[i].getCapacity();
                    planetsText[i].setText(text);

                    // calls method to fill in their planet
                    fillPopulation(i - 1, planet[i], planetShapes[i]);
                }
            }
            // if the next person in line is applicable, enable else disable
            if (!isApplicable())
            {
                accept.disable();
            }

            else
            {
                accept.enable();
            }
        }
        // update line
        update();

    }

    /**
     * This method is used to when the button is clicked the applicant is
     * rejected
     * 
     * @param button
     *            button being clicked
     */
    public void clickedReject(Button button) {

        window.removeShape(personCircles.getEntry(0));
        personCircles.remove(0);
        colonyCalculator.reject();

        if (!colonyCalculator.getQueue().isEmpty())
        {
           // sets accept button if next person is applicable
            if (!isApplicable())
            {
                accept.disable();

            }
            if (isApplicable())
            {
                accept.enable();
            }
        }
        update();
    }

    /*
     * This method returns true if the next person in line meets the
     * requirements to be accepted
     * 
     * @return true if applicable
     */
    public boolean isApplicable() {
        Person person = nextInLine();
        if (colonyCalculator.getPlanetForPerson(person) == null)
        {
            accept.disable();
            return false;
        }
        planetAdd = colonyCalculator.getPlanetForPerson(person);
        accept.enable();
        return true;
    }

    /*
     * This method will fill in a portion of the planet shape to represent that
     * they were accepted.
     * 
     * @param index specifics which color to fill
     * 
     * @param planet planet being added to
     * 
     * @param shapeP shape being added to window
     */
    public void fillPopulation(int index, Planet planet, Shape shapeP) {

        int cap = planet.getCapacity();
        int num = planet.getPopulationSize();
        int height = num * shapeP.getHeight() / cap;
        int y = shapeP.getY() + (shapeP.getHeight() - height);
        int x = shapeP.getX();
        int width = shapeP.getWidth();
        Color color = PLANET_COLOR[index].darker();
        Shape fill = new Shape(x, y, width, height, color);
        window.addShape(fill);
        window.moveToFront(fill);

    }

    /*
     * This methods returns the nextPerson in line if next person is null then
     * the window will display an error message.
     * 
     * @return colonyCalculator.getQueue().getFront() next Person in line
     * 
     */
    private Person nextInLine() {

        if (colonyCalculator.getQueue().getFront() == null)
        {
            window.removeAllShapes();
            errorMessage.setText("error person next in line");
            window.addShape(errorMessage);
            accept.disable();
            reject.disable();
        }
        return colonyCalculator.getQueue().getFront();

    }

}
