package spacecolonies;

import java.awt.Color;
import CS2114.Button;
import CS2114.CircleShape;
import CS2114.Shape;
import CS2114.TextShape;
import CS2114.Window;
import CS2114.WindowSide;
import list.AList;

public class SpaceWindow {
    /**
     * field variables
     */
    private Window window;
    private ColonyCalculator colonyCalculator;
    private Button accept;
    private Button reject;
    private AList<CircleShape> circlesArray;

    private static int PLANET_SIZE = 50;
    private static int QUEUE_STARTX = 100;
    private static int QUEUE_STARTY = 50;
    private static int CIRCLE_SIZE = 50;

    private Shape[] planetShapes;

    private static Color[] PLANET_COLORS = { Color.LIGHT_GRAY, Color.RED,
        Color.YELLOW, Color.BLUE };


    public SpaceWindow(ColonyCalculator colonyCalculator) {
        planetShapes = new Shape[4];

        this.colonyCalculator = colonyCalculator;
        window = new Window("Space Colony Placement");

        int PLANET_GAP = 300;
        // values for the shape
        int x = window.getGraphPanelWidth() / 4;
        int y = window.getGraphPanelHeight() / 2;
        // adding planetShape in window
        planetShapes[1] = new Shape(x, y, PLANET_SIZE, PLANET_COLORS[0]);
        x += PLANET_GAP;
        planetShapes[2] = new Shape(x, y, PLANET_SIZE, PLANET_COLORS[1]);
        x += PLANET_GAP;
        planetShapes[3] = new Shape(x, y, PLANET_SIZE, PLANET_COLORS[2]);

        window.addShape(planetShapes[1]);
        window.addShape(planetShapes[2]);
        window.addShape(planetShapes[3]);

        // sets personCircles
        circlesArray = new AList<>();
        Object[] personArray = colonyCalculator.getQueue().toArray();

        int numCircles = 0;
        for (int i = 0; i < personArray.length; i++) {
            circlesArray.add(new CircleShape(QUEUE_STARTX + CIRCLE_SIZE
                * numCircles, QUEUE_STARTY, CIRCLE_SIZE,
                PLANET_COLORS[((Person)personArray[i]).getPlanet()]));

            numCircles++;
            if (numCircles > 7) {
                numCircles = 0;
            }
        }

        // sets buttons
        accept = new Button("ACCEPT");
        accept.onClick(this, "clickedAccept");
        reject = new Button("REJECT");
        reject.onClick(this, "clickedReject");

        // updates the screen
        updateScreen();
    }


    /**
     * clicked accept action
     * 
     * @param button
     */
    public void clickedAccept(Button button) {
        if (colonyCalculator.accept()) {
            circlesArray.remove(0);
            updateScreen();
        }
        else {
            accept.disable();
        }
    }


    /**
     * clicked reject action
     * 
     * @param button
     */
    public void clickedReject(Button button) {
        colonyCalculator.reject();
        circlesArray.remove(0);
        updateScreen();
    }


    /**
     * updates the screen
     */
    public void updateScreen() {
        // remove everything
        window.removeAllShapes();

        if (colonyCalculator.getQueue().isEmpty()) {
            TextShape finished = new TextShape(5, 5,
                "All Applicants Processed - Good Work!");
            finished.setX(window.getGraphPanelWidth() / 2 - finished.getWidth()
                / 2);
            finished.setY(window.getGraphPanelHeight() / 2 - finished
                .getHeight() / 2);
            window.addShape(finished);
            accept.disable();
            reject.disable();
            return;
        }

        // add the current applicant
        TextShape applicant = new TextShape(5, 5, colonyCalculator.getQueue()
            .getFront().toString());
        applicant.setBackgroundColor(Color.WHITE);
        window.addShape(applicant);

        // updates horizontal locations of circles
        int numCircles = 0;
        for (int i = 0; i < circlesArray.getLength(); i++) {
            circlesArray.getEntry(i).setX(QUEUE_STARTX + CIRCLE_SIZE
                * numCircles);
            numCircles++;
            if (numCircles > 7) {
                numCircles = 0;
            }
        }

        // adds the circle shapes
        if (circlesArray.getLength() > 8) {
            for (int i = 0; i < 8; i++) {
                window.addShape(circlesArray.getEntry(i));
            }
        }
        else {
            for (int j = 0; j < circlesArray.getLength(); j++) {
                window.addShape(circlesArray.getEntry(j));
            }
        }

        /**
         * new stuff
         */

        // adds planet info
        int PLANET_GAP = 150;
        // values for the shape
        int x = window.getGraphPanelWidth() / 4;
        int y = window.getGraphPanelHeight() / 2;
        // adding planetShape in window
        Shape planetShapes1 = new Shape(x, y, PLANET_SIZE, PLANET_COLORS[1]);
        x += PLANET_GAP;
        Shape planetShapes2 = new Shape(x, y, PLANET_SIZE, PLANET_COLORS[2]);
        x += PLANET_GAP;
        Shape planetShapes3 = new Shape(x, y, PLANET_SIZE, PLANET_COLORS[3]);

        window.addShape(planetShapes1);
        window.addShape(planetShapes2);
        window.addShape(planetShapes3);

        TextShape[] planetsText = new TextShape[4];
        Planet[] planet = ColonyCalculator.getPlanets();

        int xplanet = window.getGraphPanelWidth() / 4;
        int yplanet = 3 * window.getGraphPanelHeight() / 4;

        // adding planet to window
        planetsText[1] = new TextShape(xplanet, yplanet, "");
        xplanet += PLANET_GAP;
        planetsText[2] = new TextShape(xplanet, yplanet, "");
        xplanet += PLANET_GAP;
        planetsText[3] = new TextShape(xplanet, yplanet, "");

        for (int i = 1; i < planetsText.length; i++) {

            String text = planet[i].getName() + " " + planet[i]
                .getPopulationSize() + "/" + planet[i].getCapacity() + "";

            String skills = planet[i].getSkills().toString();

            planetsText[i].setText(text);
            planetsText[i].setBackgroundColor(Color.WHITE);

            TextShape textShape = new TextShape(planetsText[i].getX(),
                planetsText[i].getY() + 40, skills);
            textShape.setBackgroundColor(Color.WHITE);

            window.addShape(textShape);
        }

        window.addShape(planetsText[1]);
        window.addShape(planetsText[2]);
        window.addShape(planetsText[3]);

        for (int i = 1; i < planetsText.length; i++) {
            String text = planet[i].getName() + " " + planet[i]
                .getPopulationSize() + "/" + planet[i].getCapacity();
            planetsText[i].setText(text);

            // calls method to fill in their planet
            fillPopulation(i - 1, planet[i], planetShapes[i]);

        }

        // add back the buttons and re-enables accept
        window.addButton(accept, WindowSide.SOUTH);
        accept.enable();
        window.addButton(reject, WindowSide.SOUTH);

    }


    public void fillPopulation(int index, Planet planet, Shape shapeP) {

        int cap = planet.getCapacity();
        int num = planet.getPopulationSize();
        int height = num * shapeP.getHeight() / cap;
        int y = shapeP.getY() + (shapeP.getHeight() - height);
        int x = shapeP.getX();
        int width = shapeP.getWidth();
        Color color = PLANET_COLORS[index + 1].darker();
        Shape fill = new Shape(x, y, width, height, color);
        window.addShape(fill);
        window.moveToFront(fill);

    }

}
