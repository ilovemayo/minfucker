package project1;

import bag.BagInterface;
import CS2114.TextShape;
import java.awt.Color;
import CS2114.Window;
import CS2114.Button;
import CS2114.WindowSide;

/**
 * this class creates a GUI to display the item bag
 * 
 * @author David Weisiger dbw
 * @version September 6, 2017
 */

public class ShapeWindow {
    private Window window;
    private TextShape textShape;
    private Button quitButton;
    private Button chooseButton;
    private BagInterface<String> itemBag;


    public ShapeWindow(BagInterface<String> itemBag) {
        // creates window and takes in item bag
        window = new Window();
        window.setTitle("Project 1");
        this.itemBag = itemBag;

        // adds buttons
        quitButton = new Button("Quit");
        quitButton.onClick(this, "clickedQuit");
        window.addButton(quitButton, WindowSide.NORTH);
        chooseButton = new Button("Choose");
        chooseButton.onClick(this, "clickedChoose");
        window.addButton(chooseButton, WindowSide.SOUTH);

        // adds textShape
        textShape = new TextShape(0, 0, "");
        window.addShape(textShape);

    }


    public void clickedQuit(Button button) {
        System.exit(0);
    }


    public void clickedChoose(Button button) {
        if (itemBag.isEmpty()) {
            textShape.setText("No more items.");
        }
        else {
            textShape.setText(itemBag.remove());
        }
        colorText();
        centerText();
    }


    private void colorText() {
        if (textShape.getText().contains("red")) {
            textShape.setForegroundColor(Color.RED);
        }
        else if (textShape.getText().contains("blue")) {
            textShape.setForegroundColor(Color.BLUE);
        }
        else {
            textShape.setForegroundColor(Color.BLACK);
        }
    }


    private void centerText() {
        textShape.setX(window.getGraphPanelWidth() / 2 - textShape.getWidth()
            / 2);
        textShape.setY(window.getGraphPanelHeight() / 2 - textShape.getHeight()
            / 2);
    }
}
