package game;

import bag.SimpleBagInterface;
import student.TestableRandom;

/**
 * Array implementation of bag
 * 
 * @author davidweisiger dbw
 * @version 10.01.17
 * 
 * @param <T>
 *            for generic usage
 */

public class SimpleArrayBag<T> implements SimpleBagInterface<T> {
    /**
     * field variables
     */
    private T[] bag;
    private int numberOfEntries;
    private static final int MAX = 25;


    /**
     * initializes bag and numberOfEntries
     */
    public SimpleArrayBag() {
        @SuppressWarnings("unchecked")
        T[] tempbag = (T[])new Object[MAX];
        bag = tempbag;

        numberOfEntries = 0;

        // sets the next ints picked for testing, comment out for game
        TestableRandom.setNextInts(3, 1, 2, 0);
    }


    /**
     * picks a random element from bag
     */
    @Override
    public T pick() {
        TestableRandom generator = new TestableRandom();
        if (isEmpty()) {
            return null;
        }
        else {
            return bag[generator.nextInt(numberOfEntries)];
        }
    }


    /**
     * implements add
     */
    @Override
    public boolean add(T something) {
        if (numberOfEntries < 25 && something != null) {
            bag[numberOfEntries] = something;
            numberOfEntries++;
            return true;
        }
        else {
            return false;
        }
    }


    /**
     * returns numberOfEntries
     */
    @Override
    public int getCurrentSize() {
        return numberOfEntries;
    }


    /**
     * checks if bag is empty
     */
    @Override
    public boolean isEmpty() {
        return this.getCurrentSize() == 0;
    }


    /**
     * removes an entry from the bag
     */
    @Override
    public boolean remove(T anEntry) {
        if (this.getIndexOf(anEntry) == -1) {
            return false;
        }
        else {
            bag[getIndexOf(anEntry)] = bag[numberOfEntries - 1];
            bag[numberOfEntries - 1] = null;
            numberOfEntries--;
            return true;
        }
    }


    /**
     * gets the index of arg0
     */
    private int getIndexOf(T something) {
        int where = -1;
        boolean found = false;
        int index = 0;
        while (!found && (index < numberOfEntries)) {
            if (something.equals(bag[index])) {
                found = true;
                where = index;
            }
            index++;
        }
        return where;
        /*
         * for (int i = 0; i < bag.length; i++) {
         * if (bag[i].equals(something)) {
         * return i;
         * }
         * }
         * return -1;
         */
        // return Arrays.asList(this).indexOf(arg0);
    }

}
