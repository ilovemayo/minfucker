package recursion;

/**
 * 
 * @author davidweisiger dbw
 * @version 10.14.17
 *
 */

public class FibonacciGeneratorTest extends student.TestCase {
    
    /**
     * setup method
     */

    public void setUp() {
        new FibonacciGenerator();
    }

    /**
     * tests the fib method
     */

    public void testFib() {
        assertEquals(0, FibonacciGenerator.fib(0));
        assertEquals(1, FibonacciGenerator.fib(1));
        assertEquals(1, FibonacciGenerator.fib(2));
        assertEquals(21, FibonacciGenerator.fib(8));
    }
}
