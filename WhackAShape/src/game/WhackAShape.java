package game;

import java.awt.Color;
import CS2114.Button;
import CS2114.CircleShape;
import CS2114.Shape;
import CS2114.SquareShape;
import CS2114.TextShape;
import CS2114.Window;
import CS2114.WindowSide;
import bag.SimpleBagInterface;
import student.TestableRandom;

/**
 * The WhackAShape class
 * 
 * @author davidweisiger dbw
 * @version 10.02.17
 *
 */

public class WhackAShape {
    /**
     * field variables for GUI and bag ADT
     */
    Window window;
    SimpleBagInterface<Shape> bag;


    /**
     * the default constructor uses 4 pre-defined shapes
     */
    public WhackAShape() {
        this.window = new Window();
        this.bag = new SimpleArrayBag<Shape>();

        Button quitButton = new Button("Quit");
        quitButton.onClick(this, "clickedQuit");
        window.addButton(quitButton, WindowSide.NORTH);

        bag.add(this.buildShape("red circle"));
        bag.add(this.buildShape("blue circle"));
        bag.add(this.buildShape("red square"));
        bag.add(this.buildShape("blue circle"));

        window.addShape(bag.pick());
    }


    /**
     * the overloaded constructor
     * 
     * @param inputs
     *            takes in an array of shapes to add to the bag
     */

    public WhackAShape(String[] inputs) {
        this.window = new Window();
        this.bag = new SimpleArrayBag<Shape>();

        Button quitButton = new Button("Quit");
        quitButton.onClick(this, "clickedQuit");
        window.addButton(quitButton, WindowSide.NORTH);

        Shape shape;
        for (String input : inputs) {
            try {
                shape = buildShape(input);
                bag.add(shape);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        window.addShape(bag.pick());

    }


    /**
     * private method that builds shape objects from a string
     * 
     * @throws IllegalArgumentException
     *             for wrong input
     * @param input
     *            takes in a shape to build
     * @return currentShape
     */

    private Shape buildShape(String input) {
        TestableRandom rand = new TestableRandom();
        int size = rand.nextInt(100) + 100;
        int x = rand.nextInt(window.getGraphPanelWidth() - size);
        int y = rand.nextInt(window.getGraphPanelHeight() - size);
        Color color = null;
        Shape currentShape = null;

        if (input.contains("red")) {
            color = Color.RED;
        }
        else if (input.contains("blue")) {
            color = Color.BLUE;
        }

        if (input.contains("circle")) {
            currentShape = new CircleShape(x, y, size, color);
        }
        else if (input.contains("square")) {
            currentShape = new SquareShape(x, y, size, color);
        }
        else {
            throw new IllegalArgumentException("Wrong input!");
        }

        currentShape.onClick(this, "clickedShape");
        return currentShape;
    }


    /**
     * defines the action when a shape is clicked
     * -removes the shape, and adds the next one
     * 
     * @param shape
     *            is what is clicked
     */

    public void clickedShape(Shape shape) {
        window.removeShape(shape);
        bag.remove(shape);
        Shape nextShape = bag.pick();

        if (nextShape == null) {
            // set textshape at 0, 0, so that it could be centered with the
            // horizontal text length accounted for
            TextShape textshape = new TextShape(0, 0, "You Win!");
            textshape.setX(window.getGraphPanelWidth() / 2 - textshape
                .getWidth() / 2);
            textshape.setY(window.getGraphPanelHeight() / 2 - textshape
                .getHeight() / 2);
            window.addShape(textshape);
        }
        else {
            window.addShape(nextShape);
        }
    }


    /**
     * quits the program
     * 
     * @param button
     *            the quit button
     */

    public void clickedQuit(Button button) {
        System.exit(0);
    }


    /**
     * getter method for the window
     * 
     * @return window
     */
    public Window getWindow() {
        return window;
    }


    /**
     * returns a simple bag interface
     * 
     * @return bag
     */
    public SimpleBagInterface<Shape> getBag() {
        return bag;
    }
}
