package spacecolonies;

public class SkillsTest extends student.TestCase {

    private Skills skills;

    /**
     * SetUp method will initiate the Skills
     */
    public void setUp() {
        skills = new Skills(5, 2, 2);
    }

    /**
     * This method will test the getAgriculture method in the Skills class
     */
    public void testGetArgricultire() {
        assertEquals(5, skills.getAgriculture());
    }

    /**
     * This method will test the getMedicine method in the Skills class
     */
    public void testGetMedicine() {
        assertEquals(2, skills.getMedicine());

    }

    /**
     * This method will test the getTechnology method in the Skills class
     */
    public void testGetTechnology() {

        assertEquals(2, skills.getTechnology());

    }

    /**
     * This method will test the getAgriculture method in the Skills class
     */
    public void testIsBelow() {
        Skills skills2 = new Skills(5, 5, 5);
        Skills s3 = new Skills(5, 3, 1);
        Skills skills4 = new Skills(3, 3, 2);
        Skills p = new Skills(5, 2, 2);
        Skills p2 = new Skills(2, 5, 2);
        Skills p3 = new Skills(2, 2, 5);

        assertFalse(p2.isBelow(skills2));
        assertTrue(p.isBelow(s3));
        assertFalse(p2.isBelow(skills2));
        assertFalse(p3.isBelow(skills2));
        assertFalse(skills.isBelow(skills2));

    }

    /**
     * This method will test the toString method in the Skills class
     */
    public void testToString() {
        assertEquals("A:5 M:2 T:2", skills.toString());

    }

    /**
     * This method will test the equals method in the Skills class
     */
    public void testEquals() {
        Skills skills2 = new Skills(3, 4, 5);
        assertFalse(skills.equals(skills2));
        skills2 = new Skills(5, 2, 2);
        assertTrue(skills.equals(skills2));

    }
}
