/**
 * 
 */
package spacecolonies;

import queue.QueueInterface;

/**
 * A class that implements a queue of objects by using an array.
 * This class implements QueueInterface and can hold an default capacity.
 * 
 * 
 * @author Henry Argueta
 * @version 10/29/17
 * 
 * @param <T> 
 *          Type being passed
 * 
 */
public class ArrayQueue<T> implements QueueInterface<T> {
    // ~ Fields -----------------------------------------------------

    /**
     * This constructor allows for the initialCapacity to be set
     */
    private T[] queue;

    /**
     * A constant holding the minimum value an int can have, -231.
     */
    private static final int DEFAULT_CAPACITY = 10;

    /**
     * A constant holding the minimum value an int can have, -231.
     */
    public static final int MAX_CAPACITY = 100;

    /**
     * The Integer class wraps a value of the primitive type int in an object.
     */
    private int enqueueIndex;

    /**
     * The Integer class wraps a value of the primitive type int in an object.
     * 
     */
    private int dequeueIndex;

    /**
     * The Integer class wraps a value of the primitive type int in an object.
     */
    private int size;

    // ~ constructor -----------------------------------------------------

    /**
     * Default constructor, sets default capacity
     */
    public ArrayQueue() {
        this(DEFAULT_CAPACITY);
    } 

    
    // ~ Default constructor----------------------------------------------

    /**
     * This constructor allows for the initialCapacity to be set
     * 
     * @param initialCapacity
     *            the initial capacity of queue array
     */
    public ArrayQueue(int initialCapacity) {
        // the cast is safe because the new array contains null entries
        @SuppressWarnings("unchecked")
        T[] tempQueue = (T[]) new Object[initialCapacity + 1];
        queue = tempQueue;
        enqueueIndex = 0;
        dequeueIndex = initialCapacity;
    }

    // ~ methods -----------------------------------------------------

    /**
     * This method returns the length of the queue array
     * 
     * @return queue.length length of the queue array
     */
    public int getLength() {
        return queue.length;
    }

    /**
     * This method returns the number of entries of the queue array
     * 
     * @return size number of entries
     */
    public int getSize() {
        return size;
    }

    /**
     * This method returns true of the queue array does not have any entries
     * 
     * @return True if size == 0
     */
    public boolean isEmpty() {
        return size <= 0;
    }

    /**
     * This private method returns true of the queue array if the queue array is
     * full entries
     * 
     * @return True if size > initial capacity -1
     */
    private boolean isFull() {
        return size >= getLength() - 1;
    }


    /**
     * This method adds a newEntry to the Array Queue and makes an 
     * initial call to ensure that the Capacity is not one less than the 
     * length. 
     * full entries
     * 
     * @param newEntry entry being added
     */
    public void enqueue(T newEntry) {
        ensureCapacity();
        dequeueIndex = (dequeueIndex + 1) % queue.length;
        queue[dequeueIndex] = newEntry;
        size++;

    }

    /**
     * This method doubles the size if the array queue if it is full.
     */
    private void ensureCapacity() {
        if (isFull())
        {

            T[] oldQueue = queue;
            int oldSize = oldQueue.length;

            @SuppressWarnings("unchecked")
            T[] tempQueue = (T[]) new Object[2 * oldSize];
            queue = tempQueue;

            for (int index = 0; index < oldSize - 1; index++)
            {
                queue[index] = oldQueue[enqueueIndex];
                enqueueIndex = (enqueueIndex + 1) % oldSize;
            }

            enqueueIndex = 0;
            dequeueIndex = oldSize - 2;
        }
    }

    /**
     * This method remove the first entry of the queue array and returns it
     * 
     * @return front entry removed
     */
    public T dequeue() {
        T front = null;
        if (!isEmpty())
        {
            front = queue[enqueueIndex];
            queue[enqueueIndex] = null;
            enqueueIndex = (enqueueIndex + 1) % queue.length;
            size--;
        }
        return front;
    }

    /**
     * This method retrieves the front of our entry.
     * 
     * @return front entry at the front of queue
     */
    public T getFront() {
        T front = null;

        if (!isEmpty())
        {
            front = queue[enqueueIndex];
        }

        return front;
    }

    /**
     * This method clears the queue array
     */
    public void clear() {
        while (!isEmpty())
        {
            dequeue();
        }
    }

    /**
     * This method returns an array of only the entries in the queue array
     * 
     * @return arrayQueue array containing only entries
     */
    public Object[] toArray() {

        @SuppressWarnings("unchecked")
        T[] arrayQueue = (T[]) new Object[getSize()];

        int i = enqueueIndex;
        for (int index = 0; index < getSize(); index++)
        {
            arrayQueue[index] = queue[i];
            i = (i + 1) % queue.length;
        }

        return arrayQueue;

    }

    /**
     * This method returns a string of the queue array
     * 
     * @return builder.toString string reputation of queue array
     */
    public String toString() {

        StringBuilder builder = new StringBuilder();
        builder.append('[');

        // if size if zero will return []
        if (getSize() == 0)
        {
            builder.append("]");
            return builder.toString();
        }

        @SuppressWarnings("unchecked")
        T[] arrayQueue = (T[]) toArray();
        for (int index = 0; index < arrayQueue.length; index++)
        {

            builder.append(String.valueOf(arrayQueue[index]) + ", ");

        }

        builder.deleteCharAt(builder.length() - 1);

        builder.deleteCharAt(builder.length() - 1);

        builder.append(']');
        
        return builder.toString();

    }

    /**
     * This method returns true if the object other equals the this.
     * 
     * @return true if this and other are equal
     * 
     * @param other
     *            object to check if equal
     */
    public boolean equals(Object other) {

        if ((other == null) || (other.getClass() != ArrayQueue.class))
        {
            return false;
        }

        if (other == this)
        {
            return true;
        }

        @SuppressWarnings("unchecked")
        ArrayQueue<T> others = (ArrayQueue<T>) other;

        if (others.getSize() != this.getSize())
        {
            return false;
        }

        for (int i = 0; i < getSize(); i++)
        {

            T myElement = queue[(enqueueIndex + i) % queue.length];
            T otherElement = others.queue[(others.enqueueIndex + i)
                    % others.queue.length];

            if (!myElement.equals(otherElement))
            {
                return false;

            }
        }
        return true;
    }
}
