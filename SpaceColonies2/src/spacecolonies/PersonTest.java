/**
 * 
 */
package spacecolonies;

import java.io.FileNotFoundException;

/**
 * @author henry argueta
 * @version 11/12/17
 *
 */
public class PersonTest extends student.TestCase {

    private Person person;
    private Person person2;
    private Person person3;
    private ArrayQueue<Person> personQ;
    private Planet[] planet = new Planet[4];

    private ColonyReader reader;
    private ColonyCalculator cal;

    private String pathP = "C:/Users/henry/eclipse-workspace/SpaceColoniesInputFilesCleanAndErrors/planets1.txt";
    private String pathI = "C:/Users/henry/eclipse-workspace/SpaceColoniesInputFilesCleanAndErrors/input.txt";

    /**
     * SetUp method will initiate the Person fields
     * 
     * @throws SpaceColonyDataExeption
     * @throws FileNotFoundException
     */
    public void setUp() throws FileNotFoundException, SpaceColonyDataExeption {
        reader = new ColonyReader(pathI, pathP);
        personQ = reader.readQueueFile(pathI);
        person = new Person("Henry Argueta", 2, 4, 3, 1);

        planet[1] = new Planet("Aggies", 5, 2, 2, 10);
        planet[2] = new Planet("Henry", 5, 2, 2, 10);
        planet[3] = new Planet("Jim", 5, 2, 2, 10);

        person2 = new Person("Henry Argueta", 1, 4, 3, 0);
        cal = new ColonyCalculator(personQ, planet);

    }

    /**
     * This method will test the getAgriculture method in the Person class
     */
    public void testGetName() {
        Skills skill = new Skills(1, 5, 3);
        assertEquals("Bob Marley", personQ.getFront().getName());
        personQ.dequeue();
        assertEquals("Nikola Tesla", personQ.getFront().getName());
        personQ.dequeue();
        assertEquals("Leonard McCoy", personQ.getFront().getName());
        assertEquals(skill, personQ.getFront().getSkills());

    }

    /**
     * This method will test the getAgriculture method in the Person class
     */
    public void testGetSkills() {
        Skills skills = new Skills(2, 4, 3);
        assertEquals(skills, person.getSkills());

    }

    /**
     * This method will test the getAgriculture method in the Person class
     */
    public void testGetPlanet() {
        assertEquals(1, personQ.getFront().getPlanet());
        assertEquals(1, personQ.getFront().getPlanet());

    }

    /**
     * This method will test the getAgriculture method in the Person class
     */
    public void testGetPlanetName() {
        assertEquals("Aggies", personQ.getFront().getPlanetName());
        personQ.dequeue();
        personQ.dequeue();
        personQ.dequeue();
        personQ.dequeue();
        personQ.dequeue();
        personQ.dequeue();
        personQ.dequeue();
        assertEquals("No preference", personQ.getFront().getPlanetName());

    }

    /**
     * This method will test the getAgriculture method in the Person class
     */
    public void testToString() {
        assertEquals("Bob Marley A:5 M:3 T1 Wants:Aggies",
                personQ.getFront().toString());

    }

    /**
     * This method will test the getAgriculture method in the Person class
     */
    public void testEquals() {
        assertFalse(person.equals(person3));

    }

}
