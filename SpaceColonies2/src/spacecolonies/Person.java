/**
 * 
 */
package spacecolonies;

/**
 * This class will contain a string, for a person�s name, a skills object, and
 * an int representation of their planet preference.
 * 
 * @author Henry Argueta
 * @version 10/29/17
 */
public class Person {
    // ~ Fields -----------------------------------------------------

    /**
     * The String class represents character strings.
     */
    private String name;
    /**
     * This class will contain three ints for their skills (on a scale of 1 to
     * 5).
     */
    private Skills skills;
    /**
     * The Integer class wraps a value of the primitive type int in an object.
     */
    private int planetPreference;

    // ~ Constructor -----------------------------------------------------

    /**
     * The Person constructor should take 5 parameters, the person's name, their
     * skills levels in all these areas and their planet preference. This data
     * should be listed on each line for the applicant input file.
     * 
     * @param name
     *            name of person
     * 
     * @param agri
     *            skills for agriculture
     * 
     * @param medi
     *            skills for medicine
     * 
     * @param tech
     *            technology skill
     * 
     * @param planet
     *            planet preference
     * 
     */
    public Person(String name, int agri, int medi, int tech, int planet) {
        this.name = name;
        this.planetPreference = planet;
        skills = new Skills(agri, medi, tech);
    }

    /**
     * This method should return the correct name string of the person.
     * 
     * @return name of the person
     */
    public String getName() {
        return name;
    }

    /**
     * This method should return the correct skills that corresponds to the
     * person.
     * 
     * @return sills of the person
     */
    public Skills getSkills() {
        return skills;
    }

    /**
     * This method returns the planetPreference that this person wants to live
     * person.
     * 
     * @return planetPreference of the person
     */
    public int getPlanet() {
        if (planetPreference <= 0 || planetPreference >= 4)
        {
            return 0;
        }
        return planetPreference;
    }

    /**
     * This method should return the correct name string given the planet
     * preference value, or �n/a� if there is not a valid planet preference. The
     * information for each planet should be stored by the ColonyCalcualtor in a
     * public static array of planets.
     * 
     * @return planetName for the person
     */
    public String getPlanetName() {
        if (getPlanet() == 0)
        {
            return "No preference";
        }

        else if (planetPreference <= 3 && planetPreference >= 1)
        {
            return ColonyCalculator.getPlanets()[planetPreference].getName();
        }

        else
        {
            return "n/a";
        }
    }

    /**
     * This method outputs a string will return the correct name string given
     * the planet preference value, or �n/a� if there is not a valid planet
     * preference. The information for each planet should be stored by the
     * ColonyCalcualtor in a public static array of planets.
     * 
     * @return String of the person information
     */
    public String toString() {

        StringBuilder string = new StringBuilder();
        String skillsNum = "A:" + skills.getAgriculture() + " M:"
                + skills.getMedicine() + " T" + skills.getTechnology();

        if (getPlanet() == 0)
        {
            string.append("No-Planet " + getName() + " " + skillsNum);
        }

        else
        {
            string.append(
                    getName() + " " + skillsNum + " Wants:" + getPlanetName());

        }
        return String.valueOf(string);

    }

    /**
     * Two Person objects are considered equal when their name, skills, and
     * planet preference value is the same. You will need to write your own
     * equals method.
     * 
     * @return true if they are equal
     * @param obj
     *            the person to check equality
     */
    public boolean equals(Object obj) {
        if (obj == null)
        {
            return false;
        }

        return this.getName().equals(((Person) obj).getName())
                && this.getSkills() == ((Person) obj).getSkills();
    }

}
