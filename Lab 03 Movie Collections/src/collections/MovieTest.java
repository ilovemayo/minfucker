package collections;
/**
 * @author David Weisiger dbw
 * @version September 15, 2017
 */

public class MovieTest extends student.TestCase {

    private Movie movie;


    /**
     * 
     * Sets up
     * 
     */

    public void setUp() {
        movie = new Movie("Spiderman");
        movie.setGenre("Action");
        movie.setYear(2002);
        movie.setRating(4);
    }


    /**
     * tests the get and set genre method
     */
    public void testGetGenre()

    {
        assertEquals("Action", movie.getGenre());
        movie.setGenre("Romance");
        assertEquals("Romance", movie.getGenre());
    }
    
    /**
     * test the get and set rating methods
     */
    public void testGetRating() {
        assertEquals(4, movie.getRating());
        movie.setRating(5);
        assertEquals(5, movie.getRating());
    }

    /**
     * tests the get and set year methods
     */
    public void testGetYear() {
        assertEquals(2002, movie.getYear());
        movie.setYear(2003);
        assertEquals(2003, movie.getYear());
    }
    
    
    
    /**
     * tests the get title method
     */
    public void testGetTitle() {
        assertEquals("Spiderman", movie.getTitle());
    }
    
    /**
     * tests the equals method
     */
    public void testEquals() {
        Movie test1 = new Movie("blah");
        Movie test2 = new Movie("blah");
        Movie test3 = new Movie("blah2");
        Movie test4 = null;
        String blah = "blah";
        assertTrue(movie.equals(movie));
        assertFalse(movie.equals(test4));
        assertTrue(test1.equals(test2));
        assertFalse(test1.equals(test3));
        assertFalse(test1.equals(blah));
    }
}
