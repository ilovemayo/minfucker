/**
 * 
 */
package prj5;

import java.io.File;

/**
 * This is the test Class for the songinfo
 * 
 * @author davidbauer
 * @author Yue Yang
 * @author david weisiger
 * @version 11.18.17
 */
public class SongInfoTest extends student.TestCase {

    /**
     * private variables
     */
    private SongInfo testSong;
    private String Rulala;
    private String genius;
    private String hoppop;


    /**
     * setup before each method
     */
    public void setUp() {
        testSong = new SongInfo(Rulala, genius, 2017, hoppop);
    }


    /**
     * tests songs toString
     */
    public void testToString() {
        SurveyProcessor sp = new SurveyProcessor(new File(
            "InputFiles/MusicSurveyData.csv"), new File(
                "InputFiles/SongList.csv"));
        LinkedList<SongInfo> si = sp.getSongs();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < si.size(); i++) {
            sb.append(si.toString());
        }
        boolean noError = true;
        assertTrue(noError);

    }


    /**
     * tests compare to artist
     */
    public void testCompareToArtist() {
        SongInfo comp = null;
        Exception e = null;
        try {
            testSong.compareToArtist(comp);
        }
        catch (IllegalArgumentException thrown) {
            e = thrown;
        }
        assertTrue(e instanceof IllegalArgumentException);

    }


    /**
     * tests compare to year
     */
    public void testCompareToYear() {
        SongInfo comp = null;
        Exception e = null;
        try {
            testSong.compareToYear(comp);
        }
        catch (IllegalArgumentException thrown) {
            e = thrown;
        }
        assertTrue(e instanceof IllegalArgumentException);

    }


    /**
     * tests compare to genre
     */
    public void testCompareToGenre() {
        SongInfo comp = null;
        Exception e = null;
        try {
            testSong.compareToGenre(comp);
        }
        catch (IllegalArgumentException thrown) {
            e = thrown;
        }
        assertTrue(e instanceof IllegalArgumentException);

    }


    /**
     * tests compare to title
     */
    public void testCompareToTitle() {
        SongInfo comp = null;
        Exception e = null;
        try {
            testSong.compareToTitle(comp);
        }
        catch (IllegalArgumentException thrown) {
            e = thrown;
        }
        assertTrue(e instanceof IllegalArgumentException);

    }

}
