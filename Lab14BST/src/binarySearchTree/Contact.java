package binarySearchTree;

/**
 * contact class
 * 
 * @author davidweisiger
 * @version 12.9.17
 *
 * @param <T>
 *            for generics
 */
public class Contact implements Comparable {
    /**
     * private variables
     */
    private String name;


    /**
     * constructor
     * 
     * @param name
     *            to set the name
     */
    public Contact(String name) {
        this.name = name;
    }


    /**
     * getter for name
     * 
     * @return the name
     */
    public String getName() {
        return name;
    }


    /**
     * compareTo method
     */
    public int compareTo(String o) {
        return 0;
    }


    @Override
    public int compareTo(Object o) {
        // TODO Auto-generated method stub
        return 0;
    }

}
