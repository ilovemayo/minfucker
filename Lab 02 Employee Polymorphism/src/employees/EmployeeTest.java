package employees;

/**
 * This class tests the Employee class methods
 * 
 * @author davidweisiger
 * @version september 8, 2017
 *
 */
public class EmployeeTest extends student.TestCase {
    /**
     * field variables
     */
    private Employee test;
    private Employee test2;
    private Employee test3;
    private Employee test4;
    private Employee test5;


    /**
     * sets up object
     */
    public void setUp() {
        test = new Employee("bob", 8.0);
        test2 = new Employee("bob", 7.0);
        test3 = new Employee("bob2", 3);
        test4 = new Employee("", 4);
        test5 = null;
    }


    /**
     * tests getname method
     */
    public void testGetName() {
        assertEquals(test.getName(), "bob");
    }


    /**
     * tests get hourly rate
     *
     */
    public void testGetHourlyRate() {
        assertEquals(test.getHourlyRate(), 8.0, .01);
    }


    /**
     * tests get weekly pay
     * 
     */
    public void testWeeklyPay() {
        assertEquals(test.weeklyPay(), 8 * 40, .01);
    }


    /**
     * tests the equals method
     */

    public void testEquals() {
        assertTrue(test.equals(test2));
        assertFalse(test2.equals(test3));
        assertTrue(test.equals(test));
        assertEquals(false, test.equals("bob"));
        assertFalse(test.equals(test4));
        assertFalse(test.equals(test5));
    }

}
