package prj5;

import java.io.File;

/**
 * This is our driver class which opens the GUI and prints out SongInfo to the
 * console
 * 
 * @author dbw
 * @version 11.19.17
 */
public class Input {

    /**
     * main method
     * 
     * @param args
     *            take in two filenames
     */
    public static void main(String[] args) {
        File surveyData = new File("InputFiles/" + args[0]);
        File songList = new File("InputFiles/" + args[1]);

        // processes the files and opens the GUI
        SurveyProcessor surveyProcessor = new SurveyProcessor(surveyData,
            songList);
        @SuppressWarnings("unused")
        GUISurveyResult musicWindow = new GUISurveyResult(surveyProcessor);

        // gets songs by title and genre
        LinkedList<SongInfo> songsByTitle = surveyProcessor.getSongs();
        Object[] songsByGenre = songsByTitle.sortbyGenre();

        // prints by genre
        for (int i = 0; i < songsByGenre.length; i++) {
            System.out.println(songsByGenre[i].toString());
            System.out.println();
        }
        // prints by title
        for (int i = 0; i < songsByTitle.size(); i++) {
            System.out.println(songsByTitle.get(i).toString());
            System.out.println();
        }
    }
}
