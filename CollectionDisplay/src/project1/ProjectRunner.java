package project1;

/**
 * this class runs the program:
 * 1) generates a bag
 * 2) generates a window to display the bag
 * 
 * @author David Weisiger dbw
 * @version September 6, 2017
 */

public class ProjectRunner {
    public static void main(String[] args) {
        DisplayCollection display = new DisplayCollection();
        ShapeWindow shapewindow = new ShapeWindow(display.getItemBag());
    }
}
