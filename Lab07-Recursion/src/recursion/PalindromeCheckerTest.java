package recursion;

/**
 * @author davidweisiger
 * @version 10.15.17
 */

public class PalindromeCheckerTest extends student.TestCase {

    /**
     * setup method
     */

    public void setUp() {
        new PalindromeChecker();
    }


    /**
     * tests if it is a palindrome
     */

    public void testIsPalindrome() {
        assertTrue(PalindromeChecker.isPalindrome(
            "Go hang a salami, I'm a lasagna hog."));
        assertTrue(PalindromeChecker.isPalindrome(
            "A Toyota! Race fast, safe car. A Toyota."));
        assertTrue(PalindromeChecker.isPalindrome(
            "\"Tie Mandie,\" I'd name it."));
        assertTrue(PalindromeChecker.isPalindrome("Wonton? Not now."));
        assertFalse(PalindromeChecker.isPalindrome("blah blah yo"));
        assertFalse(PalindromeChecker.isPalindrome(null));
    }
}
