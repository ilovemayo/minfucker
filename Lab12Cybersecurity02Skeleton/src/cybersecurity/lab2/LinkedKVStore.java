package cybersecurity.lab2;

/**
 * A doubly linked list with sentinel nodes, where each node contains a
 * key-value pair.
 * 
 * @author back
 * @version March 2017
 * @author maellis1
 * @version 06.04.2017
 *
 * @param <K>
 *            the type of the key
 * @param <V>
 *            the type of the value
 */
public class LinkedKVStore<K, V> implements LinkedKVStoreInterface<K, V> {
    /**
     * The inner class KVNode implementing LinkedKVStoreInterface.Entry<KInner,
     * VInner>
     * 
     * @author saima
     *
     * @param <KInner>
     * @param <VInner>
     */
    class KVNode<KInner, VInner>
        implements LinkedKVStoreInterface.Entry<KInner, VInner> {

        private KVNode<KInner, VInner> prev;
        private KVNode<KInner, VInner> next;
        private KInner key;
        private VInner value;


        /**
         * Node constructor no parameters
         */
        public KVNode() {
            this(null, null);
        }


        /**
         * Node constructor with key, value
         * 
         * @param key
         *            input key
         * @param value
         *            input value
         */
        public KVNode(KInner key, VInner value) {
            this.key = key;
            this.value = value;
        }


        /**
         * link with the next node
         * 
         * @param nextNode
         *            node to be linked
         */
        private void linkWith(KVNode<KInner, VInner> nextNode) {
            this.next = nextNode;
            nextNode.prev = this;
        }


        /**
         * insert after the current node
         * 
         * @param current
         *            node to be insert after
         */
        private void insertAfter(KVNode<KInner, VInner> current) {
            KVNode<KInner, VInner> tmp = current.next;
            current.linkWith(this);
            linkWith(tmp);
            size++;
        }


        /**
         * Moves this node to the front of the list.
         * 
         */
        @Override
        @SuppressWarnings("unchecked")
        public void moveToFront() {
            if (head.next.next == this) {
                head.next = head.next.next;
                head.next.next = (LinkedKVStore<K, V>.KVNode<K, V>)this.prev;
                tail.prev.prev = head.next.next;
            }
            if (tail.prev == this) {
                tail.prev = (LinkedKVStore<K, V>.KVNode<K, V>)this.prev;
                this.prev.next =
                    (LinkedKVStore<K, V>.KVNode<KInner, VInner>)tail;
                this.next =
                    (LinkedKVStore<K, V>.KVNode<KInner, VInner>)head.next;
                head.next = (LinkedKVStore<K, V>.KVNode<K, V>)this;
                this.prev = (LinkedKVStore<K, V>.KVNode<KInner, VInner>)head;
                this.next.prev = this;
            }
        }


        /**
         * Removes this node from the list
         * 
         */
        @Override
        public void remove() {
            prev.linkWith(next);
            size--;
        }


        /**
         * Returns key in this node
         * 
         * @return key in this node
         */
        @Override
        public KInner getKey() {
            return this.key;
        }


        /**
         * Returns value in this node
         * 
         * @return value in the current node
         */
        @Override
        public VInner getValue() {
            return this.value;
        }
    }

    private int size;
    private KVNode<K, V> head;
    private KVNode<K, V> tail;


    /**
     * Creates the LinkedKVStore class
     */
    public LinkedKVStore() {
        head = new KVNode<>();
        tail = new KVNode<>();
        init();
    }


    /**
     * initializes a LinkedKVStore object
     */
    private void init() {
        head.linkWith(tail);
        size = 0;
    }


    /**
     * Returns true if this list contains no elements.
     * 
     * @return true if this list contains no elements
     */
    @Override
    public boolean isEmpty() {
        return size == 0;
    }


    /**
     * Returns the number of elements in this list.
     * 
     * @return the number of elements in this list
     */
    @Override
    public int size() {
        return size;
    }


    /**
     * Removes all of the elements from this list. The list will be empty after
     * this call returns.
     */
    @Override
    public void clear() {
        init();
    }


    /**
     * Returns the first occurrence of the specified element in this list (when
     * traversing from head to tail). If this list contains an entry for the
     * specified key, the associated value is returned; otherwise, null is
     * returned.
     * 
     * @param key
     *            a key in this list
     * @return the value to which the key is mapped in this list
     */
    @Override
    public V getFirst(K key) {
        for (KVNode<K, V> current = head.next; current != tail; current =
            current.next) {
            if (current.key.equals(key)) {
                return current.value;
            }
        }
        return null;
    }


    /**
     * Returns the last occurrence of the specified element in this list (when
     * traversing from head to tail). If this list contains an entry for the
     * specified key, the associated value is returned; otherwise, null is
     * returned.
     * 
     * @param key
     *            a key in this list
     * @return the value to which the key is mapped in this list
     */
    @Override
    public V getLast(K key) {
        for (KVNode<K, V> current = tail.prev; current != head; current =
            current.prev) {
            if (current.key.equals(key)) {
                return current.value;
            }
        }
        return null;
    }


    /**
     * Inserts the key (and its corresponding value) at the end of this list.
     * 
     * @param key
     *            the key to be inserted
     * @param value
     *            the value to which the key will be mapped
     */
    @Override
    public LinkedKVStoreInterface.Entry<K, V> addLast(K key, V value) {
        KVNode<K, V> last = tail.prev;
        KVNode<K, V> node = new KVNode<>(key, value);
        node.insertAfter(last);
        return node;
    }


    /**
     * Inserts the key (and its corresponding value) at the beginning of this
     * list.
     * 
     * @param key
     *            the key to be inserted
     * @param value
     *            the value to which the key will be mapped
     */
    @Override
    public LinkedKVStoreInterface.Entry<K, V> addFirst(K key, V value) {
        KVNode<K, V> node = new KVNode<>(key, value);
        node.insertAfter(head);
        return node;
    }


    /**
     * Removes the first occurrence of the key (and its corresponding value) in
     * this list (when traversing the list from head to tail). If the list does
     * not contain the key, it is unchanged.
     * 
     * @param key
     *            the key that needs to be removed, if present
     * @return true if the list contained the specified element
     */
    @Override
    public boolean removeFirst(K key) {
        for (KVNode<K, V> current = head.next; current != tail; current =
            current.next) {
            if (current.key.equals(key)) {
                current.remove();
                return true;
            }
        }
        return false;
    }


    /**
     * Removes and returns the last KVNode in this list. If this list
     * is empty throws a RuntimeException
     *
     * @return KVNode<K, V> the last KVNode in this list
     * @throws RuntimeException
     *             when trying to remove from an empty list
     */
    @Override
    public KVNode<K, V> removeLast() {
        if (this.isEmpty()) {
            throw new RuntimeException();
        }
        else {
            KVNode<K, V> tmp = new KVNode<K, V>(tail.prev.key, tail.prev.value);
            tail.prev.remove();
            return tmp;
        }
    }


    /**
     * Returns a string representation of this linked-list displaying the key
     * and value. The list's string representation is written as
     * its contents (in front-to-rear order, then rear-to-front) with each
     * key value pair surrounded by square brackets, like
     * this:
     * 
     *
     * Forward: [2=two],[4=four],[3=three]
     * Backward: [3=three],[4=four],[2=two]
     * 
     * An empty linked-list is simply Forward: Backward:
     *
     * @return a string representation of the linked-list's key value pairs in
     *         forwards and backwards order
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Forward: ");
        for (KVNode<K, V> current = head.next; current != tail; current =
            current.next) {
            builder.append("[");
            builder.append(current.key);
            builder.append("=");
            builder.append(current.value);
            builder.append("]");
            builder.append((current.next == tail) ? "\n" : ",");
        }
        builder.append("Backward: ");
        for (KVNode<K, V> current = tail.prev; current != head; current =
            current.prev) {
            builder.append("[");
            builder.append(current.key);
            builder.append("=");
            builder.append(current.value);
            builder.append("]");
            builder.append((current.prev == head) ? "\n" : ",");
        }
        return builder.toString();
    }

}
