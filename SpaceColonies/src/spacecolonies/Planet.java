package spacecolonies;

import java.util.Arrays;

/**
 * Planet class describes the attributes of a planet and implements comparable
 * 
 * @author dbw
 * @version 11.16.17
 *
 */
public class Planet implements Comparable<Planet> {
    /**
     * instance variables
     */
    private String name;
    private Skills minSkills;
    private Person[] population;
    private int populationSize;
    private int capacity;


    /**
     * Constructor sets the private variables for planet
     * 
     * @param planetName
     *            name of planet
     * @param planetAgri
     *            minimum skill required
     * @param planetMedi
     *            minimum skill required
     * @param planetTech
     *            minimum skill required
     * @param planetCap
     *            maximum capacity
     */
    public Planet(
        String planetName,
        int planetAgri,
        int planetMedi,
        int planetTech,
        int planetCap) {

        name = planetName;
        minSkills = new Skills(planetAgri, planetMedi, planetTech);
        capacity = planetCap;
        population = new Person[capacity];
        populationSize = 0;
    }


    /**
     * mutator method for setting the planet name
     * 
     * @param name
     *            of planet
     */
    public void setName(String name) {
        this.name = name;
    }


    /**
     * access method for getting planet name
     * 
     * @return planet name
     */
    public String getName() {
        return name;
    }


    /**
     * access method for getting minimum skills required
     * 
     * @return minimum skills
     */
    public Skills getSkills() {
        return minSkills;
    }


    /**
     * @return population
     */

    public Person[] getPopulation() {
        return population;
    }


    /**
     * @return capacity
     */

    public int getCapacity() {
        return capacity;
    }


    /**
     * @return the number of available places left in planet
     */
    public int getAvailability() {
        return capacity - populationSize;
    }


    /**
     * access the population size
     * 
     * @return the population size
     */

    public int getPopulationSize() {
        return populationSize;
    }


    /**
     * @return true if population has reached max capacity, false otherwise
     */
    public boolean isFull() {
        return populationSize == capacity;
    }


    /**
     * method for adding a person to the planet
     * 
     * @param newbie
     *            is new person to add
     * @return true if successfully added, false otherwise
     */
    public boolean addPerson(Person newbie) {
        // checks that the planet isn't full and that the newbie is qualified
        if (!isFull() && isQualified(newbie)) {
            population[populationSize] = newbie;
            populationSize++;
            return true;
        }
        return false;
    }


    /**
     * this method checks the qualifications of an applicant
     * 
     * @param applicant
     *            to see if they are qualified
     * @return true if applicant skills are above the minimum, false otherwise
     */
    public boolean isQualified(Person applicant) {
        Skills applicantSkills = applicant.getSkills();
        return minSkills.isBelow(applicantSkills);
    }


    /**
     * overrides the toString method for string representation of planet
     */

    @Override
    public String toString() {
        int agri = minSkills.getAgriculture();
        int medi = minSkills.getMedicine();
        int tech = minSkills.getTechnology();

        StringBuilder sb = new StringBuilder(name + ", population"
            + populationSize);
        sb.append(" (cap: " + capacity + "), Requires: ");
        sb.append("A >= " + agri + ", M >= " + medi + ", T >= " + tech);
        return sb.toString();
    }


    /**
     * overrides the equals method to compare planets
     */

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() == obj.getClass()) {
            Planet other = (Planet)obj;

            return name.equals(other.name) && minSkills.equals(other.minSkills)
                && Arrays.equals(population, other.population)
                && populationSize == other.populationSize
                && capacity == other.capacity;
        }
        else {
            return false;
        }
    }


    /**
     * implements compareTo method so that you can compare planets to each other
     */

    @Override
    public int compareTo(Planet other) {
        return (this.getAvailability() - other.getAvailability());
    }

}
