package cybersecurity.lab2;

/**
 * LinkedKVStoreInterface<K, V> interface
 * 
 * @author saima
 * @version 04.10.2017
 *
 * @param <K>
 *            the type for the key
 * @param <V>
 *            the type for the value
 */
public interface LinkedKVStoreInterface<K, V> {
    /**
     * Returns true if this list contains no elements.
     * 
     * @return true if this list contains no elements
     */
    public boolean isEmpty();


    /**
     * Returns the number of elements in this list.
     * 
     * @return the number of elements in this list
     */
    public int size();


    /**
     * Removes all of the elements from this list. The list will be empty after
     * this call returns.
     */
    public void clear();


    /**
     * Returns the first occurrence of the specified element in this list (when
     * traversing from head to tail). If this list contains an entry for the
     * specified key, the associated value is returned; otherwise, null is
     * returned.
     * 
     * @param key
     *            a key in this list
     * @return the value to which the key is mapped in this list
     */
    public V getFirst(K key);


    /**
     * Returns the last occurrence of the specified element in this list (when
     * traversing from head to tail). If this list contains an entry for the
     * specified key, the associated value is returned; otherwise, null is
     * returned.
     * 
     * @param key
     *            a key in this list
     * @return the value to which the key is mapped in this list
     */
    public V getLast(K key);


    /**
     * Inserts the key (and its corresponding value) at the end of this list.
     * 
     * @param key
     *            the key to be inserted
     * @param value
     *            the value to which the key will be mapped
     * @return Entry<K, V> the added entry with key and value           
     */
    public Entry<K, V> addLast(K key, V value);


    /**
     * Inserts the key (and its corresponding value) at the beginning of this
     * list.
     * 
     * @param key
     *            the key to be inserted
     * @param value
     *            the value to which the key will be mapped
     * @return Entry<K, V> the added entry with key and value           
     */
    public Entry<K, V> addFirst(K key, V value);


    /**
     * Removes the first occurrence of the key (and its corresponding value) in
     * this list (when traversing the list from head to tail). If the list does
     * not contain the key, it is unchanged.
     * 
     * @param key
     *            the key that needs to be removed, if present
     * @return true if the list contained the specified element
     */
    public boolean removeFirst(K key);


    // missing for consistency is removeLast(K key);
    /**
     * Removes and returns the last KVNode in this list. If this list
     * is empty throws a RuntimeException
     *
     * @return KVNode<K, V> the last KVNode in this list
     * @throws RuntimeException
     *             when trying to remove from an empty list
     */
    public Entry<K, V> removeLast();


    /**
     * Entry<KInner, VInner> interface
     * 
     * @author saima
     *
     * @param <KInner>
     *            the type for the key
     * @param <VInner>
     *            the type for the value
     */
    public interface Entry<KInner, VInner> {
        /**
         * Returns key in this node
         * 
         * @return key in this node
         */
        public KInner getKey();


        /**
         * Returns value in this node
         * 
         * @return value in the current node
         */
        public VInner getValue();


        /**
         * Moves this node to the front of the list.
         * 
         */
        public void moveToFront(); // O(1)


        /**
         * Removes this node from the list
         * 
         */
        public void remove(); // O(1)
    }
}
