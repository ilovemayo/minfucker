package game;

/**
 * Test class for ArrayBag
 * 
 * @author davidweisiger dbw
 * @version 10.01.17
 * 
 */

public class SimpleArrayBagTest extends student.TestCase {
    /**
     * field variables
     */
    private SimpleArrayBag<String> test;


    /**
     * sets up test objects
     */
    @Override
    public void setUp() {
        test = new SimpleArrayBag<String>();
    }


    /**
     * tests the add method
     */
    public void testAdd() {
        assertTrue(test.add("ht"));
        assertTrue(test.add("blah"));
        assertEquals(test.getCurrentSize(), 2);
        assertFalse(test.add(null));
        for (int i = 0; i < 25; i++) {
            test.add("blah");
        }
        assertFalse(test.add("blah"));
    }


    /**
     * tests the getCurrentSize method
     */
    public void testGetCurrentSize() {
        assertEquals(0, test.getCurrentSize());
        test.add("blah");
        assertEquals(test.getCurrentSize(), 2);
    }


    /**
     * tests the isEmpty method
     */
    public void testIsEmpty() {
        assertTrue(test.isEmpty());
        test.add("hot");
        assertFalse(test.isEmpty());
    }


    /**
     * tests the pick method
     */
    public void testPick() {
        assertEquals(null, test.pick());
        test.add("blah");
        test.add("blah2");
        test.add("blah3");
        test.add("blah4");
        assertEquals(test.pick(), "blah4");
        assertEquals(test.pick(), "blah2");
        assertEquals(test.pick(), "blah3");
        assertEquals(test.pick(), "blah");

    }


    /**
     * tests the remove method extensively
     */
    public void testRemove() {
        String obj = null;
        assertFalse(test.add(obj));
        test.add("blah");
        test.add("blah2");
        test.add("blah3");
        test.add("blah4");
        test.remove("blah");
        assertEquals(test.getCurrentSize(), 3);
        assertEquals(test.pick(), "blah4");
        test.remove("blah2");
        assertEquals(test.pick(), "blah3");
        assertFalse(test.remove("nothere"));
    }
}
