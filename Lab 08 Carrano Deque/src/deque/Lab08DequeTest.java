package deque;

import student.TestCase;

/**
 * Tests for the DLinkedDeque class.
 *
 * @author David Weisiger dbw
 * @version October 20, 2017
 */
public class Lab08DequeTest extends TestCase {
    /**
     * field variables
     */

    private Lab08Deque<String> deque;


    /**
     * Creates two brand new, empty sets for each test method.
     */
    public void setUp() {
        deque = new Lab08Deque<String>();
    }


    /**
     * tests the postlab method
     */
    public void testRemoveSecondToLast() {
        deque.addToFront("1");
        assertEquals(null, deque.removeSecond());
        deque.addToBack("2");
        deque.addToBack("3");
        deque.addToBack("4");
        deque.addToBack("5");
        deque.removeSecondToLast();
        assertEquals("[1, 3, 4, 5]", deque.toString());
    }


    public void testRemoveSecond() {
        deque.addToFront("1");
        assertEquals(null, deque.removeSecondToLast());
        deque.addToFront("2");
        deque.addToFront("3");
        deque.addToFront("4");
        deque.addToFront("5");
        deque.removeSecond();
        assertEquals("[5, 4, 3, 1]", deque.toString());
    }


    /**
     * tests the other postlab method
     */

    /**
     * tests the add method to front
     */
    public void testAddToFront() {
        deque.addToFront("hi");
        assertEquals("hi", deque.getFront());
    }


    /**
     * tests the add method to back
     */
    public void testAddToBack() {
        deque.addToBack("hi");
        assertEquals("hi", deque.getBack());
    }


    /**
     * tests remove front
     */
    public void testRemoveFront() {
        deque.addToFront("hi");
        deque.removeBack();
        deque.addToFront("hi");
        deque.addToFront("bye");
        deque.removeFront();
        assertEquals("hi", deque.getFront());

    }


    /**
     * tests remove back
     */
    public void testRemoveBack() {
        deque.addToBack("hi");
        deque.removeFront();
        deque.addToBack("hi");
        deque.addToBack("bye");
        deque.removeBack();
        assertEquals("hi", deque.getBack());

    }


    /**
     * tests get front
     */
    public void testGetFront() {
        deque.addToFront("hi");
        assertEquals("hi", deque.getFront());
    }


    /**
     * tests get back
     */
    public void testGetBack() {
        deque.addToBack("hi");
        assertEquals("hi", deque.getBack());
    }


    /**
     * tests is empty
     */

    public void testIsEmpty() {
        assertTrue(deque.isEmpty());
        deque.addToFront("hi");
        assertFalse(deque.isEmpty());
    }


    /**
     * tests clear
     */
    public void testClear() {
        deque.addToFront("hi");
        deque.clear();
        assertEquals(0, deque.size());
    }


    /**
     * tests toString
     */
    public void testToString() {
        deque.addToFront("hi");
        assertEquals("[hi]", deque.toString());
        deque.addToFront("bye");
        assertEquals("[bye, hi]", deque.toString());
    }


    /**
     * tests the exception
     */
    public void testException() {
        // tests the remove front exception
        Exception thrown = null;
        try {
            // call the method that should throw a NoSuchElementException
            deque.removeFront();
        }
        catch (Exception exception) {
            // ”Catch” and store the exception
            thrown = exception;
        }
        // assert that an exception was thrown
        assertNotNull(thrown);
        // assert that the correct exception was thrown
        assertTrue(thrown instanceof EmptyQueueException);

        // tests the remove back exception
        thrown = null;
        try {
            // call the method that should throw a NoSuchElementException
            deque.removeBack();
        }
        catch (Exception exception) {
            // ”Catch” and store the exception
            thrown = exception;
        }
        // assert that an exception was thrown
        assertNotNull(thrown);
        // assert that the correct exception was thrown
        assertTrue(thrown instanceof EmptyQueueException);

        // tests the remove second exception
        thrown = null;
        try {
            // call the method that should throw a NoSuchElementException
            deque.removeSecond();
        }
        catch (Exception exception) {
            // ”Catch” and store the exception
            thrown = exception;
        }
        // assert that an exception was thrown
        assertNotNull(thrown);
        // assert that the correct exception was thrown
        assertTrue(thrown instanceof EmptyQueueException);

        // tests the remove second exception
        thrown = null;
        try {
            // call the method that should throw a NoSuchElementException
            deque.removeSecondToLast();
        }
        catch (Exception exception) {
            // ”Catch” and store the exception
            thrown = exception;
        }
        // assert that an exception was thrown
        assertNotNull(thrown);
        // assert that the correct exception was thrown
        assertTrue(thrown instanceof EmptyQueueException);

        // tests the remove second exception
        thrown = null;
        try {
            // call the method that should throw a NoSuchElementException
            deque.getFront();
        }
        catch (Exception exception) {
            // ”Catch” and store the exception
            thrown = exception;
        }
        // assert that an exception was thrown
        assertNotNull(thrown);
        // assert that the correct exception was thrown
        assertTrue(thrown instanceof EmptyQueueException);

        // tests the remove second exception
        thrown = null;
        try {
            // call the method that should throw a NoSuchElementException
            deque.getBack();
        }
        catch (Exception exception) {
            // ”Catch” and store the exception
            thrown = exception;
        }
        // assert that an exception was thrown
        assertNotNull(thrown);
        // assert that the correct exception was thrown
        assertTrue(thrown instanceof EmptyQueueException);
    }
}
