package prj5;

/**
 * enum classs
 * 
 * @author davidweisiger
 * @version 11.18.17
 */

public enum HobbyEnum {
    /**
     * private variables and declares enum
     */
    READING("reading"), ART("art"), SPORTS("sports"), MUSIC("music");

    private String name;


    /**
     * constructor
     * 
     * @param name
     *            of enum
     */
    HobbyEnum(String name) {
        this.name = name;
    }


    /**
     * @return name of enum
     */
    public String getName() {
        return name;
    }

}
