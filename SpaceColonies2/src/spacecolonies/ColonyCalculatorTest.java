/**
 * 
 */
package spacecolonies;

import java.io.FileNotFoundException;

/**
 * This class is responsible for reading files of applicants and planets 
 * information. 
 * @author henry argueta
 * @version 11/12/17
 *
 */
public class ColonyCalculatorTest extends student.TestCase{
    private ColonyCalculator colony;
    private String pathP = "C:/Users/henry/eclipse-workspace/SpaceColoniesInputFilesCleanAndErrors/planets1.txt";
    private String pathI = "C:/Users/henry/eclipse-workspace/SpaceColoniesInputFilesCleanAndErrors/input.txt";
    private ColonyReader colonyReader;
    private Planet[] planets;
    private ArrayQueue<Person> queue;

    public void setUp() throws FileNotFoundException, SpaceColonyDataExeption {
        colonyReader = new ColonyReader(pathI, pathP);
        planets =colonyReader.readPlanetFile(pathP);
        queue = colonyReader.readQueueFile(pathI);
        colony = new ColonyCalculator(queue,
                planets);
    }

    public void testGetQueue()
            throws FileNotFoundException, SpaceColonyDataExeption {

        assertEquals(null,colony.getQueue().getFront());



    }

    public void testGetPlanets() {
        assertEquals(1, queue.getFront().getPlanet());
        colony.getQueue().dequeue();
        assertEquals(2, queue.getFront().getPlanet());
        colony.getQueue().dequeue();
        assertEquals(3, queue.getFront().getPlanet());
        
        colony.getQueue().dequeue();
        colony.getQueue().dequeue();
        colony.getQueue().dequeue();
        colony.getQueue().dequeue();
        colony.getQueue().dequeue();
        assertEquals(0, queue.getFront().getPlanet());
    }

    public void testGetPlanetForPerson() {
        assertEquals(null, colony.getPlanetForPerson(new Person("Bob", 5,3,1,1)));
        assertEquals("Techies", colony.getPlanetForPerson(new Person("Marshall Mercedes", 5, 5, 5,3)).getName());


    }
    public void testAccept() {
        int size = queue.getSize();
        
        for(int i = 0; i<size; i++) {
            
            if(colony.accept() && i ==5) {
                assertEquals("Marva Buel", colony.getQueue().getFront().getName());
            }
            
            if(colony.accept() && i ==12) {
                assertEquals("Treva Emrick", colony.getQueue().getFront().getName());
            }
            
            if(colony.accept()) {
                System.out.println("False");
            }
        }
    }
    
    public void testReject() {
        
        for(int i =2; i >= 0; i--) {
            System.out.println(i+"i  ");
            for(int j = planets.length-1; j >= 1; j--) {
                System.out.println(j+"j");

            }
        }
            
        
    }

    public void testPlanetByNumber() {

    }
}
