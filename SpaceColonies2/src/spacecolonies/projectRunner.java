/**
 * 
 */
package spacecolonies;

import java.io.FileNotFoundException;

/**
 * This class creates a new instance of the ColonyReader class. It will call pass a file
 * in which the colonyReader will examine the files. This reader class will then go on 
 * to pass a ArrayQueue and a Array to the colony calculator where all major calculation 
 * is done. 
 * 
 * @author henry argueta 
 * @version 11/12/17
 *
 */
public class projectRunner {

    /**ProjectRunner will run the our space 
     * @param args
     * @throws SpaceColonyDataExeption 
     * @throws FileNotFoundException 
     */
    public static void main(String[] args) throws FileNotFoundException, SpaceColonyDataExeption {

         String pathP = "planets1.txt";
         String pathI = "inputAllAccept.txt";

        @SuppressWarnings("unused")
        ColonyReader colonyReader = new ColonyReader(pathI,pathP);
    }

}
