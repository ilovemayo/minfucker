package game;

import bag.Node;
import bag.SimpleBagInterface;
import student.TestableRandom;

/**
 * 
 * @author davidweisiger dbw
 * @version 10.01.17
 * 
 * @param <T>
 *            for generic usage
 */

public class SimpleLinkedBag<T> implements SimpleBagInterface<T> {
    /**
     * field variables
     */
    private Node<T> firstNode;
    private int numberOfEntries;


    /**
     * constructor
     */
    public SimpleLinkedBag() {
        firstNode = null;
        numberOfEntries = 0;

        // sets the next ints picked for testing
        TestableRandom.setNextInts(0, 1, 2, 3);
    }


    /**
     * implements the pick method
     */
    @Override
    public T pick() {
        TestableRandom generator = new TestableRandom();
        if (this.isEmpty()) {
            return null;
        }
        else {
            Node<T> currentNode = firstNode;
            int index = generator.nextInt(numberOfEntries);
            for (int i = 0; i < index; i++) {
                currentNode = currentNode.next();
            }
            return currentNode.data();
        }
    }


    /**
     * implements add method
     */
    @Override
    public boolean add(T newEntry) {
        if (newEntry == null) {
            return false;
        }
        Node<T> newNode = new Node<T>(newEntry);
        newNode.setNext(firstNode);

        firstNode = newNode;
        numberOfEntries++;

        return true;
    }


    /**
     * return number of entries
     */
    @Override
    public int getCurrentSize() {
        return numberOfEntries;
    }


    /**
     * implements the empty method
     */
    @Override
    public boolean isEmpty() {
        return this.getCurrentSize() == 0;
    }


    /**
     * implements the remove method
     */
    @Override
    public boolean remove(T anEntry) {
        boolean result = false;
        Node<T> nodeN = getReferenceTo(anEntry);

        if (nodeN != null) {
            nodeN.setData(firstNode.data());

            firstNode = firstNode.next();
            numberOfEntries--;
            result = true;
        }
        return result;
    }


    /**
     * gets the reference to an entry
     */

    private Node<T> getReferenceTo(T anEntry) {
        boolean found = false;
        Node<T> currentNode = firstNode;
        while (!found && (currentNode != null)) {
            if (anEntry.equals(currentNode.data())) {
                found = true;
            }
            else {
                currentNode = currentNode.next();
            }
        }
        return currentNode;
    }

}
