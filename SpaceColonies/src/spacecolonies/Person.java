package spacecolonies;

/**
 * person class
 *
 * @author dbw
 * @version 11.06.17
 */
public class Person {
    /**
     * field variables
     */
    private String name;
    private Skills skills;
    private int planetPreference;


    /**
     * constructor
     *
     * @param name
     *            of person
     * @param agri
     *            skill
     * @param medi
     *            skill
     * @param tech
     *            skill
     * @param planet
     *            skill
     */
    public Person(String name, int agri, int medi, int tech, int planet) {
        this.name = name;
        skills = new Skills(agri, medi, tech);
        planetPreference = planet;
    }


    /**
     * @return name
     */
    public String getName() {
        return name;
    }


    /**
     * @return skills
     */
    public Skills getSkills() {
        return skills;
    }


    /**
     * @return planet
     */
    public int getPlanet() {
        if (planetIsValid()) {
            return planetPreference;
        }
        else {
            return 0;
        }
    }


    /**
     * @return true if planetPreference is valid, false otherwise
     */
    private boolean planetIsValid() {
        return 1 <= planetPreference && planetPreference <= 3;
    }


    /**
     * @return planetName
     */
    public String getPlanetName() {
        if (planetIsValid()) {
            Planet[] planets = ColonyCalculator.getPlanets();
            return planets[planetPreference].getName();
        }
        else {
            return "n/a";
        }
    }


    /**
     * @return toString
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(name + " " + skills.toString());

        if (planetIsValid()) {
            sb.append(" Wants: " + getPlanetName());
        }
        else {
            sb.insert(0, "No-Planet ");
        }

        return sb.toString();
    }


    /**
     * @param obj
     * @return true if name, skills, and planet preference are equal, false
     *         otherwise
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() == obj.getClass()) {
            Person other = (Person)obj;

            return name.equals(other.name) && skills.equals(other.skills)
                && planetPreference == other.planetPreference;
        }
        else {
            return false;
        }
    }
}
