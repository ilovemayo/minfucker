package spacecolonies;

/**
 * tests the planet class
 * 
 * @author dbw
 * @version 11.16.17
 *
 */
public class PlanetTest extends student.TestCase {
    /**
     * private variables for testing
     */
    private Planet planet1;
    private Planet planet2;
    private Planet planet3;
    private Planet planet4;


    /**
     * sets up fresh variables for each test
     */
    public void setUp() {
        planet1 = new Planet("planet1", 1, 1, 1, 1);
        planet2 = new Planet("planet2", 1, 2, 3, 4);
        planet3 = new Planet("planet2", 1, 2, 3, 4);
        planet4 = new Planet("planet1", 1, 1, 1, 1);
    }


    /**
     * tests the add person method
     */
    public void testAddPerson() {
        // not full and not qualified
        assertFalse(planet4.addPerson(new Person("michael", 0, 0, 0, 1)));
        // not full and is qualified
        assertTrue(planet1.addPerson(new Person("michael", 2, 2, 2, 1)));
        // full and is not qualified
        assertFalse(planet1.addPerson(new Person("michael", 0, 0, 0, 1)));
        // full and is qualified
        assertFalse(planet1.addPerson(new Person("michael", 2, 2, 2, 1)));
    }


    /**
     * tests the toString method
     */
    public void testToString() {
        assertEquals(
            "planet1, population0 (cap: 1), Requires: A >= 1, M >= 1, T >= 1",
            planet1.toString());
    }


    /**
     * tests the equals method
     */
    public void testEquals() {
        assertTrue(planet1.equals(planet1));
        assertFalse(planet1.equals(null));
        assertTrue(planet2.equals(planet3));
        planet2.addPerson(new Person("michael", 4, 4, 4, 2));
        planet3.addPerson(new Person("michael", 4, 4, 4, 2));
        assertTrue(planet3.equals(planet2));
        assertFalse(planet1.equals(0));
    }


    /**
     * tests the compareTo method
     */
    public void testCompareTo() {
        assertEquals(-3, planet1.compareTo(planet2));
        assertEquals(3, planet2.compareTo(planet1));
        assertEquals(0, planet2.compareTo(planet3));
    }
}
