/**
 * 
 */
package spacecolonies;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * The ColonyReader class will read the input and begin the SpaceWindow. In the
 * constructor, you will call methods to complete parsing the parsing and then
 * create a ColonyCalculator for processing. Pass your ColonyCalculator into
 * another class, SpaceWindow, to run your program.
 * 
 * @author henry argueta
 * @version 11/2/17
 *
 */
public class ColonyReader {
    // ~ Fields -----------------------------------------------------

    /**
     * This class will create the planets to incorporate in the window.
     */
    private Planet[] planets;
    /**
     * This class will create the planets to incorporate in the window.
     */
    private ArrayQueue<Person> queue;

    // ~ Constructor -----------------------------------------------------

    /**
     * The colonyReader constructor takes in the file and construct a ArrayQueue
     * of people and array of planets. It will pass it on to colony calculator
     * then pass the colonyCalculator to the front end class which is
     * SpaceWindow
     * 
     * @param applicantFileName
     *            file of applicant
     * @param planetFileName
     *            file of planets
     * @throws SpaceColonyDataExeption
     * 
     */
    public ColonyReader(String applicantFileName, String planetFileName)
            throws SpaceColonyDataExeption, FileNotFoundException {

        // read planet first
        planets = readPlanetFile(planetFileName);
        queue = readQueueFile(applicantFileName);

        ColonyCalculator colonyCalculator = new ColonyCalculator(queue,
                planets);

        @SuppressWarnings("unused")
        SpaceWindow spaceWindow = new SpaceWindow(colonyCalculator);
    }

    // ~ Methods -----------------------------------------------------

    /**
     * This method will read through the file and containing planets and
     * creating/returning an array of planets
     * 
     * @return planet a array of planets
     * @param fileName
     *            the file name which contains the planet
     * @throws FileNotFoundException
     */
    public Planet[] readPlanetFile(String fileName)
            throws SpaceColonyDataExeption, FileNotFoundException {
        planets = new Planet[4];
        File planetFile = new File(fileName);
        @SuppressWarnings("resource")
        Scanner file = new Scanner(planetFile);

        int numberOfPlanets = 0;
        String[] str;

        while (file.hasNextLine())
        {
            str = file.nextLine().split("\\s*,\\s*");
            Integer[] nums = new Integer[4];

            // planet files must have a certain length
            if (str.length != 5)
            {
                throw new SpaceColonyDataExeption(
                        "File input has less than four input");
            }

            // numbers to pass to planet
            nums[0] = Integer.valueOf(str[1]);
            nums[1] = Integer.valueOf(str[2]);
            nums[2] = Integer.valueOf(str[3]);
            nums[3] = Integer.valueOf(str[4]);

            if (!isinSkillRange(nums[0], nums[1], nums[2]))
            {
                throw new SpaceColonyDataExeption(
                        "skills requirements not in range from 1 to 5");
            }

            Planet planet = new Planet(str[0], nums[0], nums[1], nums[2],
                    nums[3]);
            numberOfPlanets++;
            planets[numberOfPlanets] = planet;

        }

        file.close();

        // must have three planets
        if (numberOfPlanets == 3)
        {
            return planets;
        } else
        {
            throw new SpaceColonyDataExeption("Number of planet is not 3");
        }
    }

    /**
     * This method will read through the file and containing applicants and
     * creating/returning an arrayQueue of people
     * 
     * @return queue a ArrayQueue of applicants
     * @param fileName
     *            the file name which contains the applicants
     * @throws FileNotFoundException
     */
    public ArrayQueue<Person> readQueueFile(String fileName)
            throws SpaceColonyDataExeption, FileNotFoundException {

        queue = new ArrayQueue<Person>();
        File filePerson = new File(fileName);
        @SuppressWarnings("resource")
        Scanner file = new Scanner(filePerson);

        while (file.hasNextLine())
        {
            Person person;
            String strLine = file.nextLine();
            String[] str = strLine.split("\\s*,\\s*");
            int length = str.length;
            Integer[] nums = new Integer[4];

            if (length < 4 || length > 5)
            {
                throw new SpaceColonyDataExeption(
                        "File input is missing a name or a skill");
            }

            nums[0] = Integer.valueOf(str[1]);
            nums[1] = Integer.valueOf(str[2]);
            nums[2] = Integer.valueOf(str[3]);
            nums[3] = 0;

            if (!isinSkillRange(nums[0], nums[1], nums[2]))
            {
                throw new SpaceColonyDataExeption(
                        "skills requirements not in range from 1 to 5");
            }

            // if length is 5 then retrieve number
            if (str.length == 5)
            {
                nums[3] = getPlanetNumber(str[4]);
            }

            person = new Person(str[0], nums[0], nums[1], nums[2], nums[3]);
            // add person
            queue.enqueue(person);
        }
        return queue;
    }

    /**
     * This method will read through the file and containing applicants and
     * creating/returning an arrayQueue of people
     * 
     * @return planetNumber number of planet
     * @param str
     *            string of planet preference
     */
    private int getPlanetNumber(String str) {

        int planetNumber = 0;

        // for-loop through str and if a digit then return number
        for (char c : str.toCharArray())
        {
            if (Character.isDigit(c))
            {
                planetNumber = Character.getNumericValue(c);
            }

        }

        // for-loop through planet array and
        // compare names with str and return index. Start index at 1
        if (planetNumber == 0)
        {

            for (int i = 1; i < planets.length; i++)
            {
                if (planets[i].getName().equals(str))
                {
                    planetNumber = i;
                }

            }

        }

        if (planetNumber < 1 || planetNumber > 3)
        {
            return 0;
        }
        return planetNumber;
    }

    /**
     * This function should return weither or not all of the integers it is
     * passed (num1, num2 and num3) are between the minimum and maximum possible
     * values for a skill.
     * 
     * @return true if skills are in range
     * @param argic
     *            level for agriculture
     * @param med
     *            level for medical
     * @param tech
     *            level for technology
     */
    public boolean isinSkillRange(int argic, int med, int tech) {

        if (argic < 1 || med < 1 || tech < 1)
        {
            return false;
        }
        if (argic > 5 || med > 5 || tech > 5)
        {
            return false;
        }

        return true;
    }
}
