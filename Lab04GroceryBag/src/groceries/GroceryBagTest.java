package groceries;

/**
 * this class tests the grocery bag methods
 * 
 * @author David Weisiger dbw
 * @version 9.22.2017
 */

public class GroceryBagTest extends student.TestCase {
    /**
     * private instance variables
     */
    private GroceryBag bag1; // unordered
    private GroceryBag bag2; // unordered 
    private GroceryBag bag6;
    private GroceryBag bag7;
    private GroceryBag bag8;
    private GroceryBag bag5;


    /**
     * Sets up each test method.
     */
    public void setUp() {
        bag1 = new GroceryBag();
        bag1.add("apples");
        bag1.add("chips");
        bag1.add("yogurt");
        bag1.add("chicken");
        bag1.add("pasta");

        bag5 = new GroceryBag();
        bag5.add("apples");
        bag5.add("chips");
        bag5.add("yogurt");
        bag5.add("chicken");
        bag5.add("pasta");

        bag2 = new GroceryBag();
        bag2.add("apples");
        bag2.add("yogurt");
        bag2.add("chips");
        bag2.add("pasta");
        bag2.add("pasdfad");

        bag6 = new GroceryBag();
        bag6.add("apple");
        bag6.add("apple");
        bag6.add("apple");
        bag6.add("banana");

        bag7 = new GroceryBag();
        bag7.add("apple");
        bag7.add("apple");
        bag7.add("banana");
        bag7.add("banana");

        bag8 = new GroceryBag();
        bag8.add("apple");
        bag8.add("apple");
        bag8.add("banana");

    }


    /**
     * The intersection of two bags contains the minimum number of occurrences
     * of each element from each bag.
     */

    public void testIntersection() {
        assertEquals(bag6.intersection(bag7), bag8);

    }


    /**
     * tests the equals method
     */

    public void testEquals() {
        GroceryBag blah = null;
        String lah = "";
        assertFalse(bag1.equals(bag6));
        assertFalse(bag1.equals(lah));
        assertFalse(bag1.equals(bag2));
        assertTrue(bag1.equals(bag5));
        assertFalse(bag1.equals(blah));
        assertTrue(bag1.equals(bag1));

    }
}
