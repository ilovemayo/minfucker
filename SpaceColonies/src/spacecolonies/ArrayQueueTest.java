package spacecolonies;

import queue.EmptyQueueException;

/**
 * This class tests the ArrayQueue
 * 
 * @author dbw
 * @version 11.15.17
 */
public class ArrayQueueTest extends student.TestCase {
    /**
     * field variables
     */
    private ArrayQueue<String> queue1;
    private ArrayQueue<String> queue2;


    /**
     * sets up instance variables
     */
    public void setUp() {
        queue1 = new ArrayQueue<>();
        queue2 = new ArrayQueue<>();
        queue1.enqueue("a");
        queue1.enqueue("b");
        queue1.enqueue("c");
        queue1.enqueue("d");
    }


    /**
     * tests exceptions
     */

    public void testException() {
        ArrayQueue<String> queue7 = new ArrayQueue<>();
        Exception thrown = null;
        try {
            queue7.dequeue();
        }
        catch (Exception exception) {
            thrown = exception;
        }
        assertTrue(thrown instanceof EmptyQueueException);

        ArrayQueue<String> queue8 = new ArrayQueue<>();
        Exception thrown1 = null;
        try {
            queue8.getFront();
        }
        catch (Exception exception) {
            thrown1 = exception;
        }
        assertTrue(thrown1 instanceof EmptyQueueException);

        ArrayQueue<String> queue10 = new ArrayQueue<>();
        Exception thrown3 = null;
        try {
            queue10.checkCapacity(100000);
        }
        catch (Exception exception) {
            thrown3 = exception;
        }
        assertTrue(thrown3 instanceof IllegalStateException);
    }


    /**
     * tests equals method
     */
    public void testEquals2() {
        ArrayQueue<String> queue1 = new ArrayQueue<>();
        ArrayQueue<String> queue2 = null;

        assertFalse(queue1.equals(queue2));
        assertFalse(queue1.equals(0));
        assertTrue(queue1.equals(queue1));
        queue1.enqueue("a");
        assertFalse(queue1.equals(queue2));
        queue2 = new ArrayQueue<>();
        queue2.enqueue("b");
        assertFalse(queue1.equals(queue2));
        queue2.dequeue();
        queue2.enqueue("a");
        assertTrue(queue2.equals(queue1));
    }


    /**
     * this method tests the length, size, and capacity of queue
     */
    public void testLength() {
        assertEquals(11, queue1.getLength());
    }


    /**
     * this method tests the size
     */
    public void testGetSize() {
        assertEquals(4, queue1.getSize());
    }


    /**
     * tests the is full and capacity methods
     */
    public void testEnsureCapacity() {
        for (int i = 0; i < 24; i++) {
            queue2.enqueue("a");
        }
        queue2.enqueue("b");
        assertEquals("a", queue2.getFront());
        queue2.dequeue();
        assertEquals(44, queue2.getLength());
        assertEquals(24, queue2.getSize());
    }


    /**
     * tests the enqueue.method
     */
    public void testEnqueue() {
        assertEquals(11, queue1.getLength());
        assertEquals("a", queue1.getFront());
        queue2.enqueue("a");
        queue2.enqueue("b");
        queue1.dequeue();
        queue1.dequeue();
        queue1.dequeue();
        assertEquals("d", queue1.getFront());
    }


    /**
     * tests the is empty method
     */
    public void testIsEmpty() {
        assertTrue(queue2.isEmpty());
        assertFalse(queue1.isEmpty());
        queue1.clear();

    }


    /**
     * tests the dequeue method
     */
    public void testDequeue() {
        assertEquals("a", queue1.dequeue());
        assertEquals(3, queue1.getSize());
    }


    /**
     * This method will test the getFront method in the ArrayQueue class.
     */
    public void testGetFront() {
        assertEquals("a", queue1.getFront());
    }


    /**
     * tests the to array method
     */
    public void testToArray() {
        Object[] array = queue1.toArray();
        assertEquals(4, array.length);
        assertEquals("a", array[0]);
    }


    /**
     * tests the to string method
     */
    public void testToString() {
        assertEquals("[a, b, c, d]", queue1.toString());
        assertEquals("[]", queue2.toString());
    }


    /**
     * tests the equals methods
     */
    public void testEquals() {
        queue2.enqueue("a");
        queue2.enqueue("b");
        queue2.enqueue("c");
        queue2.enqueue("d");
        assertTrue(queue1.equals(queue2));
    }
}
