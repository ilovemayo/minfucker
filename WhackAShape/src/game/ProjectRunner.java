package game;

/**
 * ProjectRunner is the driver class
 * 
 * @author davidweisiger dbw
 * @version 10.03.17
 *
 */

public class ProjectRunner {

    /**
     * the main method takes in an input of strings to run for the WhackAShape
     * game
     */

    public static void main(String[] args) {
        @SuppressWarnings("unused")
        WhackAShape whack;
        /*
         * test input
         * 
         * String[] input = { "red circle", "blue circle", "red square",
         * "blue square", "red circle", "blue circle", "red square",
         * "blue square" };
         */

        if (args.length > 10) {
            whack = new WhackAShape(args);
        }
        else {
            whack = new WhackAShape();
        }
    }

}
