package employees;

/**
 * this calss test the external contractor
 * 
 * @author davidweisiger
 * @version september 8, 2017
 *
 */
public class ExternalContractorTest extends student.TestCase {
    /**
     * field variables
     */
    ExternalContractor test;


    /**
     * sets up objects
     */
    public void setUp() {
        test = new ExternalContractor("bob", 2);
    }


    /**
     * tests getting weekly pay
     */

    public void testGetHourlyRate() {
        assertEquals(test.getHourlyRate(2), 41.75, .01);
        assertEquals(test.getHourlyRate(1), 38.50, .01);
        assertEquals(test.getHourlyRate(3), 45.50, .01);
    }


    /**
     * tests the next method
     */
    public void testGetWeeklyPay() {
        assertEquals(test.weeklyPay(2, 4), 91, .01);
    }

}
