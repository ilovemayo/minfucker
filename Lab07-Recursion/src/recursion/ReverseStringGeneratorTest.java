package recursion;

/**
 * 
 * @author davidweisiger dbw
 * @version 10.14.17
 *
 */

public class ReverseStringGeneratorTest extends student.TestCase {

    /**
     * set up
     */

    public void setUp() {
        new ReverseStringGenerator();
    }


    /**
     * tests the fib
     */

    public void testReverse() {
        assertEquals("gnojes", ReverseStringGenerator.reverse("sejong"));
        assertEquals(null, ReverseStringGenerator.reverse(null));
    }
}
