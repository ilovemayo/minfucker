package cybersecurity.lab2;

import java.util.HashMap;

/**
 * A Least Recently Used (LRU) Cache Data Structure. Uses a HashMap to lookup
 * items and a LinkedList (LinkedKVStore) to maintain usage order.
 * 
 * @author back
 * @version March 2017
 * @author maellis1
 * @version 06.04.2017
 *
 * @param <K>
 *            the type of the key
 * @param <V>
 *            the type of the value
 */
public class LRUCache<K, V> implements LRUCacheInterface<K, V> {

    private int capacity;
    private HashMap<K, LinkedKVStoreInterface.Entry<K, V>> map;
    private LinkedKVStore<K, V> list;
    private int size;


    /**
     * LRUCache constructor
     * 
     * @param capacity
     *            the maximum capacity of the cache
     */
    public LRUCache(int capacity) {
        this.capacity = capacity;
        map = new HashMap<>();
        list = new LinkedKVStore<>();
        size = 0;
    }


    /**
     * Returns true if this cache contains no elements.
     * 
     * @return true if this cache contains no elements
     */
    @Override
    public boolean isEmpty() {
        return size() == 0;
    }


    /**
     * Returns the number of elements in this cache.
     * 
     * @return the number of elements in this cache
     */
    @Override
    public int size() {
        return size;
    }


    /**
     * Removes all of the elements from this cache. The cache will be empty
     * after this call returns.
     */
    @Override
    public void clear() {
        list.clear();
        size = 0;
    }


    /**
     * Returns the value associated with this key and moves the node to the
     * front of the order list, if no value is associated with this key, return
     * null
     * 
     * @param key
     *            a key in this cache
     * @return the value to which the key is mapped in this cache, or null if
     *         the key is not in the cache
     */
    @Override
    public V get(K key) {
        LinkedKVStoreInterface.Entry<K, V> entry = map.get(key);
        if (entry == null) {
            return null;
        }
        else {
            entry.moveToFront();
            return entry.getValue();
        }
    }


    /**
     * Adds the specified key and value to the map and order list, if the key
     * already exists then replace the value of that key. If the cache is full
     * then evict an item to make room for this new one.
     * 
     * @param key
     *            a key in this cache
     * @param value
     *            a value in the cache
     */
    @Override
    public void put(K key, V value) {
        // declare node to hold returned KVnode
        LinkedKVStoreInterface.Entry<K, V> existingEntry = map.get(key);

        if (size == capacity) {
            evict();
        }
        else if (existingEntry != null) {
            map.remove(existingEntry.getKey());
            existingEntry.remove();
            size--;
        }

        LinkedKVStoreInterface.Entry<K, V> entry = list.addFirst(key, value);
        map.put(key, entry);
        size++;
    }


    /**
     * Removes the last item in the order list and the corresponding data from
     * the map
     * 
     */
    private void evict() {
        K tmp = list.removeLast().getKey();
        map.remove(tmp);
        size--;
    }


    /**
     * Returns a string representation of the order list
     * 
     * @return a string representation of the order list according to its
     *         toString method
     */
    @Override
    public String toString() {
        return list.toString();
    }
}
