package cybersecurity.lab2;

/**
 * Test class for testing LRUCache
 * 
 * @author saima
 * @version 04.10.2017
 *
 */
public class LRUCacheTest extends student.TestCase {

    private LRUCache<String, String> cache;


    /**
     * setUp() a cache with capacity 3
     */
    public void setUp() {
        cache = new LRUCache<String, String>(3);
    }


    /**
     * test put() and get()
     */
    public void test1() {
        assertNull(cache.get("1"));
        cache.put("1", "one");
        assertEquals(cache.get("1"), "one");
        cache.put("2", "two");
        assertEquals(cache.get("1"), "one");
        assertEquals(cache.get("2"), "two");
    }


    /**
     * test put()
     */
    public void test2() {
        cache.put("1", "v");
        cache.put("2", "v");
        cache.put("3", "v");
        cache.put("4", "v");
        assertEquals(
            "Forward: [4=v],[3=v],[2=v]\nBackward: [2=v],[3=v],[4=v]\n", cache
                .toString());
    }


    /**
     * test put()
     */
    public void test3() {
        cache.put("1", "v");
        cache.put("2", "v");
        cache.put("3", "v");
        cache.put("4", "v");
        cache.get("4");
        cache.put("5", "v");
        assertEquals(
            "Forward: [5=v],[4=v],[3=v]\nBackward: [3=v],[4=v],[5=v]\n", cache
                .toString());
    }


    /**
     * test size() when cache size reaches capacity
     */
    public void testSize() {
        assertEquals(0, cache.size());
        assertTrue(cache.isEmpty());
        cache.put("1", "one");
        assertEquals(1, cache.size());
        cache.put("1", "newvalue");
        assertEquals(1, cache.size());
        cache.put("2", "newvalue");
        cache.put("3", "newvalue");
        cache.put("4", "newvalue");
        assertEquals(3, cache.size());
    }


    /**
     * test clear()
     */
    public void testIsEmpty() {
        assertTrue(cache.isEmpty());
        cache.put("1", "v");
        assertFalse(cache.isEmpty());
        cache.put("1", "v1");
        assertFalse(cache.isEmpty());
        cache.clear();
        assertTrue(cache.isEmpty());
    }

}
