package arraystack;

import java.util.Arrays;
import java.util.EmptyStackException;

/**
 * @author davidweisiger
 * 
 * @version friday, october 6, 2017
 */

public class ArrayBasedStackTest extends student.TestCase {
    /**
     * private variables
     */
    private ArrayBasedStack<String> stack;
    private ArrayBasedStack<String> stack2;
    private ArrayBasedStack<String> stack3;
    private ArrayBasedStack<String> stack4;


    /**
     * sets stuff up
     */
    public void setUp() {
        stack4 = new ArrayBasedStack<>();
        stack = new ArrayBasedStack<>(10);
        stack2 = new ArrayBasedStack<>(10);
        stack3 = new ArrayBasedStack<>(2);
    }


    /**
     * tests the isempty method
     */
    public void testIsEmpty() {
        assertTrue(stack4.isEmpty());
        assertTrue(stack.isEmpty());
        stack.push(1);
        assertFalse(stack.isEmpty());
    }


    /**
     * tests the peek method
     */
    public void testPeek() {

        Exception thrown = null;
        try {
            stack.peek();
        }
        catch (Exception e) {
            thrown = e;
        }

        assertNotNull(thrown);
        assertTrue(thrown instanceof EmptyStackException);

        stack.push(1);
        assertEquals(1, stack.peek());
    }


    /**
     * tests the pop method
     */
    public void testPop() {

        Exception thrown = null;
        try {
            stack.pop();
        }
        catch (Exception e) {
            thrown = e;
        }

        assertNotNull(thrown);
        assertTrue(thrown instanceof EmptyStackException);

        stack.push(1);
        assertEquals(1, stack.pop());
    }


    /**
     * tests the push method
     */
    public void testPush() {
        stack3.push(1);
        stack3.push(1);
        stack3.push(1);
        stack3.push(1);
        stack.push(1);
        assertEquals(1, stack.pop());
    }


    /**
     * tests the contains method
     */
    public void testContains() {
        assertFalse(stack.contains(0));
        stack.push(1);
        assertTrue(stack.contains(1));
        assertFalse(stack.contains(0));
    }


    /**
     * tests the size method
     */
    public void testSize() {
        assertEquals(0, stack.size());
    }


    /**
     * Clears the stack (removes all of the items from the stack).
     */
    public void testClear() {
        stack.push(1);
        stack.clear();
        assertTrue(stack.isEmpty());
    }


    /**
     * tests the to array method
     */
    public void testToArray() {
        stack2.push(1);
        stack.push(1);
        assertTrue(Arrays.equals(stack2.toArray(), stack.toArray()));
    }


    /**
     * tests the tostring
     */
    public void testToString() {
        String expected = "[null null null null null null null null null null]";
        assertEquals(expected, stack.toString());
    }

    public void testDebuggerViews()

    {

                //Put a breakpoint on the line below

                ArrayBasedStack<String> testStack = new ArrayBasedStack<String>();

                 

                //Put a breakpoint on the line below. Use Step Over to see each push.

                testStack.push("blizzard");

                testStack.push("barrage");

                testStack.push("deadeye");

                testStack.push("resurrect");

                 

                assertTrue(testStack.toString().equals("[blizzard, barrage, deadeye, resurrect]"));

                 

                //Put a breakpoint on the line below. Hit Step Over once to watch the pop.

                testStack.pop();

                 

                assertTrue(testStack.toString().equals("[blizzard, barrage, deadeye]"));

                 

                Object[] toArrayResult = testStack.toArray();

                //Drop a breakpoint on the line below.

                //Use the debugger mode to compare toArrayResult to testStack.

                assertTrue(toArrayResult[0].toString().equals("blizzard"));

                assertEquals(toArrayResult.length, 3);

                 

                //The following test fails because the stack still has entries in it. However,

                //"expected <true> but was: <false>" is not very helpful.

                //Drop a breakpoint on the line below to see what the toString SHOULD look like.

                assertTrue(testStack.toString().equals("[]"));

    }

    /**
     * tests the equals method
     */
    public void testEquals() {
        ArrayBasedStack<String> stack5 = new ArrayBasedStack<>(10);
        String blah = "";
        String blahnull = null;

        assertTrue(stack.equals(stack));
        assertFalse(stack.equals(blahnull));
        assertFalse(stack.equals(blah));
        assertTrue(stack.equals(stack5));
        stack.push(1);
        assertFalse(stack.equals(stack5));
        stack5.push(1);
        stack5.push(2);
        stack.push(2);
        assertTrue(stack.equals(stack5));
        
        ArrayBasedStack<String> stack01 = new ArrayBasedStack<>(10);
        ArrayBasedStack<String> stack02 = new ArrayBasedStack<>(10);
        stack01.push(0);
        stack01.push(1);
        stack01.push(2);
        
        stack02.push(0);
        stack02.push(1);
        stack02.push(3);
        assertFalse(stack01.equals(stack02));
        
    }
}
