package prj5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * This will process the musicsurveydata file and songlist file
 * and rewrite the string into the form our classes can read
 * 
 * @author davidbauer
 * @author Yue Yang
 * @author david weisiger
 * @version 11.18.19
 */
public class SurveyProcessor {
    private File surveyData;
    private File songList;
    private LinkedList<SongInfo> songs;
    private ArrayList<Integer> yesNo;


    /**
     * Create a new SurveyProcessor
     * 
     * @param surveyData
     *            the file we will proceed
     * @param songList
     *            the file we will proceed
     */
    public SurveyProcessor(File surveyData, File songList) {
        this.surveyData = surveyData;
        this.songList = songList;
        songs = new LinkedList<>();
        yesNo = new ArrayList<>();

        readSongData();
        readSurveyData();
    }


    /**
     * reads survey data
     */
    private void readSurveyData() {
        try {
            Scanner sc = new Scanner(surveyData);
            sc.useDelimiter(",|\r");
            // skip the column heading
            sc.nextLine();

            while (sc.hasNextLine()) {
                // skips to major column
                sc.next();
                sc.next();
                String major = sc.next();
                String region = sc.next();
                // get the hobby
                String hobby = sc.next();
                // get the rest of the line that says whether they liked or
                // disliked a song
                String yesNoLine = sc.nextLine();
                updateSongArray(major, region, hobby, yesNoLine);

            }
            sc.close();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


    /**
     * updates song array
     * 
     * @param hobby
     *            the hobby string needed to be edited
     * @param yesNoLine
     *            the string after delete yes and no
     */
    private void updateSongArray(
        String major,
        String region,
        String hobby,
        String yesNoLine) {
        // clear yesNo
        yesNo.clear();
        String[] yesNoArray = yesNoLine.split(",", -1);
        // start at 1 because first entry is empty
        for (int i = 1; i < yesNoArray.length; i++) {
            if (yesNoArray[i].equalsIgnoreCase("yes")) {
                yesNo.add(1);
            }
            else if (yesNoArray[i].equalsIgnoreCase("no")) {
                yesNo.add(0);
            }
            else {
                yesNo.add(-1);
            }
        }

        // update songs, needs to go check two indices of yesNo before iterating
        // through songs
        int songNum = 0;
        for (int i = 0; i < yesNo.size() - 1; i += 2) {

            int heardSong = yesNo.get(i);
            int likedSong = yesNo.get(i + 1);

            // iterate the number of people who answered
            if (heardSong == 1 || heardSong == 0) {
                songs.get(songNum).iteratePeopleHeard(major, region, hobby);
            }
            if (likedSong == 1 || likedSong == 0) {
                songs.get(songNum).iteratePeopleLiked(major, region, hobby);
            }

            if (heardSong == 1) {
                songs.get(songNum).iterateHeard(major, region, hobby);
            }
            if (likedSong == 1) {
                songs.get(songNum).iterateLiked(major, region, hobby);
            }

            songNum++;
        }
    }


    /**
     * reads song data
     */
    private void readSongData() {
        try {
            // create scanner and delimit by commas
            Scanner sc = new Scanner(songList);

            // this delimiter works
            sc.useDelimiter(",|\r|\n");

            // skip the column heading
            sc.nextLine();
            while (sc.hasNextLine()) {
                String title = sc.next();
                String artist = sc.next();
                int year = sc.nextInt();
                String genre = sc.next();
                songs.add(new SongInfo(title, artist, year, genre));
            }

            sc.close();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }


    /**
     * get the song list
     * 
     * @return list of songs
     */
    public LinkedList<SongInfo> getSongs() {
        return songs;
    }

}
