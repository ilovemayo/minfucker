package prj5;

/**
 * enum classs
 * 
 * @author davidbauer
 * @author Yue Yang
 * @author david weisiger
 * @version 11.18.17
 */

public enum HobbyEnum {
    /**
     * private variables and declares enum
     */
    READING("reading"), ART("art"), SPORTS("sports"), MUSIC("music");

    private String name;


    /**
     * Create a new HobbyEnum
     * 
     * @param name
     *            of enum
     */
    HobbyEnum(String name) {
        this.name = name;
    }


    /**
     * Find the name of the enum
     * @return name of enum
     */
    public String getName() {
        return name;
    }

}
