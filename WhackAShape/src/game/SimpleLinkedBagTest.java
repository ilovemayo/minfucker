package game;

/**
 * Test class for LinkedBag
 * 
 * @author davidweisiger dbw
 * @version 10.01.17
 */

public class SimpleLinkedBagTest extends student.TestCase {
    /**
     * field variables
     */
    private SimpleLinkedBag<String> test;


    /**
     * sets up test objects
     */
    @Override
    public void setUp() {
        test = new SimpleLinkedBag<>();
    }


    /**
     * tests the add method
     */
    public void testAdd() {
        assertFalse(test.add(null));
        assertTrue(test.add("hot"));
        assertTrue(test.add("blah"));
    }


    /**
     * tests the getCurrentSize method
     */
    public void testGetCurrentSize() {
        assertEquals(0, test.getCurrentSize());
        test.add("blah");
        test.add("blah2");
        assertEquals(2, test.getCurrentSize());
    }


    /**
     * tests the isEmpty method
     */
    public void testIsEmpty() {
        assertTrue(test.isEmpty());
        test.add("blah3");
        assertFalse(test.isEmpty());
    }


    /**
     * tests the pick method
     */
    public void testPick() {
        assertEquals(null, test.pick());
        test.add("blah");
        test.add("blah2");
        test.add("blah3");
        test.add("blah4");
        assertEquals("blah4", test.pick());
        assertEquals("blah3", test.pick());
        assertEquals("blah2", test.pick());
        assertEquals("blah", test.pick());
    }


    /**
     * tests the remove method
     */
    public void testRemove() {
        test.add("blah");
        test.remove("blah");
        assertTrue(test.isEmpty());
        assertFalse(test.remove("blah"));

        test.add("blah");
        test.add("blah");
        test.add("blah");
        test.add("blah1");
        test.add("blah");
        test.add("blah");
        assertTrue(test.remove("blah1"));
    }
}
