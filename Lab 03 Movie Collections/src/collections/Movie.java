package collections;

/**
 * @author David Weisiger dbw
 * @version September 15, 2017
 */
public class Movie extends MovieADT {

    /**
     * this just calls the superclass
     * @param param gets from super
     */
    public Movie(String param) {
        super(param);
    }
    
    
}
