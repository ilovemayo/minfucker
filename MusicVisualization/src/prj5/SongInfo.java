package prj5;

/**
 * This class will include every information for each song
 * and will generate percentage of people for each sorted type
 * 
 * @author davidbauer
 * @author Yue Yang
 * @author david weisiger
 * @version 11.18.17
 */
public class SongInfo {
    private String title;
    private String artist;
    private int year;
    private String genre;

    /**
     * DESCRIPTION OF 2D ARRAYS:
     * - Each row is major, region, and hobby respectively
     * - Each column is the CS, Other engineering, Math/CDMA, Other
     * NE, SE, Other US, Outside US
     * Reading, art, sports music
     * *Respectively*
     */

    // this 2d array holds the number of people who answered for liked or heard
    // survey TOTAL
    private int[][] numAnsweredLiked = { { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0,
        0, 0 } };
    private int[][] numAnsweredHeard = { { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0,
        0, 0 } };

    // this 2d array holds the number of people who actually heard or liked
    private int[][] numLiked = { { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0,
        0 } };
    private int[][] numHeard = { { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0,
        0 } };

    // these 2d arrays hold the percent people who liked or heard
    private int[][] percentLiked = { { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0,
        0 } };
    private int[][] percentHeard = { { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0,
        0 } };


    /**
     * Creating a new song with the title, artist, year and genre
     * 
     * @param title
     *            of a String
     * @param artist
     *            of a String
     * @param year
     *            of an int
     * @param genre
     *            of a String
     */
    public SongInfo(String title, String artist, int year, String genre) {
        this.title = title;
        this.artist = artist;
        this.year = year;
        this.genre = genre;
    }


    /**
     * iterates people liked
     * 
     * @param hobby
     *            of a String
     */
    public void iteratePeopleLiked(String major, String region, String hobby) {
        // iterate the majors that liked the song
        if (major.equals("Computer Science")) {
            numAnsweredLiked[0][0]++;
        }
        else if (major.equals("Other Engineering")) {
            numAnsweredLiked[0][1]++;
        }
        else if (major.equals("Math or CMDA")) {
            numAnsweredLiked[0][2]++;
        }
        else if (major.equals("Other")) {
            numAnsweredLiked[0][3]++;
        }

        // iterate the regions that liked the song
        if (region.equals("Northeast")) {
            numAnsweredLiked[1][0]++;
        }
        else if (region.equals("Southeast")) {
            numAnsweredLiked[1][1]++;
        }
        else if (region.equals(
            "United States (other than Southeast or Northwest)")) {
            numAnsweredLiked[1][2]++;
        }
        else if (region.equals("Outside of United States")) {
            numAnsweredLiked[1][3]++;
        }

        // iterate the hobbies that liked the song
        if (hobby.equals("reading")) {
            numAnsweredLiked[2][0]++;
        }
        else if (hobby.equals("art")) {
            numAnsweredLiked[2][1]++;
        }
        else if (hobby.equals("sports")) {
            numAnsweredLiked[2][2]++;
        }
        else if (hobby.equals("music")) {
            numAnsweredLiked[2][3]++;
        }

    }


    /**
     * iterates people heard
     * 
     * @param hobby
     *            of heard
     */
    public void iteratePeopleHeard(String major, String region, String hobby) {
        // iterate the majors that heard the song
        if (major.equals("Computer Science")) {
            numAnsweredHeard[0][0]++;
        }
        else if (major.equals("Other Engineering")) {
            numAnsweredHeard[0][1]++;
        }
        else if (major.equals("Math or CMDA")) {
            numAnsweredHeard[0][2]++;
        }
        else if (major.equals("Other")) {
            numAnsweredHeard[0][3]++;
        }

        // iterate the regions that heard the song
        if (region.equals("Northeast")) {
            numAnsweredHeard[1][0]++;
        }
        else if (region.equals("Southeast")) {
            numAnsweredHeard[1][1]++;
        }
        else if (region.equals(
            "United States (other than Southeast or Northwest)")) {
            numAnsweredHeard[1][2]++;
        }
        else if (region.equals("Outside of United States")) {
            numAnsweredHeard[1][3]++;
        }

        // iterate the hobbies that heard the song
        if (hobby.equals("reading")) {
            numAnsweredHeard[2][0]++;
        }
        else if (hobby.equals("art")) {
            numAnsweredHeard[2][1]++;
        }
        else if (hobby.equals("sports")) {
            numAnsweredHeard[2][2]++;
        }
        else if (hobby.equals("music")) {
            numAnsweredHeard[2][3]++;
        }
    }


    /**
     * iterates counters
     * 
     * @param hobby
     *            of a String
     * @param heardOrLiked
     *            of a String
     */

    public void iterateHeard(String major, String region, String hobby) {

        // iterate majors
        if (major.equals("Computer Science")) {
            numHeard[0][0]++;
        }
        else if (major.equals("Other Engineering")) {
            numHeard[0][1]++;
        }
        else if (major.equals("Math or CMDA")) {
            numHeard[0][2]++;
        }
        else if (major.equals("Other")) {
            numHeard[0][3]++;
        }

        // iterate regions
        if (region.equals("Northeast")) {
            numHeard[1][0]++;
        }
        else if (region.equals("Southeast")) {
            numHeard[1][1]++;
        }
        else if (region.equals(
            "United States (other than Southeast or Northwest)")) {
            numHeard[1][2]++;
        }
        else if (region.equals("Outside of United States")) {
            numHeard[1][3]++;
        }

        // iterate hobbies
        if (hobby.equalsIgnoreCase("reading")) {
            numHeard[2][0]++;
        }
        else if (hobby.equalsIgnoreCase("art")) {
            numHeard[2][1]++;
        }
        else if (hobby.equalsIgnoreCase("sports")) {
            numHeard[2][2]++;
        }
        else if (hobby.equalsIgnoreCase("music")) {
            numHeard[2][3]++;
        }
    }


    public void iterateLiked(String major, String region, String hobby) {
        // iterate majors
        if (major.equals("Computer Science")) {
            numLiked[0][0]++;
        }
        else if (major.equals("Other Engineering")) {
            numLiked[0][1]++;
        }
        else if (major.equals("Math or CMDA")) {
            numLiked[0][2]++;
        }
        else if (major.equals("Other")) {
            numLiked[0][3]++;
        }

        // iterate regions
        if (region.equals("Northeast")) {
            numLiked[1][0]++;
        }
        else if (region.equals("Southeast")) {
            numLiked[1][1]++;
        }
        else if (region.equals(
            "United States (other than Southeast or Northwest)")) {
            numLiked[1][2]++;
        }
        else if (region.equals("Outside of United States")) {
            numLiked[1][3]++;
        }

        // iterate hobbies
        if (hobby.equalsIgnoreCase("reading")) {
            numLiked[2][0]++;
        }
        else if (hobby.equalsIgnoreCase("art")) {
            numLiked[2][1]++;
        }
        else if (hobby.equalsIgnoreCase("sports")) {
            numLiked[2][2]++;
        }
        else if (hobby.equalsIgnoreCase("music")) {
            numLiked[2][3]++;
        }
    }


    public void setPercentValues() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 4; j++) {
                if (numAnsweredHeard[i][j] != 0) {
                    percentHeard[i][j] = (int)(((double)numHeard[i][j]
                        / (double)numAnsweredHeard[i][j]) * 100);
                }
                if (numAnsweredLiked[i][j] != 0) {
                    percentLiked[i][j] = (int)(((double)numLiked[i][j]
                        / (double)numAnsweredLiked[i][j]) * 100);
                }
            }
        }
    }


    /**
     * overrides object tostring method
     */
    @Override
    public String toString() {
        setPercentValues();
        StringBuilder sb = new StringBuilder();
        sb.append("Song Title: " + title);
        sb.append("\nSong Artist: " + artist);
        sb.append("\nSong Genre: " + genre);
        sb.append("\nSong Year: " + year);

        // people who heard
        sb.append("\nHeard\n");
        sb.append("reading:" + percentHeard[1][0] + " art:" + percentHeard[1][1]
            + " sports:" + percentHeard[1][2] + " music:" + percentHeard[1][3]);

        // people who liked
        sb.append("\nLikes\n");
        sb.append("reading:" + percentLiked[1][0] + " art:" + percentLiked[1][1]
            + " sports:" + percentLiked[1][2] + " music:" + percentLiked[1][3]);

        return sb.toString();
    }


    /**
     * compares artist
     * 
     * @param comp
     *            is that
     * @return artist difference
     */
    public int compareToArtist(SongInfo comp) {
        if (comp == null) {
            throw new IllegalArgumentException();
        }
        int temp = this.artist.compareTo(comp.artist);
        return temp;
    }


    /**
     * compares year
     * 
     * @param comp
     *            is that
     * @return year difference
     */
    public int compareToYear(SongInfo comp) {
        if (comp == null) {
            throw new IllegalArgumentException();
        }
        int temp = this.year - comp.year;
        return temp;
    }


    /**
     * compares genres
     * 
     * @param comp
     *            is that
     * @return genre difference
     */
    public int compareToGenre(SongInfo comp) {
        if (comp == null) {
            throw new IllegalArgumentException();
        }
        int temp = this.genre.compareTo(comp.genre);
        return temp;
    }


    /**
     * compares titles
     * 
     * @param comp
     *            is that
     * @return title difference
     */
    public int compareToTitle(SongInfo comp) {
        if (comp == null) {
            throw new IllegalArgumentException();
        }
        int temp = this.title.compareTo(comp.title);
        return temp;
    }


    public int[][] getPercentLiked() {
        return percentLiked;
    }


    public int[][] getPercentHeard() {
        return percentHeard;
    }


    public String getTitle() {
        // TODO Auto-generated method stub
        return title;
    }

}
