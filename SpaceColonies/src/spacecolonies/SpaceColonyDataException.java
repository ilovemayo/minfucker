package spacecolonies;

/**
 * Custom Exception class, suppresses warning because no need for serialization
 * 
 * @author dbw
 * @version 11.16.17
 *
 */
@SuppressWarnings("serial")
public class SpaceColonyDataException extends Exception {
    /**
     * default constructor calls parent class constructor
     */
    public SpaceColonyDataException() {
        super();
    }
}
