package groceries;

import java.io.*;
import java.util.Scanner;

/**
 * Practice reading and writing to a file.
 * 
 * @author David Weisiger dbw
 * @version 9.22.2017
 */
public class FileIO {
    /**
     * Reads groceries from a file and adds them to a GroceryBag object,
     * the writes the groceries back to a file using GroceryBag's
     * toString method.
     * 
     * @param args
     *            command line arguments.
     */
    public static void main(String[] args) {
        File filename = new File("grocerylist.txt");
        GroceryBag gb = new GroceryBag();
        try {
            Scanner sc = new Scanner(filename);
            while (sc.hasNext()) {
                gb.add(sc.next());
            }
            sc.close();

            PrintWriter pw = new PrintWriter("output.txt");
            pw.print(gb.toString());
            pw.close();
        }
        catch (FileNotFoundException e) {
            System.out.println("Scanner error opening the file ");
            System.out.println(e.getMessage());
        }
    }
}
