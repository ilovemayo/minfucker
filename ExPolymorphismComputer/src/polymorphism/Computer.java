package polymorphism;

public class Computer {
    
    
    //private Memory mem;
    private String manufacturer;
    private String processor;
    private int ramSize;
    private int diskSize;
    private double processorSpeed;
    
    public Computer(String manuf, String proc, int ram, int disk, double prcSpeed){
        manufacturer = manuf;
        processor = proc;
        ramSize = ram;
        diskSize = disk;
        processorSpeed = prcSpeed;
    }
    
    public int getRamSize(){
        return ramSize;
    }
    
    public int getDiskSize(){
        return diskSize;
    }
    
    public double getProcessorSpeed(){
        return processorSpeed;     
    }
    
    public double computePower(){
        //no variable was listed for this in UML
        return 1.7;
        
    }
    
    @Override
    public String toString(){ 
        return "manufacturer: " + manufacturer + " " + 
               "\nprocessor: " + processor + " "+ 
                "\nramSize: " + ramSize + " " + 
                "\ndiskSize: " + diskSize + " " + 
                "\nprocessorSpeed: " + processorSpeed;
        
    }
    


}
