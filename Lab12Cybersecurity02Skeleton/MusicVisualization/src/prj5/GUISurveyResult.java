package prj5;

import java.awt.Color;
import CS2114.Shape;
import CS2114.TextShape;
import CS2114.Button;
import CS2114.Window;
import CS2114.WindowSide;

/**
 * GUI
 * 
 * @author davidb21, dbw, yy96
 * @version 11.15.17
 */
public class GUISurveyResult {
    /**
     * instance variables
     */

    @SuppressWarnings("unused")
    private SurveyProcessor surveyProcessor;

    private Shape rectangle;
    private Shape rectangle1;
    private Shape rectangle2;
    private Shape rectangle3;
    private Shape rectangle4;
    private Shape rectangle5;
    private Shape rectangle6;
    private Shape rectangle7;
    private Shape middleRec;

    private Window window;

    private Button prev;
    private Button next;
    private Button sortByArtist;
    private Button sortByTitle;
    private Button sortByYear;
    private Button sortByGenre;
    private Button representByHobby;
    private Button representByMajor;
    private Button representByRegion;
    private Button quit;


    /**
     * default constructor
     */
    public GUISurveyResult(SurveyProcessor surveyProcessor) {
        this.surveyProcessor = surveyProcessor;

        window = new Window("Project 5 - Music Preference Visualization");

        buildGlyph(5);
        createLegend();
        prev = new Button("<- Prev");
        prev.onClick(this, "clickedPrev");

        next = new Button("Next ->");
        next.onClick(this, "clickedNext");

        sortByArtist = new Button("Sort by Artist Name");
        sortByArtist.onClick(this, "clickedArtist");

        sortByTitle = new Button("Sort by Song Title");
        sortByTitle.onClick(this, "clickedTitle");

        sortByYear = new Button("Sort by Release Year");
        sortByYear.onClick(this, "clickedYear");

        sortByGenre = new Button("Sort by Genre");
        sortByGenre.onClick(this, "clickedGenre");

        representByHobby = new Button("Represent Hobby");
        representByHobby.onClick(this, "clickedHobby");

        representByMajor = new Button("Represent Major");
        representByMajor.onClick(this, "clickedMajor");

        representByRegion = new Button("Represent Region");
        representByRegion.onClick(this, "clickedRegion");

        quit = new Button("Quit");
        quit.onClick(this, "clickedQuit");

        addButtons();
    }


    /**
     * adds all buttons to window
     */
    private void addButtons() {
        window.addButton(prev, WindowSide.NORTH);
        window.addButton(sortByArtist, WindowSide.NORTH);
        window.addButton(sortByTitle, WindowSide.NORTH);
        window.addButton(sortByYear, WindowSide.NORTH);
        window.addButton(sortByGenre, WindowSide.NORTH);
        window.addButton(next, WindowSide.NORTH);

        window.addButton(representByHobby, WindowSide.SOUTH);
        window.addButton(representByMajor, WindowSide.SOUTH);
        window.addButton(representByRegion, WindowSide.SOUTH);
        window.addButton(quit, WindowSide.SOUTH);
    }


    /**
     * this method exits the program
     * 
     * @param quit
     *            button
     */
    public void clickedQuit(Button quit) {
        System.exit(0);
    }


    /**
     * adds all glyphs to window
     */
    public void addShapeAll() {
        window.addShape(middleRec);
        window.addShape(rectangle);
        window.addShape(rectangle1);
        window.addShape(rectangle2);
        window.addShape(rectangle3);
        window.addShape(rectangle4);
        window.addShape(rectangle5);
        window.addShape(rectangle6);
        window.addShape(rectangle7);
    }


    /**
     * builds the first part of glyph
     * 
     * @param numPeople
     *            total
     */
    public void buildGlyph(int numPeople) {
        middleRec = new Shape(100, 30, 10, 40, Color.BLACK);
        rectangle = new Shape(100 - 5 * numPeople, 30, 5 * numPeople, 10,
            Color.PINK);
        rectangle1 = new Shape(100 - 5 * numPeople, 40, 5 * numPeople, 10,
            Color.BLUE);
        rectangle2 = new Shape(100 - 5 * numPeople, 50, 5 * numPeople, 10,
            Color.YELLOW);
        rectangle3 = new Shape(100 - 5 * numPeople, 60, 5 * numPeople, 10,
            Color.GREEN);
        rectangle4 = new Shape(110, 30, 5 * numPeople, 10, Color.PINK);
        rectangle5 = new Shape(110, 40, 5 * numPeople, 10, Color.BLUE);
        rectangle6 = new Shape(110, 50, 5 * numPeople, 10, Color.YELLOW);
        rectangle7 = new Shape(110, 60, 5 * numPeople, 10, Color.GREEN);
        addShapeAll();
    }


    /**
     * builds the second part of glyph
     * 
     * @param numPeople
     *            total
     */
    public void buildGlyph2(int numPeople) {
        middleRec = new Shape(300, 30, 10, 40, Color.BLACK);
        rectangle = new Shape(300 - 5 * numPeople, 30, 5 * numPeople, 10,
            Color.PINK);
        rectangle1 = new Shape(300 - 5 * numPeople, 40, 5 * numPeople, 10,
            Color.BLUE);
        rectangle2 = new Shape(300 - 5 * numPeople, 50, 5 * numPeople, 10,
            Color.YELLOW);
        rectangle3 = new Shape(300 - 5 * numPeople, 60, 5 * numPeople, 10,
            Color.GREEN);
        rectangle4 = new Shape(310, 30, 5 * numPeople, 10, Color.PINK);
        rectangle5 = new Shape(310, 40, 5 * numPeople, 10, Color.BLUE);
        rectangle6 = new Shape(310, 50, 5 * numPeople, 10, Color.YELLOW);
        rectangle7 = new Shape(310, 60, 5 * numPeople, 10, Color.GREEN);
    }


    /**
     * builds part of glyph
     * 
     * @param numPeople
     *            total
     */
    public void buildGlyph3(int numPeople) {
        middleRec = new Shape(600, 30, 10, 40, Color.BLACK);
        rectangle = new Shape(600 - 5 * numPeople, 30, 5 * numPeople, 10,
            Color.PINK);
        rectangle1 = new Shape(600 - 5 * numPeople, 40, 5 * numPeople, 10,
            Color.BLUE);
        rectangle2 = new Shape(600 - 5 * numPeople, 50, 5 * numPeople, 10,
            Color.YELLOW);
        rectangle3 = new Shape(600 - 5 * numPeople, 60, 5 * numPeople, 10,
            Color.GREEN);
        rectangle4 = new Shape(610, 30, 5 * numPeople, 10, Color.PINK);
        rectangle5 = new Shape(610, 40, 5 * numPeople, 10, Color.BLUE);
        rectangle6 = new Shape(610, 50, 5 * numPeople, 10, Color.YELLOW);
        rectangle7 = new Shape(610, 60, 5 * numPeople, 10, Color.GREEN);
    }


    /**
     * builds part of glyph
     * 
     * @param numPeople
     *            total
     */
    public void buildGlyph4(int numPeople) {
        middleRec = new Shape(100, 100, 10, 40, Color.BLACK);
        rectangle = new Shape(100 - 5 * numPeople, 100, 5 * numPeople, 10,
            Color.PINK);
        rectangle1 = new Shape(100 - 5 * numPeople, 110, 5 * numPeople, 10,
            Color.BLUE);
        rectangle2 = new Shape(100 - 5 * numPeople, 120, 5 * numPeople, 10,
            Color.YELLOW);
        rectangle3 = new Shape(100 - 5 * numPeople, 130, 5 * numPeople, 10,
            Color.GREEN);
        rectangle4 = new Shape(110, 100, 5 * numPeople, 10, Color.PINK);
        rectangle5 = new Shape(110, 110, 5 * numPeople, 10, Color.BLUE);
        rectangle6 = new Shape(110, 120, 5 * numPeople, 10, Color.YELLOW);
        rectangle7 = new Shape(110, 130, 5 * numPeople, 10, Color.GREEN);
    }


    /**
     * builds part of glyph
     * 
     * @param numPeople
     *            total
     */
    public void buildGlyph5(int numPeople) {
        middleRec = new Shape(300, 100, 10, 40, Color.BLACK);
        rectangle = new Shape(300 - 5 * numPeople, 100, 5 * numPeople, 10,
            Color.PINK);
        rectangle1 = new Shape(300 - 5 * numPeople, 110, 5 * numPeople, 10,
            Color.BLUE);
        rectangle2 = new Shape(300 - 5 * numPeople, 120, 5 * numPeople, 10,
            Color.YELLOW);
        rectangle3 = new Shape(300 - 5 * numPeople, 130, 5 * numPeople, 10,
            Color.GREEN);
        rectangle4 = new Shape(310, 100, 5 * numPeople, 10, Color.PINK);
        rectangle5 = new Shape(310, 110, 5 * numPeople, 10, Color.BLUE);
        rectangle6 = new Shape(310, 120, 5 * numPeople, 10, Color.YELLOW);
        rectangle7 = new Shape(310, 130, 5 * numPeople, 10, Color.GREEN);
    }


    /**
     * builds part of glyph
     * 
     * @param numPeople
     *            total
     */
    public void buildGlyph6(int numPeople) {
        middleRec = new Shape(600, 100, 10, 40, Color.BLACK);
        rectangle = new Shape(600 - 5 * numPeople, 100, 5 * numPeople, 10,
            Color.PINK);
        rectangle1 = new Shape(600 - 5 * numPeople, 110, 5 * numPeople, 10,
            Color.BLUE);
        rectangle2 = new Shape(600 - 5 * numPeople, 120, 5 * numPeople, 10,
            Color.YELLOW);
        rectangle3 = new Shape(600 - 5 * numPeople, 130, 5 * numPeople, 10,
            Color.GREEN);
        rectangle4 = new Shape(610, 100, 5 * numPeople, 10, Color.PINK);
        rectangle5 = new Shape(610, 110, 5 * numPeople, 10, Color.BLUE);
        rectangle6 = new Shape(610, 120, 5 * numPeople, 10, Color.YELLOW);
        rectangle7 = new Shape(610, 130, 5 * numPeople, 10, Color.GREEN);
    }


    /**
     * builds part of glyph
     * 
     * @param numPeople
     *            total
     */
    public void buildGlyph7(int numPeople) {
        middleRec = new Shape(100, 200, 10, 40, Color.BLACK);
        rectangle = new Shape(100 - 5 * numPeople, 200, 5 * numPeople, 10,
            Color.PINK);
        rectangle1 = new Shape(100 - 5 * numPeople, 210, 5 * numPeople, 10,
            Color.BLUE);
        rectangle2 = new Shape(100 - 5 * numPeople, 220, 5 * numPeople, 10,
            Color.YELLOW);
        rectangle3 = new Shape(100 - 5 * numPeople, 230, 5 * numPeople, 10,
            Color.GREEN);
        rectangle4 = new Shape(110, 200, 5 * numPeople, 10, Color.PINK);
        rectangle5 = new Shape(110, 210, 5 * numPeople, 10, Color.BLUE);
        rectangle6 = new Shape(110, 220, 5 * numPeople, 10, Color.YELLOW);
        rectangle7 = new Shape(110, 230, 5 * numPeople, 10, Color.GREEN);
    }


    /**
     * builds part of glyph
     * 
     * @param numPeople
     *            total
     */
    public void buildGlyph8(int numPeople) {
        middleRec = new Shape(300, 200, 10, 40, Color.BLACK);
        rectangle = new Shape(300 - 5 * numPeople, 200, 5 * numPeople, 10,
            Color.PINK);
        rectangle1 = new Shape(300 - 5 * numPeople, 210, 5 * numPeople, 10,
            Color.BLUE);
        rectangle2 = new Shape(300 - 5 * numPeople, 220, 5 * numPeople, 10,
            Color.YELLOW);
        rectangle3 = new Shape(300 - 5 * numPeople, 230, 5 * numPeople, 10,
            Color.GREEN);
        rectangle4 = new Shape(310, 200, 5 * numPeople, 10, Color.PINK);
        rectangle5 = new Shape(310, 210, 5 * numPeople, 10, Color.BLUE);
        rectangle6 = new Shape(310, 220, 5 * numPeople, 10, Color.YELLOW);
        rectangle7 = new Shape(310, 230, 5 * numPeople, 10, Color.GREEN);
    }


    /**
     * builds part of glyph
     * 
     * @param numPeople
     *            total
     */
    public void buildGlyph9(int numPeople) {
        middleRec = new Shape(900, 200, 10, 40, Color.BLACK);
        rectangle = new Shape(900 - 5 * numPeople, 200, 5 * numPeople, 10,
            Color.PINK);
        rectangle1 = new Shape(900 - 5 * numPeople, 210, 5 * numPeople, 10,
            Color.BLUE);
        rectangle2 = new Shape(900 - 5 * numPeople, 220, 5 * numPeople, 10,
            Color.YELLOW);
        rectangle3 = new Shape(900 - 5 * numPeople, 230, 5 * numPeople, 10,
            Color.GREEN);
        rectangle4 = new Shape(910, 200, 5 * numPeople, 10, Color.PINK);
        rectangle5 = new Shape(910, 210, 5 * numPeople, 10, Color.BLUE);
        rectangle6 = new Shape(910, 220, 5 * numPeople, 10, Color.YELLOW);
        rectangle7 = new Shape(910, 230, 5 * numPeople, 10, Color.GREEN);
    }


    /**
     * builds the legend
     */
    public void createLegend() {
        Shape legend = new Shape(700, 400, 100, 300);
        window.addShape(legend);
        TextShape hlegend = new TextShape(750, 420, "Hobby Legend");
        window.addShape(hlegend);
        TextShape read = new TextShape(750, 430, "Read", Color.PINK);
        window.addShape(read);
        TextShape art = new TextShape(750, 440, "Art", Color.BLUE);
        window.addShape(art);
        TextShape sports = new TextShape(750, 450, "Sports", Color.YELLOW);
        window.addShape(sports);
        TextShape musics = new TextShape(750, 460, "Musics", Color.GREEN);
        window.addShape(musics);
        TextShape songtitle = new TextShape(800, 500, "Song Title",
            Color.BLACK);
        window.addShape(songtitle);
        Shape middleRod = new Shape(850, 510, 10, 50, Color.BLACK);
        window.addShape(middleRod);
        TextShape heard = new TextShape(780, 550, "Heard", Color.BLACK);
        window.addShape(heard);
        TextShape likes = new TextShape(920, 550, "Likes", Color.BLACK);
        window.addShape(likes);
    }

}
