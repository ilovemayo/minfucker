package polymorphism;

public class Driver {
    
    private static int inventoryCount = 1;
    
    //this method will work for any subclass of Computer, even ones that haven’t 
    //yet been conceived
    public static int inventoryComputer(Computer newAddition){
        System.out.println("Adding computer number " +  inventoryCount++);
        return inventoryCount;
    }
    
    public static void main(String[] args) {
        Computer myComputer = new Computer("Acme", "Intel", 2, 160, 2.4);
        Notebook yourComputer = new Notebook("DellGate", "AMD", 4, 240, 1.8, 15.0, 7.5);
        System.out.println("My computer is:\n" + myComputer.toString() +  "\n");
        System.out.println("Your computer is:\n" + yourComputer.toString()+  "\n");
        
        
        Computer refANotebook = new Notebook("DellGateNotebook", "AMD", 4, 240, 1.8, 15.0, 7.5);
        System.out.println("A Computer referencing a Notebook object:\n" + refANotebook.toString() +  "\n");
        
        //Look can't do this because the variable is a Computer!
        //comment out to run        
       // refANotebook.getWeight();
        //have to cast it, and notice the parens
        ((Notebook)refANotebook).getWeight();
        
        
        Computer anotherComputer = new Computer("Acme", "Intel", 2, 160, 2.4);
        Computer anotherLaptop = new Notebook("Acme", "Intel", 2, 160, 2.4,7,8);
        Notebook moreLaptop = new Notebook("Acme", "Intel", 2, 160, 2.4,7,8);
        
        //both reference type Notebook so okay
        yourComputer = moreLaptop;
        
        //Look can't do this because the reference types aren't the same
        //comment out to run        
       // yourComputer = anotherLaptop;
         
        //have to cast it, and notice the parens
        yourComputer = (Notebook) anotherLaptop;
           
        //allowed in this direction because "is-a"
        anotherLaptop = yourComputer;  
           
        //Look this cast is allowed, but it will throw an exception
        //JVM realizes the object anotherComputer references is
        //a Computer and not a Notebook
        //comment out to remove exception        
        yourComputer = (Notebook) anotherComputer;  
        
        //Because of polymorphism, can call this method with 
        //any subclass of Computer
        inventoryComputer(yourComputer);
        inventoryComputer(myComputer);
        
        
        
    }
        

}
