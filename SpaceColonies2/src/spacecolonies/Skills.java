/**
 * 
 */
package spacecolonies;

/**
 * This class will contain three ints for their skills(on a scale of 1 to 5).
 * 
 * @author Henry Argueta
 * @version 10/29/17
 */
public class Skills {

    private int agriculture;
    private int medicine;
    private int technology;

    public Skills(int ag, int med, int tech) {
        this.agriculture = ag;
        this.medicine = med;
        this.technology = tech;
    }

    public int getAgriculture() {
        return agriculture;
    }

    public int getMedicine() {
        return medicine;
    }

    public int getTechnology() {
        return technology;
    }

    public boolean isBelow(Skills other) {
        
        if (other == null)
        {
            return false;
        }
        


        return other.getAgriculture() < this.getAgriculture()  
                ||other.getMedicine() < this.getMedicine() 
                || other.getTechnology() < this.getTechnology();

    }

    public String toString() {
        String skillsNum = "A:" + getAgriculture() + " M:"
                + getMedicine() + " T:" + getTechnology();

        return skillsNum;
    }

    public boolean equals(Object obj) {
        if (obj == null)
        {
            return false;
        }
        Skills otherSkill = (Skills) obj;
        int total = (this.getAgriculture() - otherSkill.getAgriculture())
                + (this.getMedicine() - otherSkill.getMedicine())
                + (this.getTechnology() - otherSkill.getTechnology());

        return total == 0;
    }

}
