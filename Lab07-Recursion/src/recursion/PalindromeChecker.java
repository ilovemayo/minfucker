package recursion;

/**
 * 
 * @author davidweisiger
 * @version 10.15.17
 *
 */

public class PalindromeChecker {

    /**
     * is this a palindrome
     * 
     * @param str
     *            as input string
     * @return true or false
     */

    public static boolean isPalindrome(String str) {
        if (str == null) {
            return false;
        }
        str = str.replaceAll("[^a-zA-Z0-9]+", " ");
        str = str.replaceAll("\\s+", "");
        if (str.length() == 0 || str.length() == 1) {
            return true;
        }
        if (Character.toLowerCase(str.charAt(0)) == Character.toLowerCase(str
            .charAt(str.length() - 1))) {
            return isPalindrome(str.substring(1, str.length() - 1));
        }
        return false;
    }
}
