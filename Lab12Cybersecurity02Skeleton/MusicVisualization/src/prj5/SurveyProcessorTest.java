/**
 * 
 */
package prj5;

import java.io.File;

/**
 * @author davidbauer
 *
 */
public class SurveyProcessorTest extends student.TestCase {
    SurveyProcessor tester;


    /**
     * @throws java.lang.Exception
     */
    public void setUp() {
        tester = new SurveyProcessor(new File("InputFiles/MusicSurveyData.csv"),
            new File("InputFiles/SongList.csv"));
    }


    public void test() {
        assertEquals(tester.getSongs().get(0).getArtCountHeard(), 13);
    }

}
