package spacecolonies;

/**
 * This class drives the program and is able to take in variable filename input
 * 
 * @author dbw
 * @version 11.16.17
 *
 */
public class ProjectRunner {

    /**
     * main method
     * 
     * @param args
     *            that might contain filenames
     */
    public static void main(String[] args) {
        @SuppressWarnings("unused")
        ColonyReader colonyReader;

        // if custom files were provided, read these
        if (args.length == 2)
            colonyReader = new ColonyReader(args[0], args[1]);
        // otherwise use default txt files
        else
            colonyReader = new ColonyReader("input.txt", "planets.txt");
    }
}
