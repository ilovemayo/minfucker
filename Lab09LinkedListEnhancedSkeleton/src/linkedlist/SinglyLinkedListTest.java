package linkedlist;

import java.util.Arrays;
/**
 * tests singlylinkedlist
 * 
 * @author davidweisiger
 * @version october 27, 2017
 *
 */
public class SinglyLinkedListTest extends student.TestCase {
    /**
     * field variables
     */
    private SinglyLinkedList<String> list1;
    private SinglyLinkedList<String> emptyList;
    private Object[] fruitArray = { "apple", "banana", "mango", "kiwi" };
    private Object[] emptyArray = {};
    
    /**
     * setup method
     */


    public void setUp() {
        list1 = new SinglyLinkedList<String>();
        list1.add("apple");
        list1.add("banana");
        list1.add("mango");
        list1.add("kiwi");

        emptyList = new SinglyLinkedList<String>();
    }


    /**
     * tests the size method
     */
    public void testSize() {
        assertEquals(0, emptyList.size());
        emptyList.add("hi");
        assertEquals(1, emptyList.size());
        assertEquals(4, list1.size());
    }


    /**
     * tests the add method
     */
    public void testAdd() {
        Object[] array = { "apple", "banana", "hi", "mango", "kiwi" };

        assertTrue(Arrays.equals(emptyArray, emptyList.toArray()));
        emptyList.add("hi");
        assertFalse(Arrays.equals(emptyArray, emptyList.toArray()));
        assertTrue(Arrays.equals(fruitArray, list1.toArray()));
       // list1.add(0, "hi");
       // assertTrue(Arrays.equals(array, list1.toArray()));
        list1.add(3, "hi");
        assertFalse(Arrays.equals(array, list1.toArray()));

        Exception thrown = null;

        try {
            list1.add(null);
        }
        catch (Exception exception) {
            thrown = exception;
        }

        assertNotNull(thrown);
        assertTrue(thrown instanceof IllegalArgumentException);
        
        try {
            list1.add(0, null);
        }
        catch (Exception exception) {
            thrown = exception;
        }

        assertNotNull(thrown);
        assertTrue(thrown instanceof IllegalArgumentException);
        
        try {
            list1.add(10, "bro");
        }
        catch (Exception exception) {
            thrown = exception;
        }

        assertNotNull(thrown);
        assertTrue(thrown instanceof IndexOutOfBoundsException);
        
        try {
            list1.add(-5, "bro");
        }
        catch (Exception exception) {
            thrown = exception;
        }

        assertNotNull(thrown);
        assertTrue(thrown instanceof IndexOutOfBoundsException);

        try {
            list1.add(null);
        }
        catch (Exception exception) {
            thrown = exception;
        }

        assertNotNull(thrown);
        assertTrue(thrown instanceof IllegalArgumentException);

    }


    /**
     * tests the isEmpty method
     */
    public void testIsEmpty() {
        assertTrue(emptyList.isEmpty());
        assertFalse(list1.isEmpty());
    }


    /**
     * tests the remove method
     */
    public void testRemove() {
        SinglyLinkedList<String> bro = new SinglyLinkedList<>();
        bro.add("a");
        bro.add("b");
        bro.add("c");
        bro.add("d");
        bro.add("e");
        
        assertTrue(list1.remove(0));
        //assertTrue(list1.remove("apple"));
        assertTrue(list1.remove("mango"));
        assertFalse(emptyList.remove("banana"));
        assertFalse(list1.remove("pineapple"));
        assertTrue(list1.remove(0));
        list1.add("hi");
        list1.add("bye");
        list1.add("cya");
        assertTrue(list1.remove(0));

        Exception thrown = null;
        try {
            list1.remove(10);
        }
        catch (Exception exception) {
            thrown = exception;
        }

        assertNotNull(thrown);
        assertTrue(thrown instanceof IndexOutOfBoundsException);
        
        try {
            emptyList.remove(10);
        }
        catch (Exception exception) {
            thrown = exception;
        }

        assertNotNull(thrown);
        assertTrue(thrown instanceof IndexOutOfBoundsException);
        
        try {
            list1.remove(-5);
        }
        catch (Exception exception) {
            thrown = exception;
        }

        assertNotNull(thrown);
        assertTrue(thrown instanceof IndexOutOfBoundsException);
    }


    /**
     * tests the get method
     */
    public void testGet() {
        assertEquals("apple", list1.get(0));
        assertEquals("mango", list1.get(2));

        Exception thrown = null;
        try {
            list1.get(10);
        }
        catch (Exception exception) {
            thrown = exception;
        }

        assertNotNull(thrown);
        assertTrue(thrown instanceof IndexOutOfBoundsException);

    }


    /**
     * tests the contains method
     */
    public void testContains() {
        assertTrue(list1.contains("apple"));
        assertTrue(list1.contains("mango"));
        assertFalse(emptyList.contains("banana"));
    }


    /**
     * tests the clear method
     */
    
    public void testClearToArray() {
        emptyList.clear();
        assertEquals(emptyList.size(), 0);
        assertTrue(Arrays.equals(list1.toArray(), fruitArray));
        list1.clear();
        assertTrue(Arrays.equals(list1.toArray(), emptyArray));
        list1.add(0, "banana");
        list1.add(0, "apple");
       // assertFalse(Arrays.equals(list1.toArray(), halfArray));
    }



    /**
     * tests the lastIndexOf method
     */
    public void testLastIndexOf() {
        assertEquals(-1, list1.lastIndexOf("bro"));
        assertEquals(0, list1.lastIndexOf("apple"));
    }


    /**
     * tests the toString method
     */
    public void testToString() {
        assertEquals("{apple, banana, mango, kiwi}", list1.toString());
    }
}
