package employees;

// -------------------------------------------------------------------------
/**
 * Represents an average employee working 40 hours per week.
 *
 * @author Megan Rigsbee (mrigsbee)
 * @version 2015.02.23
 */
public class Employee {
    // ~ Fields ................................................................

    private String name;
    private double hourlyRate;


    // ~ Constructor ...........................................................
    /**
     * New Employee object.
     *
     * @param name
     *            Name of Employee
     * @param hourlyRate
     *            Pay rate of Employee (per hour).
     */
    public Employee(String name, double hourlyRate) {
        this.name = name;
        this.hourlyRate = hourlyRate;
    }

    // ~ Methods ...............................................................


    // ----------------------------------------------------------
    /**
     * Gets the employee's name.
     * 
     * @return the employee's name
     */
    public String getName() {
        return name;
    }


    // ----------------------------------------------------------
    /**
     * Gets the pay rate (per hour).
     * 
     * @return the pay rate
     */
    public double getHourlyRate() {
        return hourlyRate;
    }


    // ----------------------------------------------------------
    /**
     * Amount paid to the employee for an average 40 hour work week.
     * 
     * @return weekly Weekly pay for employee
     */
    public double weeklyPay() {
        return hourlyRate * 40;
    }


    /**
     * implements the equals method
     */
    @Override
    public boolean equals(Object other)

    {
        if (other == this) {
            return true;
        }
        if (other == null) {
            return false;
        }
        if (this.getClass() == other.getClass()) {
            Employee obj = (Employee)other;
            return name.equals(obj.name);
        }
        else {
            return false;
        }
    }
}
