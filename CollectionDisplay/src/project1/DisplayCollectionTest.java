package project1;

import bag.BagInterface;
import java.util.Arrays;

/**
 * This class goes through all test cases to
 * make sure DisplayCollection is working properly.
 * 
 * @author David Weisiger dbw
 * @version September 6th, 2017
 */
public class DisplayCollectionTest extends student.TestCase {

    /**
     * tests to see if the bag contents only
     * have what is inside the STRINGS constant
     */
    public void testBagContents() {
        DisplayCollection coll = new DisplayCollection();
        BagInterface<String> bag = coll.getItemBag();

        while (!bag.isEmpty()) {
            assertTrue(Arrays.asList(DisplayCollection.STRINGS).contains(bag
                .remove()));
        }
    }


    /**
     * tests to see if the bag size is between 5 and 15
     */
    public void testBagSize() {
        DisplayCollection coll;
        BagInterface<String> bag;
        for (int i = 0; i < 20; i++) {
            coll = new DisplayCollection();
            bag = coll.getItemBag();
            assertTrue(bag.getCurrentSize() >= 5 && bag.getCurrentSize() <= 15);
        }
    }
}
