package polymorphism;

public class Notebook extends Computer {
    private double screenSize;
    private double weight;
    
    public Notebook( String manuf, String proc, int ram, int disk,
            double prcSpeed, double ScrnSz, double wt){
        super(manuf, proc, ram, disk, prcSpeed);
        screenSize = ScrnSz;
        weight = wt;
    }
    
    public double getScreenSize(){
        return screenSize;
    }
    
    public double getWeight(){
        return weight;
       
    }
    

// highlight method and  ctrl+/  to add/remove comments
// to see difference in toString call on a Notebook object
//    @Override
//    public String toString(){ 
//        return super.toString() + 
//                "\nscreenSize: " + screenSize+ " " + 
//                "\nweight: " + weight;
//        
//    }
    

}
