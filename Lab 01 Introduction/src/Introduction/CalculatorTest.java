package Introduction;

import student.TestCase;

/**
 * CalculatorTest will test all of the methods in Calculator to make
 * sure that they run and perform as expected
 *
 * @author David Weisiger dbw
 * @version September 1, 2017
 */

public class CalculatorTest extends TestCase {
    private Calculator calculator;
    private Calculator calculator2;


    /**
     * sets up each test method before it runs
     */
    public void setUp() {
        calculator = new Calculator(4.0, 0);
        calculator2 = new Calculator(4.0, 2);
    }


    /**
     * test getFirstInput()
     */
    public void testGetFirstInput() {
        assertEquals(calculator.getFirstInput(), 4.0, .01);
    }


    /**
     * test getSecondInput()
     */
    public void testGetSecondInput() {
        assertEquals(calculator.getSecondInput(), 0.0, .01);
    }


    /**
     * test multiply()
     */
    public void testMultiply() {
        assertEquals(calculator.multiply(), 0, .01);

    }


    /**
     * test divide() by zero
     */
    public void testDivide() {
        assertEquals(calculator.divide(), Double.NaN, .01);
    }


    /**
     * test divide() again
     */
    public void testDivide2() {
        assertEquals(calculator2.divide(), 2, .01);
    }


    /**
     * test add()
     */
    public void testAdd() {
        assertEquals(calculator.add(), 4.0, .01);
    }


    /**
     * test subtract()
     */
    public void testSubtract() {
        assertEquals(calculator.subtract(), 4, .01);
    }


    /**
     * test power()
     */
    public void testPower() {
        assertEquals(calculator.power(), 1, .01);
    }
}
