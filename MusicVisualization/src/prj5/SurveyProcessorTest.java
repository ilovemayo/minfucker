/**
 * 
 */
package prj5;

import java.io.File;
import org.junit.Before;

/**
 * Test surveyprocessor class
 * 
 * @author davidbauer
 * @author Yue Yang
 * @author david weisiger
 * @version 12/3/17
 */
public class SurveyProcessorTest extends student.TestCase {
    SurveyProcessor tester;


    /**
     * Set up before each method
     * 
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() {
        tester = new SurveyProcessor(new File("InputFiles/MusicSurveyData.csv"),
            new File("InputFiles/SongList.csv"));
    }

}
