/**
 * 
 */
package spacecolonies;

/**
 * @author henry argueta
 * @version 11/12/17
 */
public class PlanetTest extends student.TestCase {

    /**
     * objects of string, for the planet�s name, three ints for their minimum
     * skill requirements (on a scale of 1 to 5
     *
     */
    private Planet[] planet;

    /**
     * This class will contain a string, for a person�s name, a skills object,
     * and an int representation of their planet preference.
     */
    private Person[] person;

    // ~ SetUp method -----------------------------------------------------

    /**
     * This method creates an array of planets and and people for testing
     */
    public void setUp() {
        planet = new Planet[4];
        person = new Person[10];

        // planets
        planet[1] = new Planet("Henry", 5, 2, 2, 10);
        planet[2] = new Planet("Tesla", 2, 5, 2, 4);
        planet[3] = new Planet("Henry", 2, 2, 5, 3);

        // are below
        person[2] = new Person("Nikola Tesla", 5, 2, 1, 1);
        person[3] = new Person("Bob Argueta", 2, 5, 3, 3);

        // are above
        person[4] = new Person("Kam Drinkuth", 2, 4, 5, 3);
        person[5] = new Person("Ryan Drinkuth", 5, 5, 5, 2);
        person[6] = new Person("Mom Argueta", 5, 4, 3, 1);

    }

    // ~ Test Methods -----------------------------------------------------

    /**
     * This test the getName method
     */
    public void testGetName() {
        assertEquals("Henry", planet[1].getName());
        assertEquals("Tesla", planet[2].getName());

    }

    /**
     * This test the setName method
     *
     */
    public void testSetName() {
        assertEquals("Kam", planet[1].setName("Kam"));
    }

    /**
     * This test the test the getSkills method
     */
    public void testGetSkills() {
        Skills s = new Skills(2, 5, 2);
        Skills s2 = new Skills(5, 2, 2);
        Skills s3 = new Skills(2, 2, 5);

        assertEquals(s, planet[2].getSkills());
        assertEquals(s2, planet[1].getSkills());
        assertEquals(s3, planet[3].getSkills());
    }

    /**
     * This test the test the getPopulationSize method
     */
    public void testGetPopulationSize() {
        assertEquals(0, planet[3].getPopulationSize());
        planet[3].addPerson(person[5]);
        assertEquals(1, planet[3].getPopulationSize());
    }

    /**
     * This test the test the getPopulation method
     */
    public void testGetPopulation() {
        Skills s = new Skills(5, 5, 5);

        assertEquals(0, planet[3].getPopulationSize());
        planet[2].addPerson(person[5]);
        planet[2].addPerson(person[5]);
        assertEquals("Ryan Drinkuth", planet[2].getPopulation()[0].getName());
        assertEquals(s, planet[2].getPopulation()[1].getSkills());

    }

    public void testGetCapacity() {
        assertEquals(10, planet[1].getCapacity());
        assertEquals(4, planet[2].getCapacity());

    }

    /**
     * Test toString method example Caturn, population 5 (cap: 10), Requires: A
     * >= 3, M >= 2, T >= 1
     */
    public void testGetAvailability() {

        assertFalse(planet[3].equals(planet[1]));
        assertFalse(planet[2].equals(planet[1]));
        assertFalse(planet[3].equals(planet[2]));

        assertTrue(planet[1].equals(planet[1]));
        planet[1].addPerson(person[6]);
        assertFalse(planet[1].equals(planet[0]));

    }

    /**
     * Test toString method example Caturn, population 5 (cap: 10), Requires: A
     * >= 3, M >= 2, T >= 1
     */
    public void testToString() {

        assertEquals(
                "Henry, population 0 (cap: 10), Requires: A >= 5, M >= 2, T >=2",
                planet[1].toString());
    }

    /**
     * will need to determine whether the applicant is qualified to live on the
     * colony.
     */
    public void testAddPerson() {
        assertTrue(planet[1].addPerson(person[6]));
        assertFalse(planet[2].addPerson(person[6]));
        assertFalse(planet[2].addPerson(person[2]));
    }

    /**
     * Two planets are equal if all 5 their input fields are equal and
     * populationSize is equal
     */
    public void testEquals() {
        planet[1].addPerson(person[6]);
        assertEquals(9, planet[1].getAvailability());

    }

    /**
     * TestCompareTo method returns 0 if all 5 their input fields are equal and
     * populationSize is equal
     */
    public void testCompareTo() {
        assertTrue(0 > planet[3].compareTo(planet[1]));
        assertEquals(0, planet[3].compareTo(planet[3]));

    }

}
