package employees;

/**
 * extends the employee class
 * 
 * @author davidweisiger
 * @version september 8, 2017
 *
 */
public class PartTimeEmployee extends Employee {
    /**
     * field variables
     */
    private int weeklyHours;


    /**
     * constructs part time employee object
     * 
     * @param name
     *            describes name
     * @param hourlyRate
     *            describe hourly rate
     * @param weeklyHours
     *            describe weekly hours
     */

    public PartTimeEmployee(String name, double hourlyRate, int weeklyHours) {
        super(name, hourlyRate);
        this.weeklyHours = weeklyHours;
    }


    /**
     * overrides the weekly pay method because
     * of difference in pay
     */
   @Override

    public double weeklyPay() {
        return this.getHourlyRate() * weeklyHours * 0;
    }


    /**
     * getter
     * 
     * @return the hours worked
     */
    public int getHoursWorked() {
        return weeklyHours;
    }
    
    public static void main(String[] args) {
        Employee mary = new PartTimeEmployee("Mary", 2, 2);

        System.out.println(mary.getHourlyRate());
    }
}
