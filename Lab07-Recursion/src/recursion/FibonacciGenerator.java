package recursion;

/**
 * 
 * @author davidweisiger dbw
 * @version 10.14.17
 *
 */

public class FibonacciGenerator {

    /**
     * finds fibonacci sequence
     * 
     * @param n
     *            is the index
     * @return the fibonacci number
     */
    public static int fib(int n) {
        if (n <= 1) {
            return n;
        }
        else {
            return fib(n - 1) + fib(n - 2);
        }
    }

    int[] map;

    int mapSize;


    public int fib2(int n) {

        map = new int[n + 1];

        map[0] = 0;

        map[1] = 1; // stores the 0th and 1st Fibonacci numbers

        mapSize = 2;

        return (fibHelper(n));

    }


    public int fibHelper(int n) {

        if (n < mapSize) {

            return map[n];

        }
        else {

            map[n] = fibHelper(n - 1) + fibHelper(n - 2);

            mapSize++;

            return map[n];

        }

    }
    
    public static int fib3(int n) {   

        if (n == 0)       

            return 0;   

        else    {       

            int previousFib = 0;       

            int currentFib = 1;       

            for(int i = 0; i < n-1; i++) {                    

                int newFib = previousFib + currentFib;           

                previousFib = currentFib;           

                currentFib  = newFib;       

            }       

                 return currentFib;   

            }

        }


    public static void main(String[] args) {
        FibonacciGenerator blah = new FibonacciGenerator();
        System.out.println(FibonacciGenerator.fib3(6));
    }
}
