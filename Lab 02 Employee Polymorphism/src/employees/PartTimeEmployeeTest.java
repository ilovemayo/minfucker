package employees;

/**
 * this calss test the part time employee
 * 
 * @author davidweisiger
 * @version september 8, 2017
 *
 */
public class PartTimeEmployeeTest extends student.TestCase {
    /**
     * field variables
     */
    PartTimeEmployee test;


    /**
     * sets up objects
     */
    public void setUp() {
        test = new PartTimeEmployee("bob", 2, 4);
    }


    /**
     * tests getting weekly pay
     */

    public void testWeeklyPay() {
        assertEquals(test.weeklyPay(), 8, .01);
    }


    /**
     * tests get hours worked
     */
    public void testGetHoursWorked() {
        assertEquals(test.getHoursWorked(), 4, .01);
    }

}
