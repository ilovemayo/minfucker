/**
 * 
 */
package recursivetree;

/**
 * @author henry argueta
 * @version 11/30/2017
 * 
 *
 */
public class BinaryTreeTest extends student.TestCase {

    private BinaryTree<String> tree;
    private BinaryTree<String> tree2;
    private BinaryTree<String> tree4;

    /**
     * test the data element
     */
    public void setUp() {
        tree = new BinaryTree<String>("B");
        tree2 = new BinaryTree<String>("C");
        tree4 = new BinaryTree<String>("A", tree, tree2);
    }

    /**
     * test the data element
     */
    public void testGetElement() {
        assertEquals("B", tree.getElement());

    }

    /**
     * test the data element
     */
    public void testSetElement() {
        tree.setElement("A");
        assertEquals("A", tree.getElement());
    }

    /**
     * test the get left method
     */
    public void testGetLeft() {
        assertEquals(tree, tree4.getLeft());

    }

    /**
     * test the set left in method
     */
    public void testSetLeft() {
        tree4.setLeft(tree2);
        assertEquals(tree2, tree4.getLeft());

    }

    /**
     * test the get right in method
     */
    public void testGetRight() {
        assertEquals(tree2, tree4.getRight());
    }

    /**
     * test the set right in method
     */
    public void testSetRight() {
        tree4.setRight(tree);
        assertEquals(tree, tree4.getRight());
    }

    /**
     * test the size method
     */
    public void testSize() {
        assertEquals(1, tree.size());
        assertEquals(3, tree4.size());

    }

    /**
     * test the size method
     */
    public void testHeight() {
        assertEquals(1, tree.height());
        assertEquals(2, tree4.height());

    }

    /**
     * test the toPreOrderString method
     */
    public void testToPreOrderString() {
        assertEquals("(B)", tree.toPreOrderString());
        assertEquals("(A(B)(C))", tree4.toPreOrderString());

    }

    /**
     * test the toInOrderString method
     */
    public void testToInOrderString() {
        assertEquals("(B)", tree.toInOrderString());
        assertEquals("((B)A(C))", tree4.toInOrderString());
    }

    /**
     * test the toPostOrderString method
     */
    public void testToPostOrderString() {
        assertEquals("(B)", tree.toPostOrderString());
        assertEquals("((B)(C)A)", tree4.toPostOrderString());

    }
}
