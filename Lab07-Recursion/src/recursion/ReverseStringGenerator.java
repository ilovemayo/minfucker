package recursion;

/**
 * 
 * @author davidweisiger dbw
 * @version 10.14.17
 *
 */

public class ReverseStringGenerator {

    /**
     * static method for reversing a string
     * 
     * @param str
     *            as an input to reverse
     * @return the reversed string
     */
    public static String reverse(String str) {
        if ((null == str) || (str.length() <= 1)) {
            return str;
        }
        return reverse(str.substring(1)) + str.charAt(0);
    }
}
