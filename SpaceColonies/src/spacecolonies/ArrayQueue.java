package spacecolonies;

import queue.EmptyQueueException;
import queue.QueueInterface;

/**
 * ArrayQueue class
 * 
 * @author dbw
 * @version 11.16.17
 *
 * @param <T>
 *            is generic parameter
 */
public class ArrayQueue<T> implements QueueInterface<T> {
    /*
     * instance variables
     */
    private T[] queue;
    private int enqueueIndex;
    private int dequeueIndex;
    private int size;

    private static final int DEFAULT_CAPACITY = 10;
    private static final int MAX_CAPACITY = 100;


    /**
     * default constructor
     */
    public ArrayQueue() {
        this(DEFAULT_CAPACITY);
    }


    /**
     * overloaded constructor
     * 
     * @param initialCapacity
     *            is starting capacity
     */
    public ArrayQueue(int initialCapacity) {
        checkCapacity(initialCapacity);
        // safe cast to generic array because new array contains null entries
        @SuppressWarnings("unchecked")
        T[] tempQueue = (T[])new Object[initialCapacity + 1];
        queue = tempQueue;

        // sets enqueueIndex to end of array and dequeueIndex/size to 0
        dequeueIndex = 0;
        enqueueIndex = initialCapacity;
        size = 0;
    }


    /**
     * @return capacity of queue
     */
    public int getLength() {
        return queue.length;
    }


    /**
     * @return number of items in queue
     */
    public int getSize() {
        return size;
    }


    /**
     * @return true if array is empty, false otherwise
     */
    @Override
    public boolean isEmpty() {
        return dequeueIndex == ((enqueueIndex + 1) % queue.length);
    }


    /**
     * enqueues a new entry
     */
    @Override
    public void enqueue(T newEntry) {
        ensureCapacity();
        enqueueIndex = (enqueueIndex + 1) % queue.length;
        queue[enqueueIndex] = newEntry;
        size++;
    }


    /**
     * doubles the capacity if it's full
     */
    private void ensureCapacity() {
        if (dequeueIndex == ((enqueueIndex + 2) % queue.length)) {
            T[] oldQueue = queue;
            int oldSize = oldQueue.length;
            int newSize = 2 * oldSize;
            checkCapacity(newSize);

            @SuppressWarnings("unchecked")
            T[] tempQueue = (T[])new Object[newSize];
            queue = tempQueue;
            for (int i = 0; i < oldSize - 1; i++) {
                queue[i] = oldQueue[dequeueIndex];
                dequeueIndex = (dequeueIndex + 1) % oldSize;
            }
            dequeueIndex = 0;
            enqueueIndex = oldSize - 2;
        }
    }


    /**
     * dequeues front entry
     */
    @Override
    public T dequeue() {
        if (isEmpty()) {
            throw new EmptyQueueException();
        }
        else {
            T front = queue[dequeueIndex];
            queue[dequeueIndex] = null;
            dequeueIndex = (dequeueIndex + 1) % queue.length;
            size--;
            return front;
        }
    }


    /**
     * gets the front of the queue
     */
    @Override
    public T getFront() {
        if (isEmpty()) {
            throw new EmptyQueueException();
        }
        else {
            return queue[dequeueIndex];
        }
    }


    /**
     * overrides clear method, empties the queue while it has entries
     */
    @Override
    public void clear() {
        while (!isEmpty()) {
            dequeue();
        }
    }


    /**
     * helper method for checking capacity
     * 
     * @param capacity
     *            as parameter
     */
    public void checkCapacity(int capacity) {
        if (capacity > MAX_CAPACITY) {
            throw new IllegalStateException();
        }
    }


    /**
     * toArray method
     * 
     * @return array of this queue
     */
    public T[] toArray() {
        @SuppressWarnings("unchecked")
        T[] arrayQueue = (T[])new Object[getSize()];

        for (int index = 0; index < getSize(); index++) {
            arrayQueue[index] = queue[index];
        }

        return arrayQueue;
    }


    /**
     * overrides to string method returns string representation
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append('[');

        if (getSize() == 0) {
            builder.append("]");
            return builder.toString();
        }

        T[] arrayQueue = (T[])toArray();
        for (int index = 0; index < arrayQueue.length; index++) {
            builder.append(String.valueOf(arrayQueue[index]) + ", ");
        }
        builder.deleteCharAt(builder.length() - 1);
        builder.deleteCharAt(builder.length() - 1);
        builder.append(']');

        return builder.toString();
    }


    /**
     * overrides equals method, checks if queues are equal
     */
    @Override
    public boolean equals(Object obj) {
        if ((obj == null) || (obj.getClass() != ArrayQueue.class)) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        @SuppressWarnings("unchecked")
        ArrayQueue<T> others = (ArrayQueue<T>)obj;

        if (others.getSize() != this.getSize()) {
            return false;
        }

        for (int i = 0; i < getSize(); i++) {

            T thisElement = queue[i];
            T otherElement = others.queue[i];

            if (thisElement != null && otherElement != null) {
                if (!thisElement.equals(otherElement)) {
                    return false;
                }
            }
        }
        return true;

    }
}
