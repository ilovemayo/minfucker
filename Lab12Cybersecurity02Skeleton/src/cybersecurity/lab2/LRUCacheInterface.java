package cybersecurity.lab2;

import cybersecurity.KVStore;

/**
 * LRUCacheInterface<K, V> interface
 * 
 * @author saima
 * @version 04.10.2017
 *
 * @param <K>
 *            the type of the key
 * @param <V>
 *            the type of the value
 */
public interface LRUCacheInterface<K, V> extends KVStore<K, V> {
    /**
     * Returns true if this list contains no elements.
     * 
     * @return true if this list contains no elements
     */
    public boolean isEmpty();


    /**
     * Returns the number of elements in this list.
     * 
     * @return the number of elements in this list
     */
    public int size();


    /**
     * Removes all of the elements from this list. The list will be empty after
     * this call returns.
     */
    public void clear();
}
