package prj5;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * This is the LinkedList class which will sort the files in order
 * 
 * @author davidbauer
 * @author Yue Yang
 * @author david weisiger
 * @version 11/19/17
 * @param <T>
 *            generic type which will be added later
 */
public class LinkedList<T> implements Iterable<T> {

    /**
     * Node class needed for the linkedlist
     * 
     * @author davidbauer
     * @author Yue Yang
     * @author david weisiger
     * @param <T>
     */
    public static class Node<T> {

        // The data element stored in the node.
        private T data;

        // The next node in the sequence.
        private Node<T> next;


        /**
         * Creates a new node with the given data
         *
         * @param d
         *            the data to put inside the node
         */
        public Node(T d) {
            data = d;
        }


        /**
         * Sets the node after this node
         *
         * @param n
         *            the node after this one
         */
        public void setNextNode(Node<T> n) {
            next = n;
        }


        /**
         * Gets the next node
         *
         * @return the next node
         */
        public Node<T> getNextNode() {
            return next;
        }


        /**
         * Gets the data in the node
         *
         * @return the data in the node
         */
        public T getData() {
            return data;
        }
    }

    // head of the node
    private Node<T> head;

    // the size of the linked list
    private int size;


    /**
     * Creates a new LinkedList object
     */
    public LinkedList() {
        head = null;
        size = 0;

    }


    /**
     * Gets the number of elements in the list
     *
     * @return the number of elements
     */
    public int size() {
        return size;
    }


    /**
     * Adds the object to the position in the list
     *
     * @precondition obj cannot be null
     * @param index
     *            where to add the object
     * @param obj
     *            the object to add
     * @throws IndexOutOfBoundsException
     *             if index is less than zero or greater than size
     * @throws IllegalArgumentException
     *             if obj is null
     */
    public void add(int index, T obj) {
        // check if the object is null
        if (obj == null) {
            throw new IllegalArgumentException("Object is null");
        }

        // check if the index is out of bounds
        if ((index < 0) || (index > size())) {
            throw new IndexOutOfBoundsException("Index is out of bounds");
        }

        Node<T> current = head;

        // empty stack case
        if (isEmpty()) {
            head = new Node<T>(obj);
            size = 1;
        }
        // all other cases
        else {
            int currentIndex = 0;
            while (current != null) {
                if ((currentIndex + 1) == index) {
                    Node<T> nextNext = current.next;
                    Node<T> newNode = new Node<T>(obj);
                    current.setNextNode(newNode);
                    newNode.setNextNode(nextNext);
                    size++;

                }
                current = current.getNextNode();
                currentIndex++;
            }

        }
    }


    /**
     * Adds the object to the end of the list.
     *
     * @precondition obj cannot be null
     * @param obj
     *            the object to add
     * @throws IllegalArgumentException
     *             if obj is null
     */
    public void add(T obj) {
        // check if the object is null
        if (obj == null) {
            throw new IllegalArgumentException("Object is null");
        }

        Node<T> current = head;

        // empty stack case
        if (isEmpty()) {
            head = new Node<T>(obj);
        }

        // other cases
        else {
            while (current.next != null) {
                current = current.next;
            }
            current.setNextNode(new Node<T>(obj));
        }
        size++;
    }


    /**
     * Checks if the array is empty
     *
     * @return true if the array is empty
     */
    public boolean isEmpty() {
        return (size == 0);
    }


    /**
     * Removes the first instance of the given object from the list
     *
     * @param obj
     *            the object to remove
     * @return true if successful
     */
    public boolean remove(T obj) {
        Node<T> current = head;
        if (this.contains(obj)) {

            // account for matching head
            if ((obj.equals(current.data))) {
                head = head.next;
                size--;
                return true;
            }

            // account for 2+ size
            while ((current.next != null)) {
                if ((obj.equals(current.next.data))) {
                    if (current.next.next != null) {
                        current.setNextNode(current.next.next);
                    }
                    else {
                        current.setNextNode(null);
                    }
                    size--;
                    return true;
                }
                current = current.next;
            }
        }
        return false;
    }


    /**
     * Removes the object at the given position
     *
     * @param index
     *            the position of the object
     * @return true if the removal was successful
     * @throws IndexOutOfBoundsException
     *             if there is not an element at the index
     */
    public boolean remove(int index) {
        // // if the index is invalid
        if (index < 0 || head == null) {
            throw new IndexOutOfBoundsException("Index is out of bounds");
        }
        else {

            Node<T> current = head;
            int currentIndex = 0;

            // account for 1 size
            if ((index == 0)) {
                head = head.next;
                size--;
                return true;
            }
            while (currentIndex + 1 != index) {
                currentIndex++;
                current = current.next;
            }
            current.setNextNode(current.next.next);
            size--;

            return true;

        }
    }


    /**
     * Gets the object at the given position
     *
     * @param index
     *            where the object is located
     * @return The object at the given position
     * @throws IndexOutOfBoundsException
     *             if no node at the given index
     */
    public T get(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException("Index exceeds the size.");
        }
        Node<T> current = head;
        int currentIndex = 0;
        T data = null;
        while (currentIndex != index) {
            currentIndex++;
            current = current.next;
        }
        data = current.data;
        return data;

    }


    /**
     * Checks if the list contains the given object
     *
     * @param obj
     *            the object to check for
     * @return true if it contains the object
     */
    public boolean contains(T obj) {
        Node<T> current = head;
        while (current != null) {
            if (obj.equals(current.data)) {
                return true;
            }
            current = current.next;
        }

        return false;
    }


    /**
     * Returns a string representation of the list If a list contains A, B, and
     * C, the following should be returned "{A, B, C}" (Without the quotations)
     *
     * @return a string representing the list
     */
    @Override
    public String toString() {
        String result = "{";

        Node<T> current = head;
        while (current != null) {
            result += "" + current.data;
            current = current.next;
            if (current != null) {
                result += ", ";
            }
        }
        result += "}";
        return result;
    }


    /**
     * Returns an array representation of the list If a list contains A, B, and
     * C, the following should be returned {A, B, C}, If a list
     * contains A, B, C, and C the following should be returned {A, B, C, C}
     *
     * @return an array representing the list
     */
    public Object[] toArray() {

        Object[] array = new Object[this.size()];

        Node<T> current = head;
        int count = 0;
        while (current != null) {
            array[count] = current.getData();
            current = current.next;
            count++;
        }

        return array;
    }


    /**
     * Returns true if both lists have the exact same contents
     * in the exact same order
     *
     * @return a boolean of whether two lists have the same contents,
     *         item per item and in the same order
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() == obj.getClass()) {
            @SuppressWarnings("unchecked")
            LinkedList<T> other = ((LinkedList<T>)obj);
            if (other.size() == this.size()) {
                Node<T> current = head;
                Node<T> otherCurrent = other.head;
                while (current != null) {
                    if (!current.getData().equals(otherCurrent.getData())) {
                        return false;
                    }
                    current = current.getNextNode();
                    otherCurrent = otherCurrent.getNextNode();
                }
                return true;
            }
        }

        return false;
    }


    /**
     * Removes all of the elements from the list
     */
    public void clear() {
        // make sure we don't call clear on an empty list
        if (head != null) {
            head.setNextNode(null);
            head = null;
            size--;
        }
        size = 0;

    }


    /**
     * Sorts by genre
     */
    @SuppressWarnings("unchecked")
    public void sortbyGenre() {
        Object[] arr = this.toArray();
        for (int i = 1; i < arr.length; i++) {
            SongInfo temp = null;
            for (int j = 1; j < arr.length - i; j++) {
                if (((SongInfo)arr[j - 1]).compareToGenre(
                    (SongInfo)arr[j]) > 0) {
                    temp = (SongInfo)arr[j - 1];
                    arr[j - 1] = arr[j];
                    arr[j] = temp;
                }
                if (((SongInfo)arr[j - 1]).compareToGenre(
                    (SongInfo)arr[j]) == 0) {
                    if (((SongInfo)arr[j - 1]).compareToTitle(
                        (SongInfo)arr[j]) < 0) {
                        temp = (SongInfo)arr[j - 1];
                        arr[j - 1] = arr[j];
                        arr[j] = temp;
                    }
                }
            }
        }
        if (arr.length >= 2) {
            int cc = arr.length - 1;
            while (((SongInfo)arr[cc]).compareToGenre((SongInfo)arr[cc
                - 1]) < 0) {
                SongInfo temp = (SongInfo)arr[cc - 1];
                arr[cc - 1] = arr[cc];
                arr[cc] = temp;
                cc--;
            }
        }
        this.clear();
        for (int i = 0; i < arr.length; i++) {
            SongInfo temp = (SongInfo)arr[i];
            this.add((T)temp);
        }
    }


    /**
     * Sorts by title
     */
    @SuppressWarnings("unchecked")
    public void sortbyTitle() {
        Object[] arr = this.toArray();
        for (int i = 1; i < arr.length; i++) {
            SongInfo temp = null;
            for (int j = 1; j < arr.length - i; j++) {
                if (((SongInfo)arr[j - 1]).compareToTitle(
                    (SongInfo)arr[j]) > 0) {
                    temp = (SongInfo)arr[j - 1];
                    arr[j - 1] = arr[j];
                    arr[j] = temp;
                }
                if (((SongInfo)arr[j - 1]).compareToTitle(
                    (SongInfo)arr[j]) == 0) {
                    if (((SongInfo)arr[j - 1]).compareToTitle(
                        (SongInfo)arr[j]) < 0) {
                        temp = (SongInfo)arr[j - 1];
                        arr[j - 1] = arr[j];
                        arr[j] = temp;
                    }
                }
            }
        }
        if (arr.length >= 2) {
            int cc = arr.length - 1;
            while (((SongInfo)arr[cc]).compareToTitle((SongInfo)arr[cc
                - 1]) < 0) {
                SongInfo temp = (SongInfo)arr[cc - 1];
                arr[cc - 1] = arr[cc];
                arr[cc] = temp;
                cc--;
            }
        }
        this.clear();
        for (int i = 0; i < arr.length; i++) {
            SongInfo temp = (SongInfo)arr[i];
            this.add((T)temp);
        }
    }


    /**
     * Sorts by year
     */
    @SuppressWarnings("unchecked")
    public void sortbyYear() {
        Object[] arr = this.toArray();
        for (int i = 1; i < arr.length; i++) {
            SongInfo temp = null;
            for (int j = 1; j < arr.length - i; j++) {
                if (((SongInfo)arr[j - 1]).compareToYear(
                    (SongInfo)arr[j]) > 0) {
                    temp = (SongInfo)arr[j - 1];
                    arr[j - 1] = arr[j];
                    arr[j] = temp;
                }
                if (((SongInfo)arr[j - 1]).compareToYear(
                    (SongInfo)arr[j]) == 0) {
                    if (((SongInfo)arr[j - 1]).compareToYear(
                        (SongInfo)arr[j]) < 0) {
                        temp = (SongInfo)arr[j - 1];
                        arr[j - 1] = arr[j];
                        arr[j] = temp;
                    }
                }
            }
        }
        if (arr.length >= 2) {
            int cc = arr.length - 1;
            while (((SongInfo)arr[cc]).compareToYear((SongInfo)arr[cc
                - 1]) < 0) {
                SongInfo temp = (SongInfo)arr[cc - 1];
                arr[cc - 1] = arr[cc];
                arr[cc] = temp;
                cc--;
            }
        }
        this.clear();
        for (int i = 0; i < arr.length; i++) {
            SongInfo temp = (SongInfo)arr[i];
            this.add((T)temp);
        }
    }


    /**
     * Sorts by artist
     */
    @SuppressWarnings("unchecked")
    public void sortbyArtist() {
        Object[] arr = this.toArray();
        for (int i = 1; i < arr.length; i++) {
            SongInfo temp = null;
            for (int j = 1; j < arr.length - i; j++) {
                if (((SongInfo)arr[j - 1]).compareToArtist(
                    (SongInfo)arr[j]) > 0) {
                    temp = (SongInfo)arr[j - 1];
                    arr[j - 1] = arr[j];
                    arr[j] = temp;
                }
                if (((SongInfo)arr[j - 1]).compareToArtist(
                    (SongInfo)arr[j]) == 0) {
                    if (((SongInfo)arr[j - 1]).compareToArtist(
                        (SongInfo)arr[j]) < 0) {
                        temp = (SongInfo)arr[j - 1];
                        arr[j - 1] = arr[j];
                        arr[j] = temp;
                    }
                }
            }
        }
        if (arr.length >= 2) {
            int cc = arr.length - 1;
            while (((SongInfo)arr[cc]).compareToArtist((SongInfo)arr[cc
                - 1]) < 0) {
                SongInfo temp = (SongInfo)arr[cc - 1];
                arr[cc - 1] = arr[cc];
                arr[cc] = temp;
                cc--;
            }
        }
        this.clear();
        for (int i = 0; i < arr.length; i++) {
            SongInfo temp = (SongInfo)arr[i];
            this.add((T)temp);
        }
    }


    /**
     * Iterator implementation
     */
    public Iterator<T> iterator() {
        return new LinkedListIterator();
    }


    /**
     * Iterator class
     *
     */
    private class LinkedListIterator implements Iterator<T> {
        private Node<T> nextNode;


        /**
         * iterator constructor
         */
        public LinkedListIterator() {
            nextNode = head;
        }


        /**
         * method to check if has next
         */
        public boolean hasNext() {
            return nextNode != null;
        }


        /**
         * method to move the iterator
         */
        public T next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            T res = nextNode.data;
            nextNode = nextNode.next;
            return res;
        }


        /**
         * remove method
         */
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

}
