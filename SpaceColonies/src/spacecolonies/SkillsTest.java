package spacecolonies;

/**
 * Tests the Skills class
 * 
 * @author dbw
 * @version 11.16.17
 *
 */
public class SkillsTest extends student.TestCase {
    /**
     * variables we'll be using
     */
    private Skills skills1;
    private Skills skills2;
    private Skills skills3;
    private Skills skills4;


    /**
     * sets things up for us
     */
    public void setUp() {
        skills1 = new Skills(1, 1, 1);
        skills2 = new Skills(1, 1, 1);
        skills3 = new Skills(1, 2, 3);
        skills4 = null;
    }


    /**
     * tests all the simple get methods
     */
    public void testGet() {
        assertEquals(1, skills1.getAgriculture());
        assertEquals(2, skills3.getMedicine());
        assertEquals(1, skills2.getTechnology());
    }


    /**
     * tests isBelow()
     */
    public void testIsBelow() {
        assertTrue(skills2.isBelow(skills3));
        assertFalse(skills3.isBelow(skills1));
    }


    /**
     * tests equals()
     */

    public void testEquals() {
        assertTrue(skills1.equals(skills1));
        assertFalse(skills1.equals(skills4));
        assertTrue(skills1.equals(skills2));
        assertFalse(skills1.equals(skills3));
        assertFalse(skills3.equals(0));
    }
    
    /**
     * tests toString()
     */
    public void testToString() {
        assertEquals("A:1 M:2 T:3", skills3.toString());
    }
}
