package spacecolonies;

import java.util.Arrays;
import list.AList;

/**
 * ColonyCalculator class handles most calculations
 * 
 * @author dbw
 * @version 11.16.17
 *
 */
public class ColonyCalculator {
    /**
     * field variables
     */
    private static final int NUM_PLANETS = 3;
    private ArrayQueue<Person> applicantQueue;
    private AList<Person> rejectBus;
    private static Planet[] planets = new Planet[NUM_PLANETS + 1];


    /**
     * constructor
     * 
     * @param applicantQueue
     *            input
     * @param planets
     *            input
     */
    public ColonyCalculator(
        ArrayQueue<Person> applicantQueue,
        Planet[] planets) {

        this.applicantQueue = applicantQueue;
        ColonyCalculator.planets = planets;

        rejectBus = new AList<>();
    }


    /**
     * @return the applicant queue
     */
    public ArrayQueue<Person> getQueue() {
        return applicantQueue;
    }


    /**
     * @return planets from here
     */
    public static Planet[] getPlanets() {
        return planets;
    }


    /**
     * gets planet for person
     * 
     * @param nextPerson
     *            this person
     * @return planet for nextPerson
     */
    public Planet getPlanetForPerson(Person nextPerson) {
        // make sure queue is not empty
        if (!applicantQueue.isEmpty()) {
            // make sure planet is not full
            Planet preferredPlanet = planets[nextPerson.getPlanet()];
            if (nextPerson.getPlanet() == 0) {
                return getMostAvailablePlanet(nextPerson);
            }
            if (!preferredPlanet.isFull() && preferredPlanet.isQualified(
                nextPerson)) {
                return preferredPlanet;
            }
        }
        return null;
    }


    /**
     * gets the most available planet
     * 
     * @param person
     *            input
     * @return most available planet
     */
    private Planet getMostAvailablePlanet(Person person) {
        Planet[] planetsCopy = planets.clone();
        Arrays.sort(planetsCopy);
        return planetsCopy[0];
    }


    /**
     * accept method
     * 
     * @return true if accepted, false otherwise
     */
    public boolean accept() {
        // make sure queue is not empty
        if (!applicantQueue.isEmpty()) {
            Person nextPerson = applicantQueue.getFront();
            Planet planet = getPlanetForPerson(nextPerson);

            int planetIndex = Arrays.asList(planets).indexOf(planet);
            // saves from null pointer
            if (planetIndex == 0) {
                return false;
            }
            planets[planetIndex].addPerson(applicantQueue.dequeue());
            return true;
        }
        return false;
    }


    /**
     * rejects from planet
     */
    public void reject() {
        rejectBus.add(applicantQueue.dequeue());
    }


    /**
     * this is the planet method
     * 
     * @param planet
     *            is input
     * @return a planet object
     */
    public Planet planetByNumber(int planet) {
        if (1 <= planet && planet <= 3) {
            return planets[planet];
        }
        else {
            return null;
        }
    }
}
