package prj5;

import java.awt.Color;
import CS2114.Shape;
import CS2114.TextShape;
import CS2114.Button;
import CS2114.Window;
import CS2114.WindowSide;

/**
 * This is the front end of the project which will show up every things on
 * window
 * 
 * @author davidbauer
 * @author Yue Yang
 * @author david weisiger
 * @version 11.15.17
 */
public class GUISurveyResult {

	/**
	 * instance variables
	 */
	@SuppressWarnings("unused")
	private SurveyProcessor surveyProcessor;
	private LinkedList<SongInfo> songs;

	private Shape box;
	private Shape box1;
	private Shape box2;
	private Shape box3;
	private Shape box4;
	private Shape box5;
	private Shape box6;
	private Shape box7;
	private Shape box8;

	private Shape rectangle;
	private Shape rectangle1;
	private Shape rectangle2;
	private Shape rectangle3;
	private Shape rectangle4;
	private Shape rectangle5;
	private Shape rectangle6;
	private Shape rectangle7;
	private Shape middleRec;

	private Window window;

	private Button prev;
	private Button next;
	private Button sortByArtist;
	private Button sortByTitle;
	private Button sortByYear;
	private Button sortByGenre;
	private Button representByHobby;
	private Button representByMajor;
	private Button representByRegion;
	private Button quit;

	private int nextTimes;

	// this string says which student attribute we are displaying, and changes
	// when the buttons are clicked
	private String representBy;

	// this number will determine whether students response represent
	// by major, region or hobby
	private int numDetermine;

	private int currentTime;

	/**
	 * default constructor
	 */
	public GUISurveyResult(SurveyProcessor surveyProcessor) {
		this.songs = surveyProcessor.getSongs();
		this.surveyProcessor = surveyProcessor;

		window = new Window("Project 5 - Music Preference Visualization");
		addBox();

		prev = new Button("<- Prev");
		prev.onClick(this, "clickedPrev");

		next = new Button("Next ->");
		next.onClick(this, "clickedNext");

		sortByArtist = new Button("Sort by Artist Name");
		sortByArtist.onClick(this, "clickedArtist");

		sortByTitle = new Button("Sort by Song Title");
		sortByTitle.onClick(this, "clickedTitle");

		sortByYear = new Button("Sort by Release Year");
		sortByYear.onClick(this, "clickedYear");

		sortByGenre = new Button("Sort by Genre");
		sortByGenre.onClick(this, "clickedGenre");

		representByHobby = new Button("Represent Hobby");
		representByHobby.onClick(this, "clickedHobby");

		representByMajor = new Button("Represent Major");
		representByMajor.onClick(this, "clickedMajor");

		representByRegion = new Button("Represent Region");
		representByRegion.onClick(this, "clickedRegion");

		quit = new Button("Quit");
		quit.onClick(this, "clickedQuit");

		representBy = "totallynotnull";
		addButtons();

		nextTimes = 0;
	}

	/**
	 * adds all buttons to window
	 */
	private void addButtons() {
		window.addButton(prev, WindowSide.NORTH);
		window.addButton(sortByArtist, WindowSide.NORTH);
		window.addButton(sortByTitle, WindowSide.NORTH);
		window.addButton(sortByYear, WindowSide.NORTH);
		window.addButton(sortByGenre, WindowSide.NORTH);
		window.addButton(next, WindowSide.NORTH);

		window.addButton(representByHobby, WindowSide.SOUTH);
		window.addButton(representByMajor, WindowSide.SOUTH);
		window.addButton(representByRegion, WindowSide.SOUTH);
		window.addButton(quit, WindowSide.SOUTH);
	}

	/**
	 * add all glyphs to the window
	 * 
	 * @param button
	 */
	public void clickedHobby(Button button) {
		representBy = "hobby";
		createLegend();
		buildGlyph(box, correctSong(box), 2);
		buildGlyph(box1, correctSong(box1), 2);
		buildGlyph(box2, correctSong(box2), 2);
		buildGlyph(box3, correctSong(box3), 2);
		buildGlyph(box4, correctSong(box4), 2);
		buildGlyph(box5, correctSong(box5), 2);
		buildGlyph(box6, correctSong(box6), 2);
		buildGlyph(box7, correctSong(box7), 2);
		buildGlyph(box8, correctSong(box8), 2);
		numDetermine = 1;
	}

	/**
	 * add all glyphs to the window
	 * 
	 * @param button
	 */
	public void clickedMajor(Button button) {
		representBy = "major";
		createLegend();
		buildGlyph(box, correctSong(box), 0);
		buildGlyph(box1, correctSong(box1), 0);
		buildGlyph(box2, correctSong(box2), 0);
		buildGlyph(box3, correctSong(box3), 0);
		buildGlyph(box4, correctSong(box4), 0);
		buildGlyph(box5, correctSong(box5), 0);
		buildGlyph(box6, correctSong(box6), 0);
		buildGlyph(box7, correctSong(box7), 0);
		buildGlyph(box8, correctSong(box8), 0);
		numDetermine = 2;
	}

	/**
	 * add all glyphs to the window
	 * 
	 * @param button
	 */
	public void clickedRegion(Button button) {
		representBy = "region";
		createLegend();
		buildGlyph(box, correctSong(box), 1);
		buildGlyph(box1, correctSong(box1), 1);
		buildGlyph(box2, correctSong(box2), 1);
		buildGlyph(box3, correctSong(box3), 1);
		buildGlyph(box4, correctSong(box4), 1);
		buildGlyph(box5, correctSong(box5), 1);
		buildGlyph(box6, correctSong(box6), 1);
		buildGlyph(box7, correctSong(box7), 1);
		buildGlyph(box8, correctSong(box8), 1);
		numDetermine = 3;
	}

	/**
	 * method run when sortByArtist button was clicked
	 * 
	 * @param button
	 *            pressed
	 */
	public void clickedArtist(Button button) {
		songs.sortbyArtist();
		if (numDetermine == 1) {
			clickedHobby(representByHobby);
		}
		if (numDetermine == 2) {
			clickedMajor(representByMajor);
		}
		if (numDetermine == 3) {
			clickedRegion(representByRegion);
		}
		currentTime = 1;
	}

	/**
	 * method run when sortByTitle button was clicked
	 * 
	 * @param button
	 *            pressed
	 */
	public void clickedTitle(Button button) {
		songs.sortbyTitle();
		if (numDetermine == 1) {
			clickedHobby(representByHobby);
		}
		if (numDetermine == 2) {
			clickedMajor(representByMajor);
		}
		if (numDetermine == 3) {
			clickedRegion(representByRegion);
		}
		currentTime = 2;

	}

	/**
	 * method run when sortByYear button was clicked
	 * 
	 * @param button
	 *            pressed
	 */
	public void clickedYear(Button button) {
		songs.sortbyYear();
		if (numDetermine == 1) {
			clickedHobby(representByHobby);
		}
		if (numDetermine == 2) {
			clickedMajor(representByMajor);
		}
		if (numDetermine == 3) {
			clickedRegion(representByRegion);
		}
		currentTime = 3;

	}

	/**
	 * method run when sortByGenre button was clicked
	 * 
	 * @param button
	 *            pressed
	 */
	public void clickedGenre(Button button) {
		songs.sortbyGenre();
		if (numDetermine == 1) {
			clickedHobby(representByHobby);
		}
		if (numDetermine == 2) {
			clickedMajor(representByMajor);
		}
		if (numDetermine == 3) {
			clickedRegion(representByRegion);
		}
		currentTime = 4;

	}

	/**
	 * method called when button next being pressed
	 * 
	 * @param button
	 *            pressed
	 */
	public void clickedNext(Button button) {
		window.removeAllShapes();
		if (currentTime == 1) {
			clickedTitle(sortByTitle);
		} else if (currentTime == 2) {
			clickedYear(sortByYear);
		} else if (currentTime == 3) {
			clickedGenre(sortByGenre);
		} else {
			System.out.println("end");
		}
	}

	/**
	 * method called when button prev being pressed
	 * 
	 * @param button
	 *            pressed
	 */
	public void clickedPrev(Button button) {
		window.removeAllShapes();
		if (currentTime == 4) {
			clickedYear(sortByYear);
		} else if (currentTime == 3) {
			clickedTitle(sortByTitle);
		} else if (currentTime == 2) {
			clickedArtist(sortByArtist);
		} else {
			System.out.println("end");
		}
	}

	public void addTextShape(Shape box, SongInfo song) {
		TextShape name = new TextShape(box.getX() + 10, box.getY() - 20, song.getTitle(), Color.BLACK);
		window.addShape(name);
	}

	/**
	 * this method exits the program
	 * 
	 * @param quit
	 *            button
	 */
	public void clickedQuit(Button quit) {
		System.exit(0);
	}

	/**
	 * Creating a general box settled on place so we can put glyph in it after
	 * sorted the data
	 */
	public void addBox() {
		int boxX = window.getWidth() / 6;
		int boxY = window.getHeight() / 8;
		int widthB = boxY - 10;
		box = new Shape(boxX, boxY, boxX, widthB, Color.white);
		window.addShape(box);
		box1 = new Shape(boxX * 3, boxY, boxX, widthB, Color.white);
		window.addShape(box1);
		box2 = new Shape(boxX * 5, boxY, boxX, widthB, Color.white);
		window.addShape(box2);
		box3 = new Shape(boxX, boxY * 5 / 2, boxX, widthB, Color.white);
		window.addShape(box3);
		box4 = new Shape(boxX * 3, boxY * 5 / 2, boxX, widthB, Color.white);
		window.addShape(box4);
		box5 = new Shape(boxX * 5, boxY * 5 / 2, boxX, widthB, Color.white);
		window.addShape(box5);
		box6 = new Shape(boxX, boxY * 4, boxX, widthB, Color.white);
		window.addShape(box6);
		box7 = new Shape(boxX * 3, boxY * 4, boxX, widthB, Color.white);
		window.addShape(box7);
		box8 = new Shape(boxX * 5, boxY * 4, boxX, widthB, Color.white);
		window.addShape(box8);
	}

	/**
	 * adds all glyphs to window
	 */
	public void addShapeAll() {
		window.addShape(middleRec);
		window.addShape(rectangle);
		window.addShape(rectangle1);
		window.addShape(rectangle2);
		window.addShape(rectangle3);
		window.addShape(rectangle4);
		window.addShape(rectangle5);
		window.addShape(rectangle6);
		window.addShape(rectangle7);
	}

	/**
	 * Correspond each song to each box in order after sorting
	 */
	public SongInfo correctSong(Shape boxneed) {
		SongInfo correctOne = null;
		if (boxneed == box) {
			correctOne = songs.get(0 + (9 * nextTimes));
		} else if (boxneed == box1) {
			correctOne = songs.get(1 + (9 * nextTimes));
		} else if (boxneed == box2) {
			correctOne = songs.get(2 + (9 * nextTimes));
		} else if (boxneed == box3) {
			correctOne = songs.get(3 + (9 * nextTimes));
		} else if (boxneed == box4) {
			correctOne = songs.get(4 + (9 * nextTimes));
		} else if (boxneed == box5) {
			correctOne = songs.get(5 + (9 * nextTimes));
		} else if (boxneed == box6) {
			correctOne = songs.get(6 + (9 * nextTimes));
		} else if (boxneed == box7) {
			correctOne = songs.get(7 + (9 * nextTimes));
		} else {
			correctOne = songs.get(8 + (9 * nextTimes));
		}
		return correctOne;
	}

	/**
	 * builds the glyph representing hobby
	 * 
	 * @param bbox
	 *            the box it will be stay
	 * @param song
	 *            the song correspond the box
	 */
	public void buildGlyph(Shape bbox, SongInfo song, int order) {
		int width = bbox.getWidth();
		int height = bbox.getHeight();
		int xInsideBox = bbox.getX() + width / 2 - 5;
		int yInsideBox = height / 4;
		middleRec = new Shape(xInsideBox, bbox.getY(), 10, height, Color.BLACK);
		song = correctSong(bbox);
		rectangle = new Shape(xInsideBox - song.getPercentHeard()[order][0], bbox.getY(),
				song.getPercentHeard()[order][0], yInsideBox, Color.PINK);
		rectangle1 = new Shape(xInsideBox - song.getPercentHeard()[order][1], bbox.getY() + yInsideBox,
				song.getPercentHeard()[order][1], yInsideBox, Color.BLUE);
		rectangle2 = new Shape(xInsideBox - song.getPercentHeard()[order][2], bbox.getY() + yInsideBox * 2,
				song.getPercentHeard()[order][2], yInsideBox, Color.YELLOW);
		rectangle3 = new Shape(xInsideBox - song.getPercentHeard()[order][3], bbox.getY() + yInsideBox * 3,
				song.getPercentHeard()[order][3], yInsideBox, Color.GREEN);
		rectangle4 = new Shape(xInsideBox + 10, bbox.getY(), song.getPercentLiked()[order][0], yInsideBox, Color.PINK);
		rectangle5 = new Shape(xInsideBox + 10, bbox.getY() + yInsideBox, song.getPercentLiked()[order][1], yInsideBox,
				Color.BLUE);
		rectangle6 = new Shape(xInsideBox + 10, bbox.getY() + yInsideBox * 2, song.getPercentLiked()[order][2],
				yInsideBox, Color.YELLOW);
		rectangle7 = new Shape(xInsideBox + 10, bbox.getY() + yInsideBox * 3, song.getPercentLiked()[order][3],
				yInsideBox, Color.GREEN);
		addShapeAll();
		addTextShape(bbox, song);
	}

	/**
	 * builds the legend
	 */
	public void createLegend() {
		window.removeAllShapes();

		// this sets the horizontal width to 1/4th of the screen size to the
		// left from the right
		int windowWidth = window.getWidth();
		int windowHeight = window.getHeight();

		// THESE WEIGHTS CAN BE ADJUSTED BASED ON SCREEN SIZE
		Shape legendOuterBox = new Shape(windowWidth - (int) (windowWidth / 5.5),
				windowHeight - (int) (windowHeight / 1.5), (int) (windowWidth / 5.5) - 3, (int) (windowHeight / 2.5),
				Color.black);
		Shape legendInnerBox = new Shape(windowWidth - (int) (windowWidth / 5.5) + 1,
				windowHeight - (int) (windowHeight / 1.5) + 1, (int) (windowWidth / 5.5) - 5,
				(int) (windowHeight / 2.5) - 2, Color.white);

		// create the legend title
		TextShape legendTitle = new TextShape(legendInnerBox.getX() + legendInnerBox.getX() / 25, legendInnerBox.getY(),
				"");
		legendTitle.setBackgroundColor(Color.white);
		// create title1
		TextShape title1 = new TextShape(legendInnerBox.getX(), legendInnerBox.getY() + 20, "", Color.PINK);
		title1.setBackgroundColor(Color.white);
		// create title2
		TextShape title2 = new TextShape(legendInnerBox.getX(), title1.getY() + title1.getHeight(), "", Color.BLUE);
		title2.setBackgroundColor(Color.white);
		// create the sports title
		TextShape title3 = new TextShape(legendInnerBox.getX(), title2.getY() + title2.getHeight(), "",
				Color.yellow.darker());
		title3.setBackgroundColor(Color.white);
		// create the music title
		TextShape title4 = new TextShape(legendInnerBox.getX(), title3.getY() + title3.getHeight(), "", Color.GREEN);
		title4.setBackgroundColor(Color.white);

		// create song title
		TextShape songtitle = new TextShape(legendTitle.getX() + 12, title4.getY() + title4.getHeight(), "Song Title",
				Color.BLACK);
		songtitle.setBackgroundColor(Color.white);

		// create middle rod and heard/likes
		Shape middleRod = new Shape(songtitle.getX() + songtitle.getWidth() / 2,
				songtitle.getY() + songtitle.getHeight(), 10, 50, Color.BLACK);
		TextShape heard = new TextShape(title4.getX(), songtitle.getY() + songtitle.getHeight() * 2, "Heard",
				Color.BLACK);
		heard.setBackgroundColor(Color.white);
		TextShape likes = new TextShape(heard.getX() + (int) (heard.getWidth() * 2.7), heard.getY(), "Likes",
				Color.BLACK);
		likes.setBackgroundColor(Color.white);

		// set the text for titles depending on button click
		if (representBy.equals("hobby")) {
			legendTitle.setText("Hobby Legend");
			title1.setText("Read");
			title2.setText("Art");
			title3.setText("Sports");
			title4.setText("Music");
		} else if (representBy.equals("major")) {
			legendTitle.setText("Major Legend");
			title1.setText("Comp Sci");
			title2.setText("Other Eng");
			title3.setText("Math/CDMA");
			title4.setText("Other");
		} else if (representBy.equals("region")) {
			legendTitle.setText("Region Legend");
			title1.setText("NE US");
			title2.setText("SE US");
			title3.setText("Rest of US");
			title4.setText("Outisde of US");
		} else {
			return;
		}
		// add all shapes
		window.addShape(legendTitle);
		window.addShape(title1);
		window.addShape(title2);
		window.addShape(title3);
		window.addShape(title4);
		window.addShape(songtitle);
		window.addShape(middleRod);
		window.addShape(heard);
		window.addShape(likes);

		// add the outer black box
		window.addShape(legendInnerBox);
		window.addShape(legendOuterBox);

	}

}