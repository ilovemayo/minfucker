package binarySearchTree;

/**
 * test class for contact
 * 
 * @author davidweisiger
 * @version 12.9.17
 *
 */
public class ContactTest extends student.TestCase {
    /**
     * private variables
     */
    private Contact test1;
    private Contact test2;


    /**
     * sets up private variables
     */
    public void setUp() {
        test1 = new Contact("test1");
        test2 = new Contact("test2");
    }


    /**
     * test compareTo
     */
    public void testCompareTo() {
        assertEquals(0, (test1).compareTo(test2));
    }
}
