package groceries;

// -------------------------------------------------------------------------
/**
 * The Grocery Bag class is a bag data structure that holds String objects
 * that represent grocery store items.
 *
 * @author David Weisiger dbw
 * @version 9.22.2017
 */
public class GroceryBag extends ArrayBasedBag {
    // Constructors ..........................................................

    // ----------------------------------------------------------
    /**
     * Creates an empty bag using the default capacity.
     */
    public GroceryBag() {
        super(10);
    }


    // Public methods ........................................................
    /**
     * If an element is in both bags, then it will be in the
     * intersection. If there are multiple occurrences of that
     * element, then the number of occurrences of that element
     * in the intersection will equal the minimum number of occurrences
     * in either set.
     * 
     * Examples:
     * intersection of
     * ({"apple","apple","cereal","chips"},
     * {"chips", "apple","apple","chips","cake"})
     * = {"apple","apple","chips"}
     * 
     * @param bag
     *            Bag to be intersected with.
     * @return The intersection of the two bags.
     */
    public GroceryBag intersection(GroceryBag bag) {
        // Implement this method
        String[] contents1 = this.contents();
        String[] contents2 = bag.contents();

        GroceryBag obj = new GroceryBag();
        for (int i = 0; i < contents1.length; i++) {
            for (int j = 0; j < contents2.length; j++) {
                if (contents1[i] == contents2[j]) {
                    if (contents1[i] != null) {
                        obj.add(contents1[i]);
                        contents1[i] = null;
                        contents2[j] = null;
                    }
                }
            }
        }

        return obj;
    }


    /**
     * For two bags to be equal they need to contain items
     * with the exact same value (but not the same item, so
     * equality not identity). Order does not matter, but
     * number of occurrences does.
     * 
     * @param givenBag
     *            Other GroceryBag to be compared with for equality.
     * @return Returns true if the two bags have the same items.
     */
    @Override
    public boolean equals(Object givenBag) {
        // Implement this method
        boolean flag = false;

        if (givenBag == this) {
            return true;
        }
        if (givenBag == null) {
            return false;
        }
        if (this.getClass() == givenBag.getClass()) {
            GroceryBag obj = (GroceryBag)givenBag;
            // puts the contents into arrays
            String[] contents1 = this.contents();
            if (obj.size() != this.size()) {
                return false;
            }
            for (int i = 0; i < this.size(); i++) {
                if (this.occurrence(contents1[i]) == obj.occurrence(
                    contents1[i])) {
                    flag = true;
                }
                else 
                {
                    return false;
                }
            }
            return flag;
        }
        else 
        {
            return false;
        }
    }
}
