/**
 * 
 */
package spacecolonies;

/**
 * This test class will test the implementation of the ArrayQueue class.
 * 
 * @author henry argueta
 * @version 10/4/17
 *
 */
public class ArrayQueueTest extends student.TestCase {
    // ~ Fields -----------------------------------------------------

    private ArrayQueue<String> queue;
    private ArrayQueue<String> queue2;
    private Person person;
    // ~ setUp method -----------------------------------------------------


    /**
     * SetUp method will initiate the ArrayQueuet
     */
    public void setUp() {
        queue = new ArrayQueue<>();
        queue2 = new ArrayQueue<>();
        person = null;
        queue.enqueue("A");
        queue.enqueue("B");
        queue.enqueue("B");
        queue.enqueue("K");

    }


    // ~ test method -----------------------------------------------------

    /**
     * This method will test the length of the queue
     */
    public void testGetLength() {
        assertEquals(11, queue.getLength());
    }


    /**
     * This method will test the getSize method
     */
    public void testGetSize() {
        assertEquals(4, queue.getSize());
    }


    /**
     * This method will test the isFull and ensureCapacity method in the
     * ArrayQueue class.
     */
    public void testEnsureCapacity() {
        for (int i = 0; i < 23; i++) {
            queue2.enqueue("A");
        }
        assertEquals(23, queue2.getSize());
        assertEquals(44, queue2.getLength());

        queue2.enqueue("B");
        assertEquals("A", queue2.getFront());

    }


    /**
     * This method will test the enqueue method in the ArrayQueue class.
     */
    public void testEnqueue() {
        assertEquals(11, queue.getLength());
        assertEquals("A", queue.getFront());
        queue2 = new ArrayQueue<>();

        queue2.enqueue("A");
        queue2.enqueue("B");
        queue2.enqueue("B");
        queue2.enqueue("K");
        queue2.enqueue("A");
        queue2.enqueue("B");
        queue2.enqueue("B");
        queue2.enqueue("K");
        queue2.enqueue("A");
        queue2.enqueue("B");
        queue2.enqueue("B");
        queue2.enqueue("K");
        queue2.enqueue("A");
        queue2.enqueue("B");
        queue2.enqueue("B");
        queue2.enqueue("K");
        queue2.enqueue("K");
        queue2.enqueue("A");
        queue2.enqueue("B");
        queue2.enqueue("B");
        queue2.enqueue("K");
        queue2.enqueue("B");
        queue2.enqueue("B");

        queue.dequeue();
        queue.dequeue();
        queue.dequeue();

        assertEquals("K", queue.getFront());

    }


    /**
     * This method will test the isEmpty method in the ArrayQueue class.
     */
    public void testIsEmpty() {
        assertTrue(queue2.isEmpty());
        assertFalse(queue.isEmpty());
        queue.clear();
        // assertTrue(queue.isEmpty());

    }


    /**
     * This method will test the isEmpty method in the ArrayQueue class.
     */
    public void testDequeue() {
        assertEquals(null, queue2.dequeue());
        assertEquals("A", queue.dequeue());
        assertEquals(3, queue.getSize());
    }


    /**
     * This method will test the getFront method in the ArrayQueue class.
     */
    public void testGetFront() {
        assertEquals("A", queue.getFront());
        assertEquals(null, queue2.getFront());

    }


    /**
     * This method will test the toArray method in the ArrayQueue class.
     */
    public void testToArray() {
        Object[] array = queue.toArray();
        assertEquals(4, array.length);
        assertEquals("A", array[0]);

    }


    /**
     * This method will test the toString method in the ArrayQueue class.
     */
    public void testToString() {
        assertEquals("[A, B, B, K]", queue.toString());
        assertEquals("[]", queue2.toString());

    }


    /**
     * This method will test the Equals method in the ArrayQueue class.
     */
    public void testEquals() {

        ArrayQueue<String> queue3 = new ArrayQueue<>();
        assertFalse(queue.equals(queue3));
        queue3.enqueue("A");
        queue3.enqueue("B");
        queue3.enqueue("B");
        queue3.enqueue("K");
        assertFalse(queue.equals(person));
        person = new Person("Henry", 2, 2, 2, 2);
        assertFalse(queue.equals(person));
        assertTrue(queue.equals(queue3));
        assertTrue(queue3.equals(queue3));
        queue3.dequeue();
        assertFalse(queue.equals(queue3));
        queue3.enqueue("A");
        assertFalse(queue.equals(queue3));

    }

}
