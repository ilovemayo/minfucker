/**
 * 
 */
package prj5;

import java.io.File;

/**
 * @author rachelyueyy
 * @version 11.18.17
 */
public class SongInfoTest extends student.TestCase {

    /**
     * private variables
     */
    private SongInfo testSong;
    private String Rulala;
    private String genius;
    private String hoppop;


    /**
     * setup
     */
    public void setUp() {
        testSong = new SongInfo(Rulala, genius, 2017, hoppop);
    }


    /**
     * tests songs toString
     */
    public void testToString() {
        SurveyProcessor sp = new SurveyProcessor(new File(
            "InputFiles/MusicSurveyData.csv"), new File(
                "InputFiles/SongList.csv"));
        LinkedList<SongInfo> si = sp.getSongs();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < si.size(); i++) {
            sb.append(si.toString());
        }
        boolean noError = true;
        assertTrue(noError);

    }


    /**
     * tests iterate hobby heard
     */
    public void testIterateHobbyHeard() {
        String read = "reading";
        String heard = "heard";
        testSong.iterateHobby(read, heard);
        assertEquals(1, testSong.getReadingCountHeard());
        String art = "art";
        testSong.iterateHobby(art, heard);
        assertEquals(1, testSong.getArtCountHeard());
        String sports = "sports";
        testSong.iterateHobby(sports, heard);
        assertEquals(1, testSong.getSportsCountHeard());
        String musics = "musics";
        testSong.iterateHobby(musics, heard);
        assertEquals(1, testSong.getMusicCountHeard());
    }


    /**
     * tests iterate hobby liked
     */
    public void testIterateHobbyLiked() {
        String read = "reading";
        String liked = "liked";
        testSong.iterateHobby(read, liked);
        assertEquals(1, testSong.getReadingCountLiked());
        String art = "art";
        testSong.iterateHobby(art, liked);
        assertEquals(1, testSong.getArtCountLiked());
        String sports = "sports";
        testSong.iterateHobby(sports, liked);
        assertEquals(1, testSong.getSportsCountLiked());
        String musics = "musics";
        testSong.iterateHobby(musics, liked);
        assertEquals(1, testSong.getMusicCountLiked());
    }


    /**
     * tests compare to artist
     */
    public void testCompareToArtist() {
        SongInfo comp = null;
        Exception e = null;
        try {
            testSong.compareToArtist(comp);
        }
        catch (IllegalArgumentException thrown) {
            e = thrown;
        }
        assertTrue(e instanceof IllegalArgumentException);

    }


    /**
     * tests compare to year
     */
    public void testCompareToYear() {
        SongInfo comp = null;
        Exception e = null;
        try {
            testSong.compareToYear(comp);
        }
        catch (IllegalArgumentException thrown) {
            e = thrown;
        }
        assertTrue(e instanceof IllegalArgumentException);

    }


    /**
     * tests compare to genre
     */
    public void testCompareToGenre() {
        SongInfo comp = null;
        Exception e = null;
        try {
            testSong.compareToGenre(comp);
        }
        catch (IllegalArgumentException thrown) {
            e = thrown;
        }
        assertTrue(e instanceof IllegalArgumentException);

    }


    /**
     * tests compare to title
     */
    public void testCompareToTitle() {
        SongInfo comp = null;
        Exception e = null;
        try {
            testSong.compareToTitle(comp);
        }
        catch (IllegalArgumentException thrown) {
            e = thrown;
        }
        assertTrue(e instanceof IllegalArgumentException);

    }

}
