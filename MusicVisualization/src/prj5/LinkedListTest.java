package prj5;

import org.junit.Test;

/**
 * This is the test class for linkedList
 * 
 * @author davidbauer
 * @author Yue Yang
 * @author david weisiger
 * @version 11.19.17
 * @param <T>
 *            for generic type which will be added
 */
public class LinkedListTest<T> extends student.TestCase {

    private LinkedList<T> tester;
    private LinkedList<T> tester2;
    private SongInfo a;
    private SongInfo b;
    private SongInfo c;
    private SongInfo d;


    /**
     * sets up before each method
     * 
     * @throws Exception
     */
    public void setUp() throws Exception {
        tester = new LinkedList<T>();
        tester2 = new LinkedList<T>();
        a = new SongInfo("a", "a", 1, "apple");
        b = new SongInfo("b", "b", 1, "bapple");
        c = new SongInfo("c", "c", 1, "capple");
        d = new SongInfo("d", "d", 1, "dapple");
    }


    /**
     * tests size method
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testSize() {
        assertEquals(tester.size(), 0);
        tester.add(0, (T)a);
        assertEquals(tester.size(), 1);
        tester.add(1, (T)a);
        assertEquals(tester.size(), 2);
        tester.add((T)a);
        assertEquals(tester.size(), 3);
        tester.remove(0);
        assertEquals(tester.size(), 2);
        tester.remove((T)a);
        assertEquals(tester.size(), 1);
    }


    /**
     * tests add method
     */
    @SuppressWarnings("unchecked")
    public void testAdd() {
        Exception thrown = null;
        try {
            tester.add(null);
        }
        catch (Exception exception) {
            thrown = exception;
        }
        // checks whether an Exception was actually thrown
        assertNotNull(thrown);
        Exception thrown2 = null;
        try {
            tester.add(-1, (T)a);
        }
        catch (Exception exception) {
            thrown2 = exception;
        }
        // checks whether an Exception was actually thrown
        assertNotNull(thrown2);
        Exception thrown3 = null;
        try {
            tester.add(17, (T)a);
        }
        catch (Exception exception) {
            thrown3 = exception;
        }
        // checks whether an Exception was actually thrown
        assertNotNull(thrown3);
        Exception thrown4 = null;
        try {
            tester.add(0, null);
        }
        catch (Exception exception) {
            thrown4 = exception;
        }
        // checks whether an Exception was actually thrown
        assertNotNull(thrown4);
        tester.add(0, (T)a);
        assertEquals(tester.get(0), a);
        tester.add(0, (T)b);
        assertEquals(tester.get(0), a);

        assertEquals(tester.size(), 1);
        tester.add((T)c);
        assertEquals(tester.get(0), a);
        tester.remove(0);
        assertEquals(tester.size(), 1);
        assertEquals(tester.get(0), c);
        tester.remove((T)c);
        assertEquals(tester.size(), 0);
    }


    /**
     * tests empty method
     */
    @SuppressWarnings("unchecked")
    public void testIsEmpty() {
        assertTrue(tester.isEmpty());
        tester.add((T)a);
        assertFalse(tester.isEmpty());
        tester.remove(0);
        assertTrue(tester.isEmpty());
        tester.add((T)a);
        tester.add((T)a);
        assertFalse(tester.isEmpty());
        tester.remove((T)a);
        tester.remove((T)a);
        assertTrue(tester.isEmpty());
    }


    /**
     * tests remove method
     */
    @SuppressWarnings("unchecked")
    public void testRemove() {
        Exception thrown = null;
        try {
            tester.remove(-1);
        }
        catch (Exception exception) {
            thrown = exception;
        }
        // checks whether an Exception was actually thrown

        assertNotNull(thrown);
        Exception thrown2 = null;
        try {
            tester.remove(0);
        }
        catch (Exception exception) {
            thrown2 = exception;
        }
        // checks whether an Exception was actually thrown

        assertNotNull(thrown2);
        assertFalse(tester.remove((T)a));
        tester.add(0, (T)a);
        tester.add((T)c);
        assertEquals(tester.get(0), a);
        assertTrue(tester.remove(0));
        assertEquals(tester.get(0), c);
        assertEquals(tester.size(), 1);
        assertTrue(tester.remove(0));
        assertTrue(tester.isEmpty());
        tester.add((T)a);
        tester.add((T)b);
        tester.add((T)c);
        tester.add((T)d);
        tester.remove((T)c);
        assertEquals(tester.size(), 3);
        assertFalse(tester.contains((T)c));
        tester.remove((T)d);
        assertEquals(tester.size(), 2);
        assertFalse(tester.contains((T)d));
        tester.clear();
        tester.add((T)c);
        assertTrue(tester.remove((T)c));
        tester.clear();
        tester.add(0, (T)a);
        tester.add(1, (T)b);
        tester.add(2, (T)c);
        tester.add(3, (T)d);
        tester.remove(1);
        assertEquals(tester.size(), 3);
        assertFalse(tester.contains((T)b));
        tester.clear();
        tester.add((T)a);
        tester.add((T)b);
        tester.add((T)c);
        tester.add((T)d);
        tester.add((T)a);
        tester.add((T)b);
        tester.add((T)c);
        tester.add((T)d);
        tester.add((T)a);
        tester.add((T)b);
        tester.add((T)c);
        tester.add((T)d);
        assertTrue(tester.remove(10));
        assertTrue(tester.remove((T)d));
        assertTrue(tester.remove((T)a));
        tester.clear();
        tester.add((T)a);
        tester.add((T)b);
        assertTrue(tester.remove((T)a));
    }


    /**
     * tests get method
     */
    @SuppressWarnings("unchecked")
    public void testGet() {
        Exception thrown = null;
        try {
            tester.get(0);
        }
        catch (Exception exception) {
            thrown = exception;
        }
        // checks whether an Exception was actually thrown
        assertNotNull(thrown);
        tester.add(0, (T)a);
        tester.add(1, (T)b);
        tester.add(2, (T)c);
        tester.add(3, (T)d);
        assertEquals(tester.get(0), (T)a);
        assertEquals(tester.get(1), (T)b);
        assertEquals(tester.get(2), (T)c);
        assertEquals(tester.get(3), (T)d);
    }


    /**
     * tests contains method
     */
    @SuppressWarnings("unchecked")
    public void testContains() {
        assertFalse(tester.contains((T)a));
        tester.add(0, (T)b);
        tester.add(1, (T)c);
        tester.add(2, (T)d);
        assertFalse(tester.contains((T)a));
        assertTrue(tester.contains((T)d));
        assertTrue(tester.contains((T)c));
        assertTrue(tester.contains((T)b));
        tester.clear();
        assertFalse(tester.contains((T)d));
        assertFalse(tester.contains((T)b));

    }


    /**
     * tests toString method
     */
    @SuppressWarnings("unchecked")
    public void testToString() {
        assertEquals(tester.toString(), "{}");
        tester.add((T)a);
        assertEquals(tester.toString(), "{" + a.toString() + "}");
        tester.add(1, (T)b);
        tester.add(2, (T)c);
        assertEquals(tester.toString(), "{" + a.toString() + ", " + b.toString()
            + ", " + c.toString() + "}");
        tester.remove(1);
        tester.remove(1);
        assertEquals(tester.toString(), "{" + a.toString() + "}");
        tester.clear();

    }


    /**
     * tests sort genre method
     */
    @SuppressWarnings("unchecked")
    public void testSortByGenre() {

        tester.sortbyGenre();
        tester2.add((T)a);
        tester2.add((T)b);
        tester2.add((T)c);
        tester2.add((T)d);
        tester.add((T)c);
        tester.add((T)a);
        tester.add((T)d);
        tester.add((T)b);

    }


    /**
     * tests sort genre method
     */
    @SuppressWarnings("unchecked")
    public void testSortByArtist() {
        tester.sortbyArtist();
        tester2.add((T)a);
        tester2.add((T)b);
        tester2.add((T)c);
        tester2.add((T)d);
        tester.add((T)c);
        tester.add((T)a);
        tester.add((T)d);
        tester.add((T)b);
        tester.sortbyArtist();

    }


    /**
     * tests sort genre method
     */
    @SuppressWarnings("unchecked")
    public void testSortByYear() {

        tester.sortbyYear();
        tester2.add((T)a);
        tester2.add((T)b);
        tester2.add((T)c);
        tester2.add((T)d);
        tester.add((T)c);
        tester.add((T)a);
        tester.add((T)d);
        tester.add((T)b);

    }


    /**
     * tests sort genre method
     */
    @SuppressWarnings("unchecked")
    public void testSortByTitle() {

        tester.sortbyTitle();
        tester2.add((T)a);
        tester2.add((T)b);
        tester2.add((T)c);
        tester2.add((T)d);
        tester.add((T)c);
        tester.add((T)a);
        tester.add((T)d);
        tester.add((T)b);
        tester.sortbyTitle();

    }

}
