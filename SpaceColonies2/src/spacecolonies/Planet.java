/**
 * 
 */
package spacecolonies;

/**
 * This class contain objects of string, for the planet�s name, three ints for
 * their minimum skill requirements (on a scale of 1 to 5), an array of Person
 * objects for current planet population, an int for storing the current
 * population size, and a final int for the maximum allowed capacity of the
 * planet.
 * 
 * @author henry argueta
 * @version 11/1/2017
 *
 */
public class Planet implements Comparable<Planet> {
    // ~ Fields -----------------------------------------------------

    /**
     * The String class represents character strings
     */
    private String name;

    /**
     * This class will contain three ints for their skills(on a scale of 1 to
     * 5).
     */
    private Skills minSkills;

    /**
     * This class will contain a string, for a person�s name, a skills object,
     * and an int representation of their planet preference.
     */
    private Person[] population;

    /**
     * Constructs a newly allocated Integer object that represents the specified
     * int value.
     */
    private int populationSize;

    /**
     * Constructs a newly allocated Integer object that represents the specified
     * int value.
     */
    private final int capacity;

    /**
     * The Planet constructor should take 5 parameters, the planet's name, the
     * minimum skill level required in all three areas and the maximum capacity
     * for that planet. This data should be listed on each line for the planet
     * input file.
     * 
     * @param planetName
     *            name of planet
     * @param planetAgri
     *            Agriculture skill requirement
     * @param planetMedi
     *            Medical skill requirement
     * @param planetTech
     *            Technical skill requirement
     * @param planetCap
     *            capacity for planet
     */

    // ~ constructor -----------------------------------------------------

    public Planet(String planetName, int planetAgri, int planetMedi,
            int planetTech, int planetCap) {

        name = planetName;
        populationSize = 0;
        minSkills = new Skills(planetAgri, planetMedi, planetTech);
        capacity = planetCap;
        population = new Person[planetCap + 1];
    }

    // ~ Methods -----------------------------------------------------

    /**
     * This method returns the name of the planet.
     *
     * @return name of planet left in planet
     */
    public String getName() {
        return name;

    }

    /**
     * This method sets the name of the planet.
     *
     * @return nameSet of planet left in planet
     * @param nameSet name to be set
     */
    public String setName(String nameSet) {
        return nameSet;

    }

    /**
     * This method gets the Skills of the planet.
     *
     * @return minSkills of planet min requirement
     */
    public Skills getSkills() {
        return minSkills;

    }

    /**
     * This method gets the population of people.
     *
     * @return population people in planet
     */
    public Person[] getPopulation() {
        return population;
    }

    /**
     * This method gets the population size.
     *
     * @return populationSize of planet
     */
    public int getPopulationSize() {
        return populationSize;

    }

    /**
     * This method gets the planets capacity.
     *
     * @return capacity of planet
     */
    public int getCapacity() {
        return capacity;

    }

    /**
     * This method should return the number of available places left in the
     * planet, by using the planet�s capacity and current population size.
     *
     * @return available spots left in planet
     */
    public int getAvailability() {
        return capacity - populationSize;

    }

    /**
     * This method should return true if the planet�s population has reached max
     * capacity.
     *
     * @return true planet is full
     */
    public boolean isFull() {
        return capacity == populationSize;
    }

    /**
     * This method will attempt to add a Person to the Planet. It will need to
     * check two things. First, that the colony has available space for this
     * applicant. Secondly, it will need to determine whether the applicant is
     * qualified to live on the colony.
     *
     * @return availablePlaces left in planet
     */
    public boolean addPerson(Person newbie) {
        if (!isFull() && !minSkills.isBelow(newbie.getSkills()))
        {
            population[populationSize] = newbie;
            populationSize++;
            return true;
        }
        return false;

    }

    /**
     * Using a StringBuilder, concatenate the name, �population� followed by the
     * current population size. Next concatenate the capacity of the planet, and
     * then the required skill values in the following format:"Caturn,
     * population 5 (cap: 10), Requires: A >= 3, M >= 2, T >= 1"
     * 
     * @return String.valueOf(builder) the string reputation
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(getName() + ", population " + getPopulationSize()
                + " (cap: " + capacity + "), Requires: A >= "
                + minSkills.getAgriculture() + ", M >= "
                + minSkills.getMedicine() + ", T >="
                + minSkills.getTechnology());
        return String.valueOf(builder);
    }

    /**
     * Two planets are equal if all 5 their input fields are equal and
     * populationSize is equal
     *
     * @return true if equal
     * @param obj
     *            planet to check if equal
     */
    public boolean equals(Object obj) {
        if (obj == null)
        {
            return false;
        }

        Planet objs = (Planet) obj;

        return this.getName().equals(objs.getName())
                && this.getPopulationSize() == objs.getPopulationSize()
                && this.getSkills().equals(objs.getSkills())
                && this.getCapacity() == objs.getCapacity();

    }

    /**
     * Planet should implement the Comparable Interface and the compareTo method
     * should order the planets based on availability. A planet with more
     * available slots will be greater than one with less. Note that this is
     * slightly different than how equals works. compareTo will return 0 if
     * populatoinSize is equal regardless of whether the rest of the planet
     * attributes are the same.
     *
     * @return 0 if equal
     * @param other
     *            other planet to be compared.
     */
    public int compareTo(Planet other) {
        
        if(other == null) {
            
        }
        return this.getAvailability() - other.getAvailability();

    }

}
