package employees;

/**
 * this class extends the employee class
 * 
 * @author David Weisiger dbw
 * @version September 8, 2017
 *
 */
public class ExternalContractor extends Employee {
    /**
     * this construcuts the object
     * 
     * @param name
     *            is the name
     * @param hourlyRate
     *            is the hourly rate
     */

    public ExternalContractor(String name, double hourlyRate) {
        super(name, hourlyRate);
    }


    /**
     * overloads the hourlyrate method
     * 
     * @precondition parameter must be positive
     * @param rank
     *            determines the hourly pay
     * @return hourly rate
     */
    public double getHourlyRate(int rank) {
        if (rank >= 3) {
            return 45.50;
        }
        else if (rank == 2) {
            return 41.75;
        }
        else {
            return 38.50;
        }
    }


    /**
     * The amount paid per week.
     * 
     * @precondition custRank must be positive
     * @param hours
     *            The number of hours worked per week.
     * @param custRank
     *            The customer's rank (1 - 3)
     * @return Returns the amount paid for a certain week.
     */

    public double weeklyPay(int hours, int custRank) {
        return this.getHourlyRate(custRank) * hours;
    }
    
    public static void main(String[] args) {
        ExternalContractor test = new ExternalContractor("blah", 2);
        System.out.println(test.getHourlyRate() + " - " + test.getHourlyRate(3));
    }

}
