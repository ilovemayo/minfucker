package arraystack;

import java.util.EmptyStackException;

/**
 * This is an array based stack class
 * 
 * @author davidweisiger
 * @version friday, october 6th, 2017
 *
 * @param <T>
 *            is cool
 */

public class ArrayBasedStack<T> implements StackADT<T> {
    /**
     * field vairables
     */
    private T[] elements;
    private int size;
    private int capacity;


    /**
     * constructor
     * 
     */

    public ArrayBasedStack() {
        this(100);
    }


    /**
     * overloaded constructor
     * 
     * @param capacity
     *            does that
     */

    public ArrayBasedStack(int capacity) {
        @SuppressWarnings("unchecked")
        T[] tempElements = (T[])new Object[capacity];
        elements = tempElements;

        this.capacity = capacity;
        this.size = 0;
    }


    /**
     * Checks if the stack is empty.
     *
     * @return {@code true} if the stack is empty;
     *         {@code false} otherwise.
     */
    public boolean isEmpty() {
        return size == 0;
    }


    /**
     * Returns the item at the top of the stack without removing it.
     *
     * @throws EmptyStackException
     *             if stack is empty.
     *
     * @return the item at the top of the stack.
     */
    public T peek() throws EmptyStackException {
        if (size == 0) {
            throw new EmptyStackException();
        }

        return elements[size - 1];
    }


    /**
     * Removes the item at the top of the stack.
     *
     * @throws EmptyStackException
     *             if stack is empty.
     *
     * @return the item at the top of the stack.
     */
    public T pop() throws EmptyStackException {
        if (size == 0) {
            throw new EmptyStackException();
        }
        size--;
        return elements[size];
    }


    /**
     * Pushes an item onto the stack.
     *
     * @param item
     *            the item to be pushed onto the stack.
     */
    @SuppressWarnings("unchecked")
    @Override
    public void push(Object item) {
        if (size == capacity - 1) {
            this.expandCapacity();
        }

        elements[size] = (T)item;
        size++;

    }


    /**
     * Checks if an item is in the stack.
     *
     * @param item
     *            the item to be looked for.
     *
     * @return {@code true}, if the item is somewhere in the stack;
     *         {@code false} otherwise.
     */
    @Override
    public boolean contains(Object item) {
        for (int i = 0; i < this.size(); i++) {
            if (elements[i].equals(item)) {
                return true;
            }
        }
        return false;
    }


    /**
     * Number of items in the stack.
     *
     * @return the number of items in the stack.
     */
    public int size() {
        return size;
    }


    /**
     * Clears the stack (removes all of the items from the stack).
     */
    public void clear() {
        for (int i = 0; i < size; i++) {

            elements[i] = null;
        }
        size = 0;
    }


    /**
     * Returns an array with a copy of each element in the stack with the top of
     * the stack being the last element
     *
     * @return the array representation of the stack
     */
    public T[] toArray() {
        return elements;
    }


    /**
     * Returns the string representation of the stack.
     * 
     * [] (if the stack is empty)
     * [bottom, item, ..., item, top] (if the stack contains items)
     * 
     * @return the string representation of the stack.
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        for (T element : elements) {
            sb.append(element + " ");
        }
        return sb.deleteCharAt(sb.length() - 1).append("]").toString();
    }


    /**
     * Two stacks are equal iff they both have the same size and contain the
     * same elements in the same order.
     *
     * @param other
     *            the other object to compare to this
     *
     * @return {@code true}, if the stacks are equal;
     *         {@code false} otherwise.
     */
    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null) {
            return false;
        }
        if (this.getClass().equals(other.getClass())) {
            ArrayBasedStack<?> otherStack = (ArrayBasedStack<?>)other;
            if (this.size() != otherStack.size()) {
                return false;
            }
            Object[] otherArray = otherStack.toArray();
            for (int i = 0; i < this.size(); i++) {
                if (!(this.elements[i].equals(otherArray[i]))) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }


    /**
     * Expands the capacity of the stack by doubling its current capacity.
     */
    private void expandCapacity() {

        @SuppressWarnings("unchecked")
        T[] newArray = (T[])new Object[this.capacity * 2];

        for (int i = 0; i < this.capacity; i++) {
            newArray[i] = this.elements[i];
        }

        this.elements = newArray;
        this.capacity *= 2;
    }

}
