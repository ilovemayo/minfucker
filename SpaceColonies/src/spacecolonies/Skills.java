package spacecolonies;

/**
 * This class is a wrapper for the individual skills of a Person
 *
 * @author dbw
 * @version 11.06.17
 */
public class Skills {
    /**
     * field variables
     */
    private int agriculture;
    private int medicine;
    private int technology;


    /**
     * constructor
     *
     * @param ag
     *            agriculture
     * @param med
     *            medicine
     * @param tech
     *            technology
     */
    public Skills(int ag, int med, int tech) {
        agriculture = ag;
        medicine = med;
        technology = tech;
    }


    /**
     * @return agriculture
     */
    public int getAgriculture() {
        return agriculture;
    }


    /**
     * @return medicine
     */
    public int getMedicine() {
        return medicine;
    }


    /**
     * @return technology
     */
    public int getTechnology() {
        return technology;
    }


    /**
     * @param other
     *            is the other skill
     * @return true if all three skills are less than those in other, false
     *         otherwise
     */
    public boolean isBelow(Skills other) {
        return agriculture <= other.agriculture && medicine <= other.medicine
            && technology <= other.technology;
    }


    /**
     * @param obj
     *            being compared to
     * @return true if all skills are equal, false otherwise
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() == obj.getClass()) {
            Skills other = (Skills)obj;

            return agriculture == other.agriculture
                && medicine == other.medicine && technology == other.technology;
        }
        else {
            return false;
        }
    }


    /**
     * @return string representation of skills
     */
    @Override
    public String toString() {
        return "A:" + agriculture + " M:" + medicine + " T:" + technology;
    }
}
