/**
 * 
 */
package spacecolonies;

import java.io.FileNotFoundException;

/**
 * @author henry
 *
 */
public class ColonyReaderTest extends student.TestCase{

    private ColonyReader colonyReader;
    private String pathP = "C:/Users/henry/eclipse-workspace/SpaceColoniesInputFilesCleanAndErrors/planets1.txt";
    private String pathI = "C:/Users/henry/eclipse-workspace/SpaceColoniesInputFilesCleanAndErrors/input.txt";

    public void setUp() throws SpaceColonyDataExeption, FileNotFoundException {
        colonyReader = new ColonyReader(pathI,pathP);
    }
    
    public void testReadPlanetFile() throws SpaceColonyDataExeption, FileNotFoundException {
        assertEquals("Techies",colonyReader.readPlanetFile(pathP)[3].getName());
        assertEquals(8,colonyReader.readPlanetFile(pathP)[3].getAvailability());


        assertEquals(10,colonyReader.readPlanetFile(pathP)[1].getCapacity());
        Skills skills = new Skills(5, 3, 1);
        Planet planet = new Planet("Aggies",5, 2, 2, 10);
        assertEquals(skills,colonyReader.readPlanetFile(pathP)[1].getSkills());
        assertEquals(planet,colonyReader.readPlanetFile(pathP)[1]);

    }
    
    public void testReadQueueFile() throws SpaceColonyDataExeption, FileNotFoundException {
        assertEquals(23,colonyReader.readQueueFile(pathI).getSize());
        assertEquals(1,colonyReader.readQueueFile(pathI).getFront().getPlanet());

    }
    
    public void testIsInSkillRange() throws SpaceColonyDataExeption, FileNotFoundException {

        assertFalse(colonyReader.isinSkillRange(1, 0, 5));
        assertTrue(colonyReader.isinSkillRange(1, 5, 5));

    }
    
}
