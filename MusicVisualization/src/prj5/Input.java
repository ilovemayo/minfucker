package prj5;

import java.io.File;
import java.util.Iterator;

/**
 * This is our driver class which opens the GUI and prints out SongInfo to the
 * console
 * 
 * @author davidbauer
 * @author Yue Yang
 * @author david weisiger
 * @version 11.19.17
 */
public class Input {

    /**
     * This is the main method which will run the whole project
     * 
     * @param args
     *            take in two filenames
     */
    public static void main(String[] args) {
        File surveyData = new File("InputFiles/" + args[0]);
        File songList = new File("InputFiles/" + args[1]);

        // processes the files and opens the GUI
        SurveyProcessor surveyProcessor = new SurveyProcessor(surveyData,
            songList);
        @SuppressWarnings("unused")
        GUISurveyResult musicWindow = new GUISurveyResult(surveyProcessor);

        // gets songs by title and genre
        LinkedList<SongInfo> songs = surveyProcessor.getSongs();
        songs.sortbyGenre();
        Iterator<SongInfo> itr = songs.iterator();
        while (itr.hasNext())
            System.out.print(itr.next().toString() + " ");

    }
}
