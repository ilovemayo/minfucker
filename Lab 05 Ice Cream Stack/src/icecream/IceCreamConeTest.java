package icecream;

/**
 * 
 * This class tests the ice cream cone class
 * 
 * @author davidweisiger
 * @version September 29, 2017
 *
 */

public class IceCreamConeTest extends student.TestCase {
    /**
     * instance variables
     */
    private IceCreamCone test1;
    private IceCreamCone test2;
    private IceCreamCone test3;
    private IceCreamCone test4;
    
    /**
     * set ups
     */
    public void setUp() {
        test1 = new IceCreamCone();
        test1.addScoop("vanilla");
        test2 = new IceCreamCone();
        test2.addScoop("chocolate");
        test3 = new IceCreamCone();
        test3.addScoop("vanilla");
        test4 = new IceCreamCone();
        
    }
    
    /**
     * tests the equals methods
     */
    
    public void testEquals() {
        String blah = "hi";
        String blah1 = null;
        assertFalse(test1.equals(blah));
        assertTrue(test1.equals(test1));
        assertFalse(test1.equals(blah1));
        assertTrue(test1.equals(test3));
        assertFalse(test1.equals(test2));
        
    }
    
    /**
     * tests the eat scoop
     */
    
    public void testEatScoop() {
        assertEquals(test1.eatScoop(), "vanilla");
    }
    
    /**
     * tests the add scoop
     */
    
    public void testAddScoop() {
        test1.addScoop("chocolate");
        assertEquals(test1.eatScoop(), "chocolate");
    }
    
    /**
     * tests the num scoops
     */
    
    public void testNumScoops() {
        assertEquals(test1.numScoops(), 1);
        assertEquals(test4.numScoops(), 0);
    }
    
    /**
     * test the contains
     */
    
    public void testContains() {
        assertTrue(test1.contains("vanilla"));
        assertFalse(test1.contains("chocolate"));
    }
    
    /**
     * tests the empty cone
     */
    
    public void testEmptyCone() {
        assertFalse(test1.emptyCone());
        test1.eatScoop();
        assertTrue(test1.emptyCone());
    }
    
    /**
     * tests the current scoop
     */

    public void testCurrentScoop() {
        assertEquals(test1.currentScoop(), "vanilla");
        
    }
    
    /**
     * tests the to string method
     */
    
    public void testToString() {
        assertEquals(test1.toString(), "[vanilla]");
    }
}
