package cybersecurity.lab1;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * This class tests the LinkedKVStore class
 * 
 * @author dbw
 * @version 11.16.17
 *
 */
public class LinkedKVStoreTest extends student.TestCase {
    /**
     * field variables
     */
    private LinkedKVStore<Integer, String> linkedStore;
    private Iterator<Integer> itr;


    /**
     * overrides setUp in student.TestCase and instantiates field variables
     */
    @Override
    public void setUp() {
        linkedStore = new LinkedKVStore<Integer, String>();
        itr = linkedStore.iterator();
    }


    /**
     * tests the isEmpty method
     */
    public void testIsEmpty() {
        assertTrue(linkedStore.isEmpty());
        linkedStore.put(1, "one");
        assertFalse(linkedStore.isEmpty());
    }


    /**
     * tests the size method
     */
    public void testSize() {
        assertEquals((int)0, linkedStore.size());
        linkedStore.put(1, "one");
        assertEquals((int)1, linkedStore.size());
    }


    /**
     * tests the clear method
     */
    public void testClear() {
        linkedStore.put(1, "one");
        assertEquals((int)1, linkedStore.size());
        linkedStore.clear();
        assertEquals((int)0, linkedStore.size());
    }


    /**
     * tests the put and get method
     */
    public void testPutAndGet() {
        assertNull(linkedStore.get(1));
        linkedStore.put(1, "one");
        assertEquals("one", linkedStore.get(1));
    }


    /**
     * tests the contains method
     */
    public void testContains() {
        linkedStore.put(2, "hey");
        linkedStore.put(3, "sup");
        assertFalse(linkedStore.contains(1));
        linkedStore.put(1, "test");
        assertTrue(linkedStore.contains(1));
    }


    /**
     * tests the addFirst method
     */
    public void testAddFirst() {
        linkedStore.put(1, "one");
        linkedStore.addFirst(2, "two");
        assertEquals(2, itr.next().intValue());
        assertEquals(1, itr.next().intValue());
    }


    /**
     * tests the addLast method
     */
    public void testAddLast() {
        linkedStore.put(1, "1.one");
        linkedStore.addLast(1, "1.two");
        assertEquals(linkedStore.getLast(1), "1.two");
    }


    /**
     * tests the removeFirst method
     */
    public void testRemoveFirst() {
        assertFalse(linkedStore.removeFirst(1));
        linkedStore.put(2, "sup");
        linkedStore.put(1, "one");
        linkedStore.put(2, "hey");
        linkedStore.put(1, "two");
        assertTrue(linkedStore.contains(1));
        assertTrue(linkedStore.removeFirst(1));
        assertTrue(linkedStore.removeFirst(2));
        linkedStore.removeFirst(1);
        assertFalse(linkedStore.removeFirst(4));
        assertFalse(linkedStore.contains(1));
    }


    /**
     * tests the removeLastMethod()
     */
    public void testRemoveLast() {
        assertFalse(linkedStore.removeLast(1));
        linkedStore.put(2, "sup");
        linkedStore.put(1, "one");
        linkedStore.put(2, "hey");
        linkedStore.put(1, "two");
        assertTrue(linkedStore.contains(1));
        assertTrue(linkedStore.removeLast(1));
        assertTrue(linkedStore.removeLast(2));
        linkedStore.removeLast(1);
        assertFalse(linkedStore.removeLast(4));
        assertFalse(linkedStore.contains(1));
    }


    /**
     * tests the getFirst method
     */
    public void testGetFirst() {
        linkedStore.put(1, "one");
        linkedStore.addFirst(2, "two");
        assertEquals(2, itr.next().intValue());
        assertEquals(1, itr.next().intValue());
        assertEquals("two", linkedStore.getFirst(2));
        assertEquals("one", linkedStore.getFirst(1));
        assertNull(linkedStore.getFirst(3));
    }


    /**
     * tests the getLast method
     */
    public void testGetLast() {
        linkedStore.put(1, "hi");
        linkedStore.put(2, "bye");
        linkedStore.put(1, "bye");
        assertEquals("bye", linkedStore.getLast(1));
        assertEquals("bye", linkedStore.getLast(2));
    }


    /**
     * tests toString method
     */
    public void testToString() {
        linkedStore.put(1, "one");
        linkedStore.put(1, "two");
        assertEquals("[1, 1]", linkedStore.toString());
    }


    /**
     * tests the exception thrown in Iterator
     */
    public void testIteratorException() {
        Exception thrown = null;
        try {
            itr.next();
        }
        catch (Exception e) {
            thrown = e;
        }
        assertTrue(thrown instanceof NoSuchElementException);
    }
}
