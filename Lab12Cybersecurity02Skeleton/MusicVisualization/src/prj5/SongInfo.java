package prj5;

/**
 * 
 * @author davidbauer + 2 others
 * @version 11.18.17
 */
public class SongInfo {
    private String title;
    private String artist;
    private int year;
    private String genre;

    // count for # of people in a hobby that heard the song
    private int readingCountHeard;
    private int artCountHeard;
    private int sportsCountHeard;
    private int musicCountHeard;
    // count for # of people in a hobby that liked the song
    private int readingCountLiked;
    private int artCountLiked;
    private int sportsCountLiked;
    private int musicCountLiked;

    // total number of people who answered survey
    private int[] numPeopleAnsweredLiked = { 0, 0, 0, 0 };
    private int[] numPeopleAnsweredHeard = { 0, 0, 0, 0 };


    /**
     * 
     * @param title
     *            of a String
     * @param artist
     *            of a String
     * @param year
     *            of an int
     * @param genre
     *            of a String
     */
    public SongInfo(String title, String artist, int year, String genre) {
        this.title = title;
        this.artist = artist;
        this.year = year;
        this.genre = genre;

        // sets all counts to 0
        readingCountHeard = 0;
        artCountHeard = 0;
        sportsCountHeard = 0;
        musicCountHeard = 0;
        readingCountLiked = 0;
        artCountLiked = 0;
        sportsCountLiked = 0;
        musicCountLiked = 0;

    }


    /**
     * iterates people liked
     * 
     * @param hobby
     *            of a String
     */
    public void iteratePeopleLiked(String hobby) {
        if (hobby.equals("reading")) {
            numPeopleAnsweredLiked[0]++;
        }
        else if (hobby.equals("art")) {
            numPeopleAnsweredLiked[1]++;
        }
        else if (hobby.equals("sports")) {
            numPeopleAnsweredLiked[2]++;
        }
        else if (hobby.equals("music")) {
            numPeopleAnsweredLiked[3]++;
        }

    }


    /**
     * iterates people heard
     * 
     * @param hobby
     *            of heard
     */
    public void iteratePeopleHeard(String hobby) {
        if (hobby.equals("reading")) {
            numPeopleAnsweredHeard[0]++;
        }
        else if (hobby.equals("art")) {
            numPeopleAnsweredHeard[1]++;
        }
        else if (hobby.equals("sports")) {
            numPeopleAnsweredHeard[2]++;
        }
        else if (hobby.equals("music")) {
            numPeopleAnsweredHeard[3]++;
        }
    }


    /**
     * iterates counters
     * 
     * @param hobby
     *            of a String
     * @param heardOrLiked
     *            of a String
     */

    public void iterateHobby(String hobby, String heardOrLiked) {
        if (hobby.equalsIgnoreCase("reading")) {
            if (heardOrLiked.equals("heard")) {
                readingCountHeard++;
            }
            else {
                readingCountLiked++;
            }
        }
        else if (hobby.equalsIgnoreCase("art")) {
            if (heardOrLiked.equals("heard")) {
                artCountHeard++;
            }
            else {
                artCountLiked++;
            }
        }
        else if (hobby.equalsIgnoreCase("sports")) {
            if (heardOrLiked.equals("heard")) {
                sportsCountHeard++;
            }
            else {
                sportsCountLiked++;
            }
        }
        else if (hobby.equalsIgnoreCase("music")) {
            if (heardOrLiked.equals("heard")) {
                musicCountHeard++;
            }
            else {
                musicCountLiked++;
            }
        }
    }


    /**
     * overrides object tostring method
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Song Title: " + title);
        sb.append("\nSong Artist: " + artist);
        sb.append("\nSong Genre: " + genre);
        sb.append("\nSong Year: " + year);

        // people who heard
        sb.append("\nHeard\n");
        double readingHeardPercent = 0;
        double artHeardPercent = 0;
        double sportsHeardPercent = 0;
        double musicHeardPercent = 0;

        if (numPeopleAnsweredHeard[0] != 0) {
            readingHeardPercent += (double)readingCountHeard
                / (double)numPeopleAnsweredHeard[0];
        }
        if (numPeopleAnsweredHeard[1] != 0) {
            artHeardPercent += (double)artCountHeard
                / (double)numPeopleAnsweredHeard[1];
        }
        if (numPeopleAnsweredHeard[2] != 0) {
            sportsHeardPercent += (double)sportsCountHeard
                / (double)numPeopleAnsweredHeard[2];
        }
        if (numPeopleAnsweredHeard[3] != 0) {
            musicHeardPercent += (double)musicCountHeard
                / (double)numPeopleAnsweredHeard[3];
        }
        sb.append("reading:" + (int)(readingHeardPercent * 100) + " art:"
            + (int)(artHeardPercent * 100) + " sports:"
            + (int)(sportsHeardPercent * 100) + " music:"
            + (int)(musicHeardPercent * 100));

        // people who liked
        sb.append("\nLikes\n");
        double readingLikesPercent = 0;
        double artLikesPercent = 0;
        double sportsLikesPercent = 0;
        double musicLikesPercent = 0;

        if (numPeopleAnsweredLiked[0] != 0) {
            readingLikesPercent += (double)readingCountLiked
                / (double)numPeopleAnsweredLiked[0];
        }
        if (numPeopleAnsweredLiked[1] != 0) {
            artLikesPercent += (double)artCountLiked
                / (double)numPeopleAnsweredLiked[1];
        }
        if (numPeopleAnsweredLiked[2] != 0) {
            sportsLikesPercent += (double)sportsCountLiked
                / (double)numPeopleAnsweredLiked[2];
        }
        if (numPeopleAnsweredLiked[3] != 0) {
            musicLikesPercent += (double)musicCountLiked
                / (double)numPeopleAnsweredLiked[3];
        }
        sb.append("reading:" + (int)(readingLikesPercent * 100) + " art:"
            + (int)(artLikesPercent * 100) + " sports:"
            + (int)(sportsLikesPercent * 100) + " music:"
            + (int)(musicLikesPercent * 100));

        return sb.toString();
    }


    /**
     * compares artist
     * 
     * @param comp
     *            is that
     * @return artist difference
     */
    public int compareToArtist(SongInfo comp) {
        if (comp == null) {
            throw new IllegalArgumentException();
        }
        int temp = this.artist.compareTo(comp.artist);
        return temp;
    }


    /**
     * compares year
     * 
     * @param comp
     *            is that
     * @return year difference
     */
    public int compareToYear(SongInfo comp) {
        if (comp == null) {
            throw new IllegalArgumentException();
        }
        int temp = this.year - comp.year;
        return temp;
    }


    /**
     * compares genres
     * 
     * @param comp
     *            is that
     * @return genre difference
     */
    public int compareToGenre(SongInfo comp) {
        if (comp == null) {
            throw new IllegalArgumentException();
        }
        int temp = this.genre.compareTo(comp.genre);
        return temp;
    }


    /**
     * compares titles
     * 
     * @param comp
     *            is that
     * @return title difference
     */
    public int compareToTitle(SongInfo comp) {
        if (comp == null) {
            throw new IllegalArgumentException();
        }
        int temp = this.title.compareTo(comp.title);
        return temp;
    }


    /**
     * getter
     * 
     * @return count heard
     */
    public int getReadingCountHeard() {
        return readingCountHeard;
    }


    /**
     * getter
     * 
     * @return count liked
     */
    public int getReadingCountLiked() {
        return readingCountLiked;
    }


    /**
     * getter
     * 
     * @return count liked
     */
    public int getArtCountHeard() {
        return readingCountHeard;
    }


    /**
     * getter
     * 
     * @return count liked
     */
    public int getArtCountLiked() {
        return readingCountLiked;
    }


    /**
     * getter
     * 
     * @return count heard
     */
    public int getSportsCountHeard() {
        return readingCountHeard;
    }


    /**
     * getter
     * 
     * @return count heard
     */
    public int getSportsCountLiked() {
        return readingCountLiked;
    }


    /**
     * getter
     * 
     * @return count liked
     */
    public int getMusicCountHeard() {
        return readingCountHeard;
    }


    /**
     * getter
     * 
     * @return count heard
     */
    public int getMusicCountLiked() {
        return readingCountLiked;
    }

}
