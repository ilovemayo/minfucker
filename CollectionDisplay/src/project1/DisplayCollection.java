package project1;

import bag.Bag;
import bag.BagInterface;
import java.util.Random;

/**
 * this class just makes a bag filled with the strings
 * 
 * @author David Weisiger dbw
 * @version September 5, 2017
 */

public class DisplayCollection {
    /**
     * two field variables:
     * 1) a constant string array with the allowed strings
     * 2) an item bag
     */
    public static final String[] STRINGS = { "red circle", "blue circle",
        "red square", "blue square" };
    private BagInterface<String> itemBag;


    /**
     * creates a bag filled with strings
     */
    public DisplayCollection() {
        itemBag = new Bag<>();

        Random random = new Random();
        int count = random.nextInt(11) + 5;
        for (int i = 0; i < count; i++) {
            itemBag.add(STRINGS[random.nextInt(4)]);
        }

    }


    /**
     * @return the item bag
     */
    public BagInterface<String> getItemBag() {
        return itemBag;
    }
}
