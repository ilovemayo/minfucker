package cybersecurity.lab2;

/**
 * Test class for testing LinkedKVStore
 * 
 * @author saima
 * @version 04.10.2017
 *
 */
public class LinkedKVStoreTest extends student.TestCase {

    private LinkedKVStore<Integer, String> store;


    /**
     * setUp() method
     */
    public void setUp() {
        store = new LinkedKVStore<Integer, String>();
    }


    /**
     * test isEmpty() before and after adding something in the store
     */
    public void testIsEmpty() {
        assertTrue(store.isEmpty());
        store.addLast(1, "one");
        assertFalse(store.isEmpty());
    }


    /**
     * test size()
     */
    public void testSize() {
        assertEquals(0, store.size());
        store.addLast(3, "three");
        assertEquals(1, store.size());
        store.addLast(3, "tree");
        assertEquals(2, store.size());
        store.removeFirst(3);
        assertEquals(1, store.size());
    }


    /**
     * test clear()
     */
    public void testClear() {
        store.clear();
        assertTrue(store.isEmpty());
        store.addLast(3, "three");
        assertFalse(store.isEmpty());
        store.clear();
        assertTrue(store.isEmpty());
        store.addLast(1, "one");
        store.addLast(2, "two");
        store.clear();
        assertTrue(store.isEmpty());
    }


    /**
     * test addLast() and getFirst()
     */
    public void testGetFirst() {
        assertNull(store.getFirst(1));
        store.addLast(3, "three");
        assertEquals("three", store.getFirst(3));
        store.addLast(3, "tree");
        assertEquals("three", store.getFirst(3));
    }


    /**
     * test addLast() and getLast()
     */
    public void testGetLast() {
        assertNull(store.getLast(1));
        store.addLast(3, "three");
        assertEquals("three", store.getLast(3));
        store.addLast(3, "tree");
        assertEquals("tree", store.getLast(3));
    }


    /**
     * test addLast() and getLast()
     */
    public void testAddLast() {
        store.addLast(1, "one");
        store.addLast(2, "two");

        assertEquals("one", store.getLast(1));
        assertEquals("two", store.getLast(2));

    }


    /**
     * test addFirst() and addLast()
     */
    public void testAddFirst() {
        store.addLast(1, "one");
        store.addFirst(2, "two");

        assertEquals("two", store.getFirst(2));
        assertEquals("one", store.getLast(1));
    }


    /**
     * test addLast()
     */
    public void testGetFirst2() {
        assertNull(store.getFirst(1));
        store.addLast(1, "one");
        store.addLast(2, "two");
        store.addLast(1, "uno");
        assertEquals("two", store.getFirst(2));
        assertEquals("one", store.getFirst(1));
    }


    /**
     * test removeFirst()
     */
    public void testRemoveFirst() {
        assertFalse(store.removeFirst(1));
        store.addLast(1, "one");
        assertTrue(store.removeFirst(1));
        store.addLast(1, "1.one");
        store.addLast(1, "1.two");
        store.addLast(1, "1.three");
        assertTrue(store.removeFirst(1));
        assertEquals(
            "Forward: [1=1.two],[1=1.three]\nBackward: [1=1.three],[1=1.two]\n",
            store.toString());
        assertEquals("1.two", store.getFirst(1));
        assertFalse(store.removeFirst(8));
    }


    /**
     * test removeLast() throws exception
     */
    public void testRemoveLast() {

        Exception exception = null;
        // Use try-catch block to see whether you get an exception of expected
        // type
        try {
            store.removeLast();
        }
        catch (RuntimeException e) {
            exception = e;
        }
        assertNotNull(exception);

    }


    /**
     * test toString()
     */
    public void testToString() {
        assertEquals("Forward: Backward: ", store.toString());
        store.addLast(1, "one");
        store.addLast(2, "two");
        store.addLast(1, "juan");
        store.addLast(1, "1");
        assertEquals("Forward: [1=one],[2=two],[1=juan],[1=1]\nBackward: "
            + "[1=1],[1=juan],[2=two],[1=one]\n", store.toString());
        store.removeFirst(1);
        assertEquals("Forward: [2=two],[1=juan],[1=1]\nBackward: [1=1],"
            + "[1=juan],[2=two]\n", store.toString());
        store.clear();
        assertEquals("Forward: Backward: ", store.toString());
    }

}
