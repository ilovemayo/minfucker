package game;
import java.util.EmptyStackException;
import java.lang.StringBuilder;

public class ArrayStack<T> {
  private T[] elements;
  private int size;

  public ArrayStack(int capacity) {
    @SuppressWarnings("unchecked")
    T[] tempElements = (T[]) new Object[capacity];
    elements = tempElements;

    this.size = 0;
  }

  /**
 * Checks if the stack is empty.
 *
 * @return {@code true} if the stack is empty;
 *         {@code false} otherwise.
 */
public boolean isEmpty() {
  return size == 0;
}

/**
 * Returns the item at the top of the stack without removing it.
 *
 * @throws EmptyStackException if stack is empty.
 *
 * @return the item at the top of the stack.
 */
public T peek() throws EmptyStackException {
    return elements[size - 1];
}

/**
 * Removes the item at the top of the stack.
 *
 * @throws EmptyStackException if stack is empty.
 *
 * @return the item at the top of the stack.
 */
public T pop() throws EmptyStackException {
  size--;
  return elements[size - 1];
}

/**
 * Pushes an item onto the stack.
 *
 * @param item the item to be pushed onto the stack.
 */
public void push(T item) {
  elements[size] = item;
  size++;
}

/**
 * Checks if an item is in the stack.
 *
 * @param item the item to be looked for.
 *
 * @return {@code true}, if the item is somewhere in the stack;
 * {@code false} otherwise.
 */
public boolean contains(T item) {
  for(T element : elements) {
    if(element.equals(item))
      return true;
  }
  return false;
}

/**
 * Number of items in the stack.
 *
 * @return the number of items in the stack.
 */
public int size() {
  return size;
}

/**
 * Clears the stack (removes all of the items from the stack).
 */
public void clear() {
  for(int i = 0; i < size; i++)
    elements[i] = null;
}

/**
 * Returns an array with a copy of each element in the stack with the top of
 * the stack being the last element
 *
 * @return the array representation of the stack
 */
public T[] toArray() {
  return elements;
}

/**
 * Returns the string representation of the stack.
 *
 * [] (if the stack is empty)
 * [bottom, item, ..., item, top] (if the stack contains items)
 *
 * @return the string representation of the stack.
 */
@Override
public String toString() {
  StringBuilder sb = new StringBuilder("[");
  for(T element : elements)
    sb.append(element + " ");
  return sb.deleteCharAt(sb.length() - 1).append("]").toString();
}

/**
 * Two stacks are equal iff they both have the same size and contain the
 * same elements in the same order.
 *
 * @param other the other object to compare to this
 *
 * @return {@code true}, if the stacks are equal;
 * {@code false} otherwise.
 */
}