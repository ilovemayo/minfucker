package icecream;

import java.util.Stack;

/**
 * 
 * This class implements the ice cream cone adt
 * 
 * @author davidweisiger
 * @version September 29, 2017
 *
 */

public class IceCreamCone implements IceCreamConeADT {
    /**
     * private instance variables
     */
    Stack<String> flavors;
    private int numScoops;


    /**
     * simple constructor
     * 
     */

    public IceCreamCone() {
        numScoops = 0;
        flavors = new Stack<String>();
    }


    /**
     * similar equals to what we've seen
     */

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other == null) || (other.getClass() != IceCreamCone.class)) {
            return false;
        }
        return flavors.equals(((IceCreamCone)other).flavors);
    }


    /**
     * overrides the eat scoop method
     */

    @Override
    public String eatScoop() {
        String flavor = flavors.peek();
        flavors.pop();
        numScoops--;
        return flavor;
    }


    /**
     * overrides the add scoop method
     */

    @Override
    public void addScoop(String flavor) {
        flavors.push(flavor);
        numScoops++;
    }


    /**
     * overrides the num scoop method
     */

    @Override
    public int numScoops() {
        return numScoops;
    }


    /**
     * overrides the contains method
     */

    @Override
    public boolean contains(String flavor) {
        return flavors.contains(flavor);
    }


    /**
     * overrides the empty cone method
     */

    @Override
    public boolean emptyCone() {
        return flavors.isEmpty();
    }


    /**
     * overrides the current scoop method
     */

    @Override
    public String currentScoop() {
        return flavors.peek();
    }


    /**
     * overrides the to string method
     */

    @Override
    public String toString() {
        return flavors.toString();
    }
}
