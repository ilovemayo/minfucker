/**
 * 
 */
package spacecolonies;

import java.util.Arrays;

import list.AList;

/**
 * This class handles all the major calculations and decision-making for the
 * program. It is in charge of handling accept people in a planet if they meet
 * all the requirements. It will either add people to a planet or reject them.
 * 
 * @author henry argueta
 * @version 11/12/17
 *
 */

public class ColonyCalculator {
    // ~ Fields -----------------------------------------------------

    /**
     * Constructs a newly allocated Integer object that represents the specified
     * int value.
     */
    public static final int NUM_PLANETS = 3;

    /**
     * Constructs a newly allocated Integer object that represents the specified
     * int value.
     * 
     */
    public static final int MIN_SKILLS_LEVEL = 1;

    /**
     * Constructs a newly allocated Integer object that represents the specified
     * int value.
     * 
     */
    public static final int MAX_SKILLS_LEVEL = 5;

    /**
     * A class that implements a queue of objects by using an array
     */
    private ArrayQueue<Person> applicantQueue;

    /**
     * containing the elements of the specified collection
     */
    private AList<Person> rejectBus;

    /**
     * This class contain objects of string, for the planet�s name, three ints
     * for their minimum skill requirements (on a scale of 1 to 5),
     */
    private static Planet[] planets = new Planet[NUM_PLANETS + 1];
    
    // ~ Constructor -----------------------------------------------------

    /**
     * This constructor takes in a ArrayQueue and an array of planets generated
     * by ColonyReader. This constructor will intitiate rjectBus, applicantQueue
     * and planets
     * 
     * @param personQueue
     *            ArrayQueue of applicants
     * @param planet
     *            an array of planets
     */
    public ColonyCalculator(ArrayQueue<Person> personQueue, Planet[] planet) {

        if (personQueue == null)
        {
            throw new IllegalArgumentException();
        }

        rejectBus = new AList<>();
        applicantQueue = personQueue;
        planets = planet;

    }

    /**
     * Gets applicantWQueue
     * 
     * @return applicantQueue applicant Queue
     */
    public ArrayQueue<Person> getQueue() {
        return applicantQueue;
    }

    /**
     * Gets planets array
     * 
     * @return planets array of planets
     */
    public static Planet[] getPlanets() {
        return planets;

    }
    
    // ~ Methods -----------------------------------------------------

    /**
     * This method will determine if the next applicant can be accepted to a
     * planet. If they have a preference, then check that the planet is not full
     * and the applicant matches the eligibility requirements. If they would be
     * accepted to that planet, then return that planet, otherwise return null.
     * If they don't have a preference then return the planet that has the most
     * from 3 to 1 if they meet requirement. If they don't then it will search
     * until one if found and if one is not found then will return null.
     * 
     * @return planets array of planets
     * @param nextPerson
     *            next person's planet
     */
    public Planet getPlanetForPerson(Person nextPerson) {

        // check if null
        if (nextPerson == null)
        {
            return null;
        }

        if (planetByNumber(nextPerson.getPlanet()) != null
                && getPreferredPlanet(nextPerson,
                        nextPerson.getPlanet()) != null)
        {
            return getPreferredPlanet(nextPerson, nextPerson.getPlanet());

        }

        if (planetByNumber(nextPerson.getPlanet()) == null
                && getMostAvailablePlanet(nextPerson) != null)
        {
            return getMostAvailablePlanet(nextPerson);

        }

        return null;
    }

    /**
     * This method will determine if the next applicant has a planet preference.
     * 
     * @return planets array of planets
     * 
     * @param person
     *            person's planet
     * @param n
     *            planet number
     */
    private Planet getPreferredPlanet(Person person, int n) {

        if (!planets[n].isFull()
                && !planets[n].getSkills().isBelow(person.getSkills()))
        {

            return planets[n];
        }

        else
        {
            return null;

        }

    }

    /**
     * if this method is called then the person does not have a preference and
     * the most available one will be assigned if they meet the requirements
     * 
     * @return planets of person's planet
     * 
     * @param person
     *            person's planet
     */
    private Planet getMostAvailablePlanet(Person person) {

        // check if null
        if (person == null)
        {
            return null;
        }

        if (person.getSkills() == null)
        {
            return null;

        }

        if (planets == null)
        {
            return null;

        }

        Skills skills = person.getSkills();

        int array[] = { planets[3].getAvailability(),
                planets[2].getAvailability(), planets[1].getAvailability() };

        // sort array
        Arrays.sort(array);

        // find most available from planet 3 to planet 1
        for (int i = array.length - 1; i >= 0; i--)
        {

            for (int j = planets.length - 1; j >= 1; j--)
            {

                if (array[i] == planets[j].getAvailability()
                        && !planets[j].isFull()
                        && !planets[j].getSkills().isBelow(skills))
                {
                    return planets[j];
                }
            }
        }

        return null;

    }

    /**
     * This method will attempt to accept the next applicant. will determine the
     * person�s planet preference with getPlanetForPerson in order to send them
     * to the correct planet. Then add the person to their preferred planet and
     * then dequeue them from the applicant queue.
     * 
     * @return true if person is able to get accepted
     */
    public boolean accept() {

        if (applicantQueue.isEmpty())
        {
            return false;
        }

        if (applicantQueue.getFront() == null)
        {
            return false;
        }

        Person person = applicantQueue.getFront();

        if (getPlanetForPerson(person) != null)
        {

            if (getPlanetForPerson(person).addPerson(person))
            {
                applicantQueue.dequeue();
                return true;
            }
        }
        return false;
    }

    /**
     * When reject is called, the next applicant in line will be put on the
     * rejectBus. It will then Remove the Person from the line and add them to
     * an AList<Person> that will represent the bus.
     */
    public void reject() {

        if (!applicantQueue.isEmpty() && applicantQueue.getFront() != null)
        {
            rejectBus.add(applicantQueue.getFront());
            applicantQueue.dequeue();
        }

    }

    /**
     * Return the Planet object for the given number For any other number,
     * return null
     * 
     * @param planet
     *            planet number
     * @return planet that corresponds to the number
     */
    public Planet planetByNumber(int planet) {
        if (planet <= 0 || planet >= 4)
        {

            return null;
        }

        if (planets[planet] == null)
        {
            return null;
        }

        return planets[planet];
    }

}
